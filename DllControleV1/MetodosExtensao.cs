﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace DllControleV1
{
    public static class Extensao
    {
        public static string RemoveAcentos(this string str)
        {
            string[] acentos = new string[] { "ç", "Ç", "á", "é", "í", "ó", "ú", "ý", "Á", "É", "Í", "Ó", "Ú", "Ý", "à", "è", "ì", "ò", "ù", "À", "È", "Ì", "Ò", "Ù", "ã", "õ", "ñ", "ä", "ë", "ï", "ö", "ü", "ÿ", "Ä", "Ë", "Ï", "Ö", "Ü", "Ã", "Õ", "Ñ", "â", "ê", "î", "ô", "û", "Â", "Ê", "Î", "Ô", "Û" };
            string[] semAcento = new string[] { "c", "C", "a", "e", "i", "o", "u", "y", "A", "E", "I", "O", "U", "Y", "a", "e", "i", "o", "u", "A", "E", "I", "O", "U", "a", "o", "n", "a", "e", "i", "o", "u", "y", "A", "E", "I", "O", "U", "A", "O", "N", "a", "e", "i", "o", "u", "A", "E", "I", "O", "U" };

            str = str.Trim();
            for (int i = 0; i < acentos.Length; i++)
            {
                str = str.Replace(acentos[i], semAcento[i]);
            }
            return str;
        }

        public static string RemoveAscii(this string str)
        {
            string ascii;
            str = str.Trim();

            //string[] caracteresEspeciais = { "'", "\"" };
            //for (int i = 0; i < caracteresEspeciais.Length; i++)
            //{
            //    str = str.Replace(caracteresEspeciais[i], "");
            //}

            for (int i = 0; i < str.Length; i++)
            {
                if ((int)str[i] < 32 || (int)str[i] > 127)
                {
                    ascii = str[i].ToString();
                    str = str.Replace(ascii, "");
                }
            }
            return str;
        }

        //  **  data vazia no fox vem com: "1899-12-30 00:00:00.000", deixo 1753-01-01 00:00:00.000 para o sql  **
        public static DateTime FixDataNull(this DateTime datain)
        {
            string datafox = "1899-12-30 00:00:00.000";

            int result = DateTime.Compare(datain, DateTime.Parse(datafox));

            if (result == 0) // ** mesma data   **
                return DateTime.Parse("1753-01-01 00:00:00.000");
            else
                return datain;
        }

        //  **  se a string for convertivel para numero mantem a mesma, senao retorna caracter "0"    **
        public static string NumTryParse(this string nstring)
        {
            string snumero = nstring;
            double numero = 0;

            try
            {
                numero = Convert.ToDouble(nstring);
            }
            catch
            {
                snumero = "0";
            }

            return snumero;
        }


    }
}
