/*
===============================================================
Genexus - Impressora Fiscal .Net
Impressora Bematech
Autoria: Dalci de Jesus Bagolin
dalci@abos.com.br
�gua Boa - MT
---------------------------------------------------------------
Ex.:
Event 'CancelaItemGenerico'
Call('bemafi32cs') // Apenas para inserir a Unit automaticamente no .rsp
CSHARP [!&iRetorno!] = bemafi32cs.Bematech_FI_CancelaItemGenerico([!&Num_Item!]);
CSHARP bemafi32cs.Analisa_iRetorno([!&iRetorno!]);
CSHARP [!&iRetorno!] = bemafi32cs.Bematech_FI_LeituraX();
CSHARP bemafi32cs.Analisa_iRetorno([!&iRetorno!]);
EndEvent
================================================================
*/

using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Security.Cryptography; 
using System.Text;


namespace DllControleV1
{
	public class geramd5{

		  public static string CalcMd5(string input)
		  {
			MD5 md5 = System.Security.Cryptography.MD5.Create();
			byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
			byte[] hash = md5.ComputeHash(inputBytes);

			// Segundo passo, converter o array de bytes em uma string haxadecimal
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < hash.Length; i++) {
			sb.Append(hash[i].ToString("X2"));
			}
			return sb.ToString();
		  
		  }
	}
}

