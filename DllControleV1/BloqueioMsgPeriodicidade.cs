﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Windows;
using System.IO;
//using System.Text;
using System.Security.Cryptography;

namespace DllControleV1
{

    public class MsgBloqueio
    {
        private DateTime dataatualizacao;
        private string mensagem;

        public void Set(DateTime dataatualizacao, string mensagem)
        {
            this.dataatualizacao = dataatualizacao;
            this.mensagem = mensagem;
        }

        //  **   Load do objeto pelo arquivo    **
        public void Load()
        {
            DateTime dataatualiza = DateTime.MinValue;
            string mensagemcompleta = "";
            string nome_arquivo = @"mb.dat";
            string pathfile = Directory.GetCurrentDirectory() + '\\' + nome_arquivo;

            int counter = 0;
            string line;

//            MessageBox.Show("LOAD");

            if (File.Exists(pathfile))
            {
                //mensagemb = System.IO.File.ReadAllText(pathfile);
                System.IO.StreamReader file = new System.IO.StreamReader(pathfile);
                while ((line = file.ReadLine()) != null)
                {
                    //System.Console.WriteLine(line);
                    counter++;
                    if (counter == 1)   //  ** Data da ultima atualização esta na 1º linha    **
                    {
                        if (DateTime.TryParse(line, out dataatualiza)){}
                        else
                             dataatualiza = DateTime.MinValue;
                    }else
                        mensagemcompleta += line + '\n';
                }

                this.dataatualizacao = dataatualiza;
                this.mensagem = mensagemcompleta;

                file.Close();

                
            }
            else // ** Mensagem Padrao **
            {
               
                mensagemcompleta = "<p>Prezado Cliente,</p>" + 
                "<p>&nbsp;</p>" + 
                "<p>Pedimos gentilmente que entre em contato com nosso departamento Financeiro.</p>" + 
                "<p>Hor&aacute;rio das 09:00 as 17:00 se segunda a sexta.</p>" + 
                "<p>Telefone: 3755-3003 ou 3755-3002. E-mail para&nbsp;<a href=" + '"' + "mailto:cobranca@workmotor.com.br" +'"' + ">cobranca@workmotor.com.br</a>.</p>" + 
                "<p>Atenciosamente,</p>" + 
                "<p>Equipe WorkMotor</p>";

                this.dataatualizacao = dataatualiza;
                this.mensagem = mensagemcompleta;
                Save();
            }

            //MessageBox.Show(this.dataatualizacao.ToString());
            //MessageBox.Show(this.mensagem);
            //MessageBox.Show(mensagemcompleta);

        }

        public void Save()
        {
            string nome_arquivo = @"mb.dat";
            string pathfile = Directory.GetCurrentDirectory() + '\\' + nome_arquivo;
            string msgcompleta = this.dataatualizacao.ToString() + "\n" + this.mensagem;

            //MessageBox.Show("Entrou para salvar :\n" + this.mensagem);

            if (msgcompleta.Length > 0)
            {
                if (System.IO.File.Exists(pathfile))
                {

                    System.IO.File.Delete(pathfile);
                    //MessageBox.Show("Deveria ter apagado!!");
                }

                System.IO.File.WriteAllText(pathfile, msgcompleta);
                //MessageBox.Show("Salvou geral!!");
            }

        }

        public string Download()
        {
            sbyte modeloMsg = 3;
            short segundos_lock = 0;
            int segundos_periodicidade = 0;
            string mensagem = "";

            //MessageBox.Show("Download");

            if (Controle.IsConnectedToInternet())
            {
                try
                {
                    string cnpj = Controle.pega_cnpj().Trim();  //  **  pega cnpj para pegar a msg  **
                    MsgTravaGet.MsgTravaGet ws = new MsgTravaGet.MsgTravaGet();

                    ws.Execute(ref cnpj, ref modeloMsg, ref segundos_lock, ref segundos_periodicidade, ref mensagem);

                }catch{}

                //MessageBox.Show(mensagem);

//                if (mensagem.Length > 0)
//                    MensagemBloqueioLocalSet(mensagem);
            }
            return mensagem;

        }


        public DateTime DataGet()
        {
            return this.dataatualizacao;
        }

        public string MensagemGet()
        {
            return this.mensagem;
        }
    }

    [ComVisible(true)]
    public class BloqueioMsgPeriodicidade
    {
        //Form form;
        //private System.Object lockThis = new System.Object();

        [DllImport("user32.dll", EntryPoint = "GetParent", CharSet = CharSet.Auto)]
        internal static extern IntPtr GetParent(IntPtr hWnd);


        public class WindowWrapper : System.Windows.Forms.IWin32Window
        {
            private IntPtr _hwnd;

            // Property
            public virtual IntPtr Handle
            {
                get { return _hwnd; }
            }

            // Constructor
            public WindowWrapper(IntPtr handle)
            {
                _hwnd = handle;
            }
        }

        public static void MensagemBloqueia(int time_thread)
        {
            Process currentProcess = Process.GetCurrentProcess();
            IntPtr myFormParentHandle = currentProcess.MainWindowHandle;


            Thread thread = new Thread(() => MostraMensagem(myFormParentHandle, time_thread));

            thread.SetApartmentState(ApartmentState.STA);   // ** precisa colocar para nao dar erro na chamada do form no thread    **
            thread.Start();

        }

        private static void MostraMensagem(IntPtr myFormParentHandle, int time_thread)
        {
            sbyte Trava = 1;
            //short segundos_lock = (short)tempobloqueado;
            MsgBloqueio mensagem = new MsgBloqueio();
            //MessageBox.Show("Inicio MSg");
            while (true)
            {
                try
                {
                    //  **  Pega a licenca do wkmotor   **
                    string licenca = Controle.licenca_atual();
                    Licenca ObjLicenca = new Licenca();

                    ObjLicenca = DecodeLicenca.Decode(licenca);

                    //MsgBloqueio mensagem = new MsgBloqueio();
                    //MessageBox.Show("Tempo:" + ObjLicenca.PeriodicidadeMsgBCode.ToString() + " * 1000" );
                    //MessageBox.Show("LiqBlock:" + ObjLicenca.LiqBlock.ToString());

                    // ** Msg de Bloqueio   **
                    if (ObjLicenca.LiqBlock == 2)
                    {
                        //Thread.Sleep(ObjLicenca.PeriodicidadeMsgBCode * 1000);
                        //Thread.Sleep(100);
                        mensagem.Load();

                        Form form = new Form1(mensagem.MensagemGet(), Trava, (short)ObjLicenca.TempoMsgB);
                        form.ShowDialog(new WindowWrapper(myFormParentHandle));
                        form.Dispose();

                        Thread.Sleep(ObjLicenca.PeriodicidadeMsgBCode * 1000);
                        
                    }
                    else
                    {
                        Thread.Sleep(time_thread);   // **  Se não for bloqueio espera  **
                    }

                }
                catch {
                    //MessageBox.Show(ex.Message);
                }
            }
        }


    }
}
