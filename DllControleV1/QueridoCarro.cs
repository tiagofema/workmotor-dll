﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Globalization;
using System.Threading;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Linq;
using System.IO;
using DllControleV1.ReenvioOs;
using DllControleV1.ReenvioOsProcessa;

namespace DllControleV1
{
    //  **  lista de tentativas de envio para nao parar o envio caso tenha alguma os com dados corrompidos  **
    //public class fila_tentativas_orcodigo
    //{
    //    public string orcodigo;
    //    public int tentativas;
    //}


    [ComVisible(true)]
    public class QueridoCarro
    {
        public const string versao_dll_envio = "1.09";
        public string versao_wkm = "0.00";

        //  **  dados default caso não tenha os arquivos    **
        int quantidade_venda = 10;
        int quantidade_compra = 10;
        int intervalo_envio_venda = 10;
        int intervalo_envio_compra = 10;
        int tempo_lancamento_venda = 30;
        int tempo_lancamento_compra = 30;

        int time_enviarprodutos = 30;
        int time_enviarmarcas = 60;
        int time_enviarfamilias = 60;
        int time_enviarclientes = 10;
        int time_recebervendas = 10;
        int time_receberclientes = 10;
        int time_enviarfuncionarios = 1200;
        int time_enviarveiculosclientes = 10;
        int time_enviartabelafipe = 120;
        int time_vendavalores = 60;

        public String Versao_Controle()
        {
            return versao_dll_envio;
        }

        public void Envia_Dados_QueridoCarro(string Versao_WkmFox)
        {
            versao_wkm = Versao_WkmFox;

            //            System.Threading.Thread.Sleep(2000);
            //MessageBox.Show("Tacale o Pal MARCO VEIO !!!! AAHHHHH");

            if (!System.IO.File.Exists("qcoff.dat"))
            {
                //  **  carrega os dados dat senao usa os defaults  **

                Arquivo_Configuracao();

                //MessageBox.Show(quantidade_venda.ToString() + " | " + intervalo_envio_venda.ToString() + " | " + tempo_lancamento_venda.ToString());
                //MessageBox.Show(quantidade_compra.ToString() + " | " + intervalo_envio_compra.ToString() + " | " + tempo_lancamento_compra.ToString());

                //  **  Reenvio de dados    **
                Thread thread = new Thread(() => th_Reenvio_Dados());
                thread.Start();

                //  **  sobe oficina ja bloqueada se for nova   **
                Thread thread1 = new Thread(() => Sobe_oficina());
                thread1.Start();

                //  **  sobe cliente e venda   **
                Thread thread2 = new Thread(() => th_Sobe_Venda());
                thread2.Start();

                //  **  sobe compras   **
                Thread thread3 = new Thread(() => th_Sobe_Compra());
                thread3.Start();
            }
            using (StreamWriter writer = new StreamWriter("AppWkmOficinaAtiva.json"))
            {
                writer.WriteLine(AppWkm.OficinaAtiva());
                writer.WriteLine(Controle.pega_cnpj());
            }

            if (!System.IO.File.Exists("AppWkmOff.dat") && AppWkm.OficinaAtiva() == true)
            {
                Arquivo_Configuracao_AppWkm();
                
                /* INTEGRAÇÃO QUERIDOCARRO - HELPMECARS */
                Thread thread12 = new Thread(() => th_VerificaChamados());
                thread12.Start();
                /* FIM METODOS INTEGRAÇÃO DLL APP WKM */

                /* METODOS INTEGRAÇÃO DLL APP WKM */

                //Thread thread4 = new Thread(() => th_Sobe_Marcas());
                //thread4.Start();

                //Thread thread5 = new Thread(() => th_Sobe_Familia());
                //thread5.Start();

                Thread thread6 = new Thread(() => th_Sobe_Estoque());
                thread6.Start();
                
                Thread thread7 = new Thread(() => th_Sobe_Clientes());
                thread7.Start();
                
                Thread thread8 = new Thread(() => th_Recebe_Clientes());
                thread8.Start();
                
                //Thread thread9 = new Thread(() => th_Sobe_Veiculos());
                //thread9.Start();
                
                Thread thread10 = new Thread(() => th_Sobe_Funcionarios());
                thread10.Start();
                
                Thread thread11 = new Thread(() => th_Recebe_Venda_app());
                thread11.Start();
                
                //Thread thread13 = new Thread(() => th_EnviarFipe());
                //thread13.Start();
                
                Thread thread14 = new Thread(() => th_Recebe_Veiculos_app());
                thread14.Start();
                
                Thread thread15 = new Thread(() => th_Envia_Vendas_Valores());
                thread15.Start();
                
                /* FIM METODOS INTEGRAÇÃO DLL APP WKM */
            }
        }
        

        public void th_Sobe_Clientes()
        {
            //  **  Esperar 10 segundos antes de rodar      **
            Thread.Sleep((10 / 60) * 60 * 1000);

            while (true)
            {
                bool returnClientes = false;
                bool returnFipe = false;

                while (!returnClientes && !returnFipe)
                {
                    if(returnClientes != true) { returnClientes = AppWkm.EnviarClientes(); }

                    if(returnFipe != true) { returnFipe = AppWkm.EnviarFipe(); }
                }

                AppWkm.EnviarVeiculos();
                
                Thread.Sleep((time_enviarclientes / 60) * 60 * 1000);
            }
        }
        
        public void th_Sobe_Estoque()
        {
            //  **  Esperar 10 segundos antes de rodar      **
            Thread.Sleep((10 / 60) * 60 * 1000);

            while (true)
            {

                bool returnMarcas = false;
                bool returnFamilias = false;

                while (!returnMarcas && !returnFamilias)
                {
                    if (returnMarcas != true) { returnMarcas = AppWkm.EnviarMarcas(); }

                    if (returnFamilias != true) { returnFamilias = AppWkm.EnviarFamilias(); }
                }

                if(returnFamilias == true || returnMarcas == true) { AppWkm.EnviarEstoque(); }
                
                Thread.Sleep((time_enviarprodutos / 60) * 60 * 1000);
            }
        }
        
        public void th_Recebe_Clientes()
        {
            //  **  Esperar 10 segundos antes de rodar      **
            Thread.Sleep((10 / 60) * 60 * 1000);

            while (true)
            {
                AppWkm.ReceberClientes();
                Thread.Sleep( (time_receberclientes / 60) * 60 * 1000);
                //Thread.Sleep( 5 * 60 * 1000); //5 minutos
            }
        }
        
        public void th_Sobe_Funcionarios()
        {
            //  **  Esperar 10 segundos antes de rodar      **
            Thread.Sleep((10 / 60) * 60 * 1000);

            while (true)
            {
                AppWkm.EnviarFuncionarios();
                Thread.Sleep((time_enviarfuncionarios / 60) * 60 * 1000);
                //Thread.Sleep( 5 * 60 * 1000); //5 minutos
            }
        }
        
        public void th_Envia_Vendas_Valores()
        {
            //  **  Esperar 10 segundos antes de rodar      **
            Thread.Sleep((10 / 60) * 60 * 1000);

            while (true)
            {
                AppWkm.EnviarOrdensServico();
                Thread.Sleep((time_vendavalores / 60) * 60 * 1000);
                //Thread.Sleep( 5 * 60 * 1000); //5 minutos
            }
        }
        
        public void th_Recebe_Venda_app()
        {
            //  **  Esperar 10 segundos antes de rodar      **
            Thread.Sleep((10 / 60) * 60 * 1000);

            while (true)
            {
                AppWkm.RecebeOrdensServico();
                Thread.Sleep((time_recebervendas / 60) * 60 * 1000);
            }
        }
        
        public void th_Recebe_Veiculos_app()
        {
            //  **  Esperar 60 segundos antes de rodar      **
            Thread.Sleep(1 * 60 * 1000);

            while (true)
            {
                Thread.Sleep(1 * 60 * 1000); //5 minutos
                AppWkm.ReceberVeiculos();
                Thread.Sleep((time_receberclientes / 60) * 60 * 1000);
                //Thread.Sleep( 5 * 60 * 1000); //5 minutos
            }
        }
        
        public void th_VerificaChamados()
        {
            while (true)
            {
                try
                {
                    string verificaChamado = Controle.ativo_verifica_chamado();
                    int time = Controle.pega_sleep_chamado();
                    Thread.Sleep(time * 1000);

                    if (verificaChamado == "S")
                    {
                        ServicosQC.Main();
                    } else
                    {
                        Thread.Sleep(20 * 60 * 1000);
                    }
                } catch
                {

                }
            }
        }

        public void th_Reenvio_Dados()
        {
            //  **  Esperar 1 minutos antes de rodar      **
            Thread.Sleep(1 * 60 * 1000);

            //  **  Apenas 1 Verificação por dia    **
            logReenvioArquivo log = new logReenvioArquivo();
            DateTime data_processado = log.getDataProcesado();
            String OfCnpjCpf = null;

            //  **  Se não existir o arquivo ou estiver com data antiga
            if ((data_processado.CompareTo(default(DateTime)) == 0) || (data_processado.CompareTo(DateTime.Today) < 0))
            {
                ReenviaOs ws = new ReenviaOs();
                ListaReenvios lista = new ListaReenvios();
                try
                {
//                    MessageBox.Show("busca dados");

                    String tselect = "select cfgtitcgc, cfgrzsocia, cfgnomefar, cfgddd, cfgtelefon, cfgemail, cfgestado, cfgcidade, cfgbairro, cfgenderec, cfgnumero from cfgsist where cfgcodigo = 1";
                    DataTable dt = Controle.getda_data_vfp(tselect);
                    if (dt.Rows.Count > 0 && !dt.HasErrors)
                    {
                        foreach (DataRow dtRow in dt.Rows)
                        {
                            OfCnpjCpf = dtRow[dtRow.Table.Columns["cfgtitcgc"].Ordinal].ToString().Replace("/", "").Replace(".", "").Replace("-", "");
                        }
                    }

                    if (!string.IsNullOrEmpty(OfCnpjCpf))
                    {
                        
                        lista = ws.Execute(OfCnpjCpf);
                        if (lista.Reenvios.Count() > 0)
                        {
                            foreach (Reenvios datas in lista.Reenvios)
                            {
                                add_fila_envio(datas.ReDtInicio, datas.ReDtFim, datas.ReTipo, OfCnpjCpf, datas.ReSeq);
                            }
                        }

                        log.setDataProcesado(DateTime.Now);
                    }
                }
                catch (Exception)
                {
                    
                    //throw;
                }

            }
            else
            {
                //MessageBox.Show("ja foi");
            }

        }

        public void th_Sobe_Venda()
        {
            //MessageBox.Show("Entrou no thread!");
            while (true)
            {
                //  **  1s = 1000ms
                Sobe_Venda();
                //  **  envia a fila de itens, tanto de os quanto de entarda    **
                Quantidade_Itens_Fila();

                //Thread.Sleep( 5 * 60 * 1000); //5 minutos
                Thread.Sleep(intervalo_envio_venda * 60 * 1000); 
            }
        }

        public void th_Sobe_Compra()
        {
            while (true)
            {
                Sobe_Compras();
                Thread.Sleep(intervalo_envio_compra * 60 * 1000); //5 minutos
            }
        }

        protected void Limpa_Fila_Dados_Invalidos(string modulo)
        {
            int quantidade_registros = 3;
            DataTable dt_dados;
            string tselect;
            List<string> lista_orcodigo_str = new List<string>();
            List<string> lista_nfecodigo = new List<string>();
            string orcodigo_str, nfecodigo;

            switch (modulo)
            {
                case "VENDA":
                    //  **  verifico se o orcodigo na fila existe na venda, senao apago **
                    tselect = "select top " + quantidade_registros.ToString().Trim() + " qcorcodigo " +
                              "from QCENVIAD order by qcorcodigo";

                    dt_dados = Controle.getda_data_vfp(tselect);
                    lista_orcodigo_str.Clear();

                    if (!dt_dados.HasErrors)
                    {
                        foreach (DataRow dtRow in dt_dados.Rows)
                        {
                            orcodigo_str = dtRow[dtRow.Table.Columns["qcorcodigo"].Ordinal].ToString().Trim();
                            tselect = "select orcodigo from orcvens1 where orcodigo = " + orcodigo_str;
                            try
                            {
                                DataTable dt_orcodigo = Controle.getda_data_vfp_sem_try(tselect);
                                if (dt_orcodigo.Rows.Count == 0 && !dt_orcodigo.HasErrors)
                                    lista_orcodigo_str.Add(orcodigo_str);
                            }
                            catch { }
                        }                      
                    }

                    if (lista_orcodigo_str.Count > 0)
                        Apaga_Fila_Envio_Venda(lista_orcodigo_str);

                    break;

                case "COMPRA":
                    //  **  verifico se o nfecodigo na fila existe na compra, senao apago **
                    tselect = "select top " + quantidade_registros.ToString().Trim() + " qcnfecodig " +
                              "from qcentrad order by qcnfecodig";

                    dt_dados = Controle.getda_data_vfp(tselect);
                    lista_nfecodigo.Clear();

                    if (!dt_dados.HasErrors)
                    {
                        foreach (DataRow dtRow in dt_dados.Rows)
                        {
                            nfecodigo = dtRow[dtRow.Table.Columns["qcnfecodig"].Ordinal].ToString().Trim();
                            tselect = "select nfecodigo from nentrsto where nfecodigo = " + '"' + nfecodigo.Trim() + '"';
                            try
                            {
                                DataTable dt_nfe = Controle.getda_data_vfp_sem_try(tselect);
                                if (dt_nfe.Rows.Count == 0 && !dt_nfe.HasErrors)
                                    lista_nfecodigo.Add(nfecodigo);
                            }
                            catch { }
                        }                        
                    }

                    if (lista_nfecodigo.Count > 0)
                        Apaga_Fila_Envio_Compra(lista_nfecodigo);

                    break;
            }

        }

        public void Arquivo_Configuracao()
        {
            string line;
            int modulo = 0;
            int pos = 0;
            int tam = 0;
            string palavra = "";
            string arquivo = "CTRLCONFIG.DAT";

            if (System.IO.File.Exists(arquivo))
            {
                System.IO.StreamReader file = new System.IO.StreamReader(arquivo);
                while ((line = file.ReadLine()) != null)
                {
                    if (line.Trim() == "#VENDA")
                    {
                        modulo = 1;
                        line = file.ReadLine();
                    }
                    else if (line.Trim() == "#COMPRA")
                    {
                        modulo = 2;
                        line = file.ReadLine();
                    }

                    pos = line.IndexOf(':');
                    tam = line.Length - pos - 1;
                    palavra = line.Substring(0, pos);

                    //MessageBox.Show(modulo.ToString() + "||" + palavra);

                    switch (palavra)
                    {
                        case "QUANTIDADE":
                            if (modulo == 1)
                                quantidade_venda = Convert.ToInt32(line.Substring(pos + 1, tam));
                            else if (modulo == 2)
                                quantidade_compra = Convert.ToInt32(line.Substring(pos + 1, tam));
                            break;

                        case "INTERVALO ENVIO":
                            if (modulo == 1)
                                intervalo_envio_venda = Convert.ToInt32(line.Substring(pos + 1, tam));
                            else if (modulo == 2)
                                intervalo_envio_compra = Convert.ToInt32(line.Substring(pos + 1, tam));
                            break;

                        case "TEMPO LANCAMENTO":
                            if (modulo == 1)
                                tempo_lancamento_venda = Convert.ToInt32(line.Substring(pos + 1, tam));
                            else if (modulo == 2)
                                tempo_lancamento_compra = Convert.ToInt32(line.Substring(pos + 1, tam));
                            break;

                    }
                    
                }

                file.Close();
                //MessageBox.Show(quantidade_venda.ToString() + " | " + intervalo_envio_venda.ToString() + " | " + tempo_lancamento_venda.ToString());
                //MessageBox.Show(quantidade_compra.ToString() + " | " + intervalo_envio_compra.ToString() + " | " + tempo_lancamento_compra.ToString());
            }
            else   //   ** se nao existe o arquivo crio com os campos default para usar como modelo caso quera alterar   **
            {
                try
                {
                    string dados_arquivo = "#VENDA" + "\n" +
                                           "QUANTIDADE:" + quantidade_venda.ToString().Trim() + "\n" +
                                           "INTERVALO ENVIO:" + intervalo_envio_venda.ToString().Trim() + "\n" +
                                           "TEMPO LANCAMENTO:" + tempo_lancamento_venda.ToString().Trim() + "\n" +
                                           "#COMPRA" + "\n" +
                                           "QUANTIDADE:" + quantidade_compra.ToString().Trim() + "\n" +
                                           "INTERVALO ENVIO:" + intervalo_envio_compra.ToString().Trim() + "\n" +
                                           "TEMPO LANCAMENTO:" + tempo_lancamento_compra.ToString().Trim();

                    System.IO.TextWriter txt = System.IO.File.AppendText(arquivo);
                    txt.WriteLine(dados_arquivo);
                    txt.Close();
                }
                catch { }
            }

        }

        public static void Sobe_oficina()
        {
            string tselect = "";
            QueridoCarro_GravaOficina.GravaOf ws = new QueridoCarro_GravaOficina.GravaOf();
            QueridoCarro_GravaOficina.Ofic Oficina = new QueridoCarro_GravaOficina.Ofic();
            //bool Sucesso = false;

            tselect = "select cfgtitcgc, cfgrzsocia, cfgnomefar, cfgddd, cfgtelefon, cfgemail, cfgestado, cfgcidade, cfgbairro, cfgenderec, cfgnumero from cfgsist where cfgcodigo = 1";

            DataTable dt = Controle.getda_data_vfp(tselect);
            if (dt.Rows.Count > 0 && !dt.HasErrors)
            {
                foreach (DataRow dtRow in dt.Rows)
                {
                    //MessageBox.Show(dtRow[dtRow.Table.Columns["cfgtitcgc"].Ordinal].ToString());
                    try
                    {
                        Oficina.OfCnpjCpf = dtRow[dtRow.Table.Columns["cfgtitcgc"].Ordinal].ToString();
                        Oficina.OfRzSocia = dtRow[dtRow.Table.Columns["cfgrzsocia"].Ordinal].ToString().RemoveAcentos();
                        Oficina.OfNomeFan = dtRow[dtRow.Table.Columns["cfgnomefar"].Ordinal].ToString().RemoveAcentos();
                        Oficina.OfDDD = Convert.ToInt16(dtRow[dtRow.Table.Columns["cfgddd"].Ordinal].ToString());
                        Oficina.OfTel = Convert.ToInt32(dtRow[dtRow.Table.Columns["cfgtelefon"].Ordinal].ToString());
                        Oficina.OfEmail = dtRow[dtRow.Table.Columns["cfgemail"].Ordinal].ToString();
                        Oficina.OfEstado = dtRow[dtRow.Table.Columns["cfgestado"].Ordinal].ToString();
                        Oficina.OfCidade = dtRow[dtRow.Table.Columns["cfgcidade"].Ordinal].ToString();
                        Oficina.OfBairro = dtRow[dtRow.Table.Columns["cfgbairro"].Ordinal].ToString();
                        Oficina.OfEndere = dtRow[dtRow.Table.Columns["cfgenderec"].Ordinal].ToString();
                        Oficina.OfNumero = Convert.ToInt32(dtRow[dtRow.Table.Columns["cfgnumero"].Ordinal].ToString());

                        ws.Execute(ref Oficina);
                        //Sucesso = true;
                        //string mensagem = "Subiu Oficina : " + DateTime.Now.ToString();
                        //Controle.log("log_qc.txt", mensagem);
                    }
                    //catch (Exception e)
                    catch
                    {
                        //MessageBox.Show(e.Message);
                        //Sucesso = true;
                        //string mensagem = "Erro Não Subiu Oficina : " + DateTime.Now.ToString();
                        //Controle.log("log_qc.txt", mensagem);

                    }

                }
            }
            //else Sucesso = false;

            //return Sucesso;
        }

        public bool Sobe_Cliente(List<string> empcodigo_str)
        {
            // tratar o emptipo depois

            // MessageBox.Show("Entrei Cliente : " + empcodigo_str.Count.ToString());

            QueridoCarro_GravaCliente.ClienteClienteItem[] Lista_Cliente = new QueridoCarro_GravaCliente.ClienteClienteItem[empcodigo_str.Count];
            QueridoCarro_GravaCliente.GravaCli ws = new QueridoCarro_GravaCliente.GravaCli();
            bool Sucesso = false;
            int cont_empresas = 0;
            DataTable dt;

            //MessageBox.Show("empcodigo_str.Count: " + empcodigo_str.Count.ToString());

            string tselect = "";
            for (int i = 0; i < empcodigo_str.Count; i++)
            {
                tselect = "select Empcgccpf, Emprzsocia, EmpNomFan, EmpSexo, EmpDtNasc, EmpTime, Empemail, EmpTipo, Empcep, Empcodigo" +
                                 ", Empendere, Empnumero, Empcomple, Empbairro, Empcidade, Empestado, Empddd, Empfone, EmpDDDcel, Empcel, Empcodigo " +
                                 "from empresas where empcodigo = " + empcodigo_str[i];

                //MessageBox.Show(tselect);
                dt = Controle.getda_data_vfp(tselect);
                if (dt.Rows.Count > 0 && !dt.HasErrors)
                {
                    foreach (DataRow dtRow in dt.Rows)
                    {
                        Lista_Cliente[i] = new QueridoCarro_GravaCliente.ClienteClienteItem();

                        Lista_Cliente[i].CliCgcCpf = dtRow[dtRow.Table.Columns["Empcgccpf"].Ordinal].ToString().Replace("-", "").Replace("/", "").Replace(".", "");
                        Lista_Cliente[i].CliRzSocia = dtRow[dtRow.Table.Columns["Emprzsocia"].Ordinal].ToString();
                        Lista_Cliente[i].CliNomFam = dtRow[dtRow.Table.Columns["EmpNomFan"].Ordinal].ToString();
                        Lista_Cliente[i].CliSexo = dtRow[dtRow.Table.Columns["EmpSexo"].Ordinal].ToString();
                        Lista_Cliente[i].CliDtNasc = DateTime.Parse(dtRow[dtRow.Table.Columns["EmpDtNasc"].Ordinal].ToString()).FixDataNull();
                        Lista_Cliente[i].CliTime = dtRow[dtRow.Table.Columns["EmpTime"].Ordinal].ToString();
                        Lista_Cliente[i].CliEmail = dtRow[dtRow.Table.Columns["Empemail"].Ordinal].ToString();
                        Lista_Cliente[i].CliTipo = Convert.ToSByte(dtRow[dtRow.Table.Columns["EmpTipo"].Ordinal].ToString().NumTryParse());
                        Lista_Cliente[i].CliCep = dtRow[dtRow.Table.Columns["Empcep"].Ordinal].ToString();
                        Lista_Cliente[i].CliEndere = dtRow[dtRow.Table.Columns["Empendere"].Ordinal].ToString();
                        Lista_Cliente[i].CliNum = Convert.ToInt32(dtRow[dtRow.Table.Columns["Empnumero"].Ordinal].ToString().NumTryParse());
                        Lista_Cliente[i].CliComple = dtRow[dtRow.Table.Columns["Empcomple"].Ordinal].ToString();
                        Lista_Cliente[i].CliBairro = dtRow[dtRow.Table.Columns["Empbairro"].Ordinal].ToString();
                        Lista_Cliente[i].CliCidade = dtRow[dtRow.Table.Columns["Empcidade"].Ordinal].ToString();
                        Lista_Cliente[i].CliEstado = dtRow[dtRow.Table.Columns["Empestado"].Ordinal].ToString();
                        Lista_Cliente[i].CliDdd = Convert.ToInt16(dtRow[dtRow.Table.Columns["Empddd"].Ordinal].ToString().NumTryParse());
                        Lista_Cliente[i].CliFone = Convert.ToInt32(dtRow[dtRow.Table.Columns["Empfone"].Ordinal].ToString().NumTryParse());
                        Lista_Cliente[i].CliDDDCel = Convert.ToInt16(dtRow[dtRow.Table.Columns["EmpDDDcel"].Ordinal].ToString().NumTryParse());
                        Lista_Cliente[i].CliCel = Convert.ToInt32(dtRow[dtRow.Table.Columns["Empcel"].Ordinal].ToString().NumTryParse());
                        Lista_Cliente[i].CliCod = Convert.ToInt32(dtRow[dtRow.Table.Columns["Empcodigo"].Ordinal].ToString().NumTryParse());
                        cont_empresas += 1;
                    }
                }
                else
                {
                    //  **  sempre tem que achar a empresa, se não achou é erro **
                    //string mensagem = "Erro Sobre Cliente -> Quantidade de cliente : " + DateTime.Now.ToString();
                    //Controle.log("log_qc.txt", mensagem);

                    Sucesso = false;
                    break;
                }

            }   //  **  loop    **

            //MessageBox.Show(empcodigo_str.Count.ToString() + " == " + cont_empresas.ToString());
            if (empcodigo_str.Count == cont_empresas)   //  **  se qtd de empresas a mesma da venda pegou todos os dados    **
            {
                try
                {
                    ws.Execute(ref Lista_Cliente);
                    Sucesso = true;

                    //string mensagem = "Enviado cliente com sucesso : " + DateTime.Now.ToString();
                    //Controle.log("log_qc.txt", mensagem);
                    //for (int i = 0; i < cont_empresas; i++)
                    //{
                    //    MessageBox.Show(Lista_Cliente[i].CliRzSocia);
                    //}

                }
                catch(Exception ex)
                {
                    //MessageBox.Show("Erro sobe cliente:" + ex.Message);
                    //string mensagem = "Erro Sobre Cliente Tentativa envio : " + DateTime.Now.ToString();
                    //Controle.log("log_qc.txt", mensagem);

                    Sucesso = false;
                }

            }


            return Sucesso;
        }

        public bool bloq_Envio_Pesquisa_Satisfacao(string tipo, string empcodigo)
        {
            bool retorno = false;
            string tselect = "";
            string nomeColuna = "";

            switch (tipo)
            {
                case "CFG":
                    tselect = "select cfgqcemail from cfgsist where cfgcodigo = 1";
                    nomeColuna = "cfgqcemail";
                    break;
                case "CLIENTE":
                    tselect = "select empqcemail from empresas where empcodigo = " + empcodigo.Trim();
                    nomeColuna = "empqcemail";
                    break;
            }

            DataTable dt = Controle.getda_data_vfp(tselect);
            if (dt.Rows.Count > 0 && !dt.HasErrors)
            {
                foreach (DataRow dtRow in dt.Rows)
                {
                    try
                    {
                        if (Convert.ToInt16(dtRow[dtRow.Table.Columns[nomeColuna].Ordinal].ToString()) == 1)
                            retorno = true;
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                }
            }

            //MessageBox.Show(retorno.ToString());        
            
        
/*

            //  **********************************    ENVIA PESQUISA OU NÃO USA O PRISMA   *****************************
            string tselect = "select cfgqcemail from cfgsist where cfgcodigo = 1";
            bool retorno = false;

            DataTable dt = Controle.getda_data_vfp(tselect);
            if (dt.Rows.Count > 0 && !dt.HasErrors)
            {
                foreach (DataRow dtRow in dt.Rows)
                {
                    //MessageBox.Show(dtRow[dtRow.Table.Columns["cfgtitcgc"].Ordinal].ToString());
                    try
                    {
//                        MessageBox.Show("cfgqcemail: " + dtRow[dtRow.Table.Columns["cfgqcemail"].Ordinal].ToString());

                        if (Convert.ToInt16(dtRow[dtRow.Table.Columns["cfgqcemail"].Ordinal].ToString()) == 1)
                        {
                            
                            retorno = true;
                        }
                            
                        //Sucesso = true;
                        //string mensagem = "Subiu Oficina : " + DateTime.Now.ToString();
                        //Controle.log("log_qc.txt", mensagem);
                    }
                    //catch (Exception e)
                    catch
                    {
                        //MessageBox.Show("Deu ruim!");
                        return false;
                    }

                }
            }
 */ 
            return retorno;
        }

        public void Sobe_Venda()
        {
            //MessageBox.Show("Entrei Sobe Venda .");

            //int quantidade_registros = 2;
            //QueridoCarro_GravaOficina.Ofic Oficina = new QueridoCarro_GravaOficina.Ofic();
            //QueridoCarro_GravaVenda2.GravaVenda2 ws = new QueridoCarro_GravaVenda2.GravaVenda2();
            //QueridoCarro_GravaVenda2.CestaCestaItem Itens = new QueridoCarro_GravaVenda2.CestaCestaItem();

            QueridoCarro_GravaVendaDllV1.GravaVendaDllV1 ws = new QueridoCarro_GravaVendaDllV1.GravaVendaDllV1();
            QueridoCarro_GravaVendaDllV1.CestaCestaItem Itens = new QueridoCarro_GravaVendaDllV1.CestaCestaItem();


            //List<QueridoCarro_GravaVenda2.CestaCestaItem> ItensList = new List<QueridoCarro_GravaVenda2.CestaCestaItem>();
            List<QueridoCarro_GravaVendaDllV1.CestaCestaItem> ItensList = new List<QueridoCarro_GravaVendaDllV1.CestaCestaItem>();
            List<String> orcodigo_list = new List<string>();
            List<string> lista_empcodigo_str = new List<string>();

            lista_empcodigo_str.Clear();

            //QueridoCarro_GravaVenda2.Venda2VendaItem[] lista_venda = new QueridoCarro_GravaVenda2.Venda2VendaItem[quantidade_venda];

            QueridoCarro_GravaVendaDllV1.VendaDllV1VendaItem[] lista_venda = new QueridoCarro_GravaVendaDllV1.VendaDllV1VendaItem[quantidade_venda];

            int cont_venda = 0;
            int cont_itens = 0;
            //int cont_deleta = 0;
            //            int cont1 = 0, cont2 = 0;

            string tselect = "";
            string cnpj = "";
            string Empcgccpf = "";

            //where OfCnpjCpf = '27812162000180'    // !!!!!!!!!!!!!

            //if (Envio_Liberado("VENDA"))   //  **  se tem os a enviar e com acesso a internet  **
            //{
            //    cnpj = Controle.pega_cnpj().Trim();
            //    if (!string.IsNullOrEmpty(cnpj))
            //    {
            //        MessageBox.Show("ok");
            //    }
            //    else
            //        MessageBox.Show("Erro 1");
            //}
            //else
            //    MessageBox.Show("erro 2");

            if (Envio_Liberado("VENDA"))   //  **  se tem os a enviar e com acesso a internet  **
            {
                cnpj = Controle.pega_cnpj().Trim();
                if (!string.IsNullOrEmpty(cnpj))
                {
                    //  **********************************    ENVIA PESQUISA OU NÃO. USA O PRISMA   *****************************
                    short bloq_envio_pesquisa = 0;
                    if (bloq_Envio_Pesquisa_Satisfacao("CFG", ""))
                        bloq_envio_pesquisa = 1;
                   

                    //  **********************************    VENDA   *****************************


                    // ** tirei DadKmRet pq é formula   **
                    /*
                        tselect = "select top " + quantidade_venda.ToString().Trim() + " qcorcodigo, qcorcdata, orcodigo, Orcdata, OrcHrInic, (select Empcgccpf from empresas where empcodigo = A.empcodigo ) as Empcgccpf" +
                        ", OrcRzSocia, DadDatFech, OrcHrFim, DadChapa, DadKm, DadKmMesRo, DadCombust, DadPrevRet " +
                        ", DadChassis, DadCorVeic, DadAnoVeic, OrcObjcodi, OrcObjeto, DadAnoFab, DadModVeic, Orcdescto " +
                        ",((select sum( Round(ociprctab *  ociquanti, 2)  ) from ORCVENS2 where orcodigo = A.orcodigo and day(ocidtapro) > 0 ) - Orcdescto + OrcOutrasD + OrcFrete - OrcTotrete + OrcProdIpi) as OrcTotVed " +
                        ",((select sum( Round(ociprctab *  ociquanti, 2)  ) from ORCVENS2 where orcodigo = A.orcodigo) - Orcdescto + OrcOutrasD + OrcFrete - OrcTotrete) as OrcTotOrc " +
                        ", Empcodigo " +
                        "from ORCVENS1 A ,QCENVIAD B where A.orcodigo = B.qcorcodigo " +
                        "order by orcodigo";
                    */


                    //  **  Foi colocado o "min" pois estava dando erro "subquery returns more than one record"
                    tselect = "select top " + quantidade_venda.ToString().Trim() + " qcorcodigo, qcorcdata, orcodigo, Orcdata, OrcHrInic, (select min(Empcgccpf) from empresas where A.empcodigo = empcodigo ) as Empcgccpf" +
                    ", OrcRzSocia, DadDatFech, OrcHrFim, DadChapa, DadKm, DadKmMesRo, DadCombust, DadPrevRet " +
                    ", DadChassis, DadCorVeic, DadAnoVeic, OrcObjcodi, OrcObjeto, DadAnoFab, DadModVeic, Orcdescto " +
                    ",((select sum( Round(ociprctab *  ociquanti, 2)  ) from ORCVENS2 where orcodigo = A.orcodigo and day(ocidtapro) > 0 ) - Orcdescto + OrcOutrasD + OrcFrete - OrcTotrete + OrcProdIpi) as OrcTotVed " +
                    ",((select sum( Round(ociprctab *  ociquanti, 2)  ) from ORCVENS2 where orcodigo = A.orcodigo) - Orcdescto + OrcOutrasD + OrcFrete - OrcTotrete) as OrcTotOrc " +
                    ", Empcodigo " +
                    "from ORCVENS1 A ,QCENVIAD B where A.orcodigo = B.qcorcodigo " +
                    "order by orcodigo";

                    //datainicial = DateTime.Parse(dtRow[dtRow.Table.Columns["Qcorcdata"].Ordinal].ToString()).FixDataNull();
                    //MessageBox.Show((datafinal - datainicial).TotalMinutes.ToString());
                    //MessageBox.Show(tselect);

                    DataTable dt_venda = Controle.getda_data_vfp(tselect);

                    //MessageBox.Show("Quantidade Itens Select :" + dt_venda.Rows.Count.ToString() + " | " + dt_venda.HasErrors.ToString());

                    DataTable dt_itens;
                    string orcodigo_str;

                    orcodigo_list.Clear();
                    if (dt_venda.Rows.Count > 0 && !dt_venda.HasErrors)
                    {
                        foreach (DataRow dtRow in dt_venda.Rows)    //  **  percorro as vendas  **
                        {
                            //  **  apenas envio os que não tiveram alteração a pelo menos 10 minutos   **
                            DateTime datainicial = DateTime.Parse(dtRow[dtRow.Table.Columns["Qcorcdata"].Ordinal].ToString()).FixDataNull();
                            DateTime datafinal = DateTime.Now;

                            //  **  nao preciso me preocupar com a data zerada pois ao converter sempre sera menor e tentara enviar **
                            if (((datafinal - datainicial).TotalMinutes > tempo_lancamento_venda))
                            {

                                //MessageBox.Show("tempo liberado");

                                //MessageBox.Show(dtRow[dtRow.Table.Columns["orcodigo"].Ordinal].ToString() +
                                //       " = " + dtRow[dtRow.Table.Columns["Empcodigo"].Ordinal].ToString());

                                //  **********************************************    VENDA   **********************************************
                                //lista_venda[cont_venda] = new QueridoCarro_GravaVenda2.Venda2VendaItem();
                                lista_venda[cont_venda] = new QueridoCarro_GravaVendaDllV1.VendaDllV1VendaItem();

                                //MessageBox.Show("venda : " + dtRow[dtRow.Table.Columns["orcodigo"].Ordinal].ToString());

                                lista_venda[cont_venda].OfCnpjCpf = cnpj;
                                lista_venda[cont_venda].Orcodigo = Convert.ToInt32(dtRow[dtRow.Table.Columns["orcodigo"].Ordinal].ToString());
                                lista_venda[cont_venda].Orcdata = DateTime.Parse(dtRow[dtRow.Table.Columns["Orcdata"].Ordinal].ToString()).FixDataNull();
                                lista_venda[cont_venda].OrcHrInic = dtRow[dtRow.Table.Columns["OrcHrInic"].Ordinal].ToString();

                                //&CnpjCpfAux = Substr(Trim(Strtran(Strtran(Strtran(&CfgTitCgc,"/",""),".",""),"-","")),1,10) + Trim(Strtran(DadChapa,"-",""))
                                Empcgccpf = dtRow[dtRow.Table.Columns["Empcgccpf"].Ordinal].ToString().Replace("-", "").Replace("/", "").Replace(".", "");

                                //  **  ser for cpf ou cnpj valido usa o do cadastro senao usa cnpj da emprsa + placa   **
                                if (ValidaCPF.IsCpf(Empcgccpf) || ValidaCNPJ.IsCnpj(Empcgccpf))
                                {
                                    lista_venda[cont_venda].Empcgccpf = Empcgccpf.Trim();
                                }
                                else
                                {
                                    lista_venda[cont_venda].Empcgccpf = cnpj.Trim() + dtRow[dtRow.Table.Columns["DadChapa"].Ordinal].ToString().Replace("-", "").Trim();
                                }

                                //lista_venda[cont_venda].Empcgccpf = dtRow[dtRow.Table.Columns["Empcgccpf"].Ordinal].ToString().Replace("-", "").Replace("/", "").Replace(".", "");
                                lista_venda[cont_venda].OrcRzSocia = dtRow[dtRow.Table.Columns["OrcRzSocia"].Ordinal].ToString();
                                lista_venda[cont_venda].DadDatFech = DateTime.Parse(dtRow[dtRow.Table.Columns["DadDatFech"].Ordinal].ToString()).FixDataNull();
                                lista_venda[cont_venda].OrcHrFim = dtRow[dtRow.Table.Columns["OrcHrFim"].Ordinal].ToString();
                                lista_venda[cont_venda].DadChapa = dtRow[dtRow.Table.Columns["DadChapa"].Ordinal].ToString().Replace("-", "").Trim();
                                lista_venda[cont_venda].DadKm = Convert.ToInt64(dtRow[dtRow.Table.Columns["DadKm"].Ordinal].ToString());
                                lista_venda[cont_venda].DadKmMesRod = Convert.ToInt64(dtRow[dtRow.Table.Columns["DadKmMesRo"].Ordinal].ToString());
                                lista_venda[cont_venda].DadCombust = dtRow[dtRow.Table.Columns["DadCombust"].Ordinal].ToString();
                                lista_venda[cont_venda].DadPrevRet = DateTime.Parse(dtRow[dtRow.Table.Columns["DadPrevRet"].Ordinal].ToString()).FixDataNull();
                                lista_venda[cont_venda].DadChassis = dtRow[dtRow.Table.Columns["DadChassis"].Ordinal].ToString();
                                lista_venda[cont_venda].DadCorVeic = dtRow[dtRow.Table.Columns["DadCorVeic"].Ordinal].ToString();
                                lista_venda[cont_venda].DadAnoVeiculo = dtRow[dtRow.Table.Columns["DadAnoVeic"].Ordinal].ToString();
                                lista_venda[cont_venda].OrcObjcodi = dtRow[dtRow.Table.Columns["OrcObjcodi"].Ordinal].ToString();
                                lista_venda[cont_venda].OrcObjeto = dtRow[dtRow.Table.Columns["OrcObjeto"].Ordinal].ToString();
                                lista_venda[cont_venda].DadAnoFab = dtRow[dtRow.Table.Columns["DadAnoFab"].Ordinal].ToString();
                                lista_venda[cont_venda].DadModVeic = dtRow[dtRow.Table.Columns["DadModVeic"].Ordinal].ToString();
                                lista_venda[cont_venda].Orcdescto = Convert.ToDouble(dtRow[dtRow.Table.Columns["Orcdescto"].Ordinal].ToString());
                                lista_venda[cont_venda].OrcTotOrc = Convert.ToDouble(dtRow[dtRow.Table.Columns["OrcTotOrc"].Ordinal].ToString().NumTryParse());
                                lista_venda[cont_venda].Orctotved = Convert.ToDouble(dtRow[dtRow.Table.Columns["OrcTotVed"].Ordinal].ToString().NumTryParse());
                                lista_venda[cont_venda].VersaoDll = versao_dll_envio;
                                lista_venda[cont_venda].VersaoWkm = versao_wkm;


                                //  **  Verifica se o cliente permite o envio de email  **
                                if (bloq_Envio_Pesquisa_Satisfacao("CLIENTE", dtRow[dtRow.Table.Columns["Empcodigo"].Ordinal].ToString()) && bloq_envio_pesquisa == 0)
                                    bloq_envio_pesquisa = 1;

                                //  **  Caso o data/horario seja 01/01/2001 01:01:01 Não deve envia a pesquisa de satisfação pq é dado forçado pela wg  **
                                if (datainicial.ToString().CompareTo("01/01/2001 01:01:01") == 0)
                                {
                                    lista_venda[cont_venda].PrisCod = 1;
                                    //MessageBox.Show("nao mando email!!!");
                                }
                                else
                                    lista_venda[cont_venda].PrisCod = bloq_envio_pesquisa;

                                //  **********************************************    ITENS   **********************************************

                                orcodigo_str = dtRow[dtRow.Table.Columns["orcodigo"].Ordinal].ToString().Trim();
                                orcodigo_list.Add(orcodigo_str);

                                lista_empcodigo_str.Add(dtRow[dtRow.Table.Columns["Empcodigo"].Ordinal].ToString()); //  **  monto a lista de clientes para enviar os dados dele    **

                                tselect = "select orcodigo, ocitem, procodigo " +
                                ", ( select x.Marca from marcas X, produtos Y where Y.procodigo = A.procodigo and Y.marcodigo = X.marcodigo ) as Marca" +
                                ", ( select x.Famnome from famcad X, produtos Y where Y.procodigo = A.procodigo and Y.famcodigo = X.famcodigo ) as FamNome, Sercodigo, Descricao " +
                                ", OciGarKm, OciKmRevi, Ociquanti, OciprcTab, Ocidtapro, Ocitergar, OciDtRevi" +
                                ", Round(ociprctab *  ociquanti, 2) as Ocisubtot " +
                                " from ORCVENS2 A where orcodigo = " + orcodigo_str;

                                cont_itens = 0;

                                //MessageBox.Show("Itens");
                                dt_itens = Controle.getda_data_vfp(tselect);

                                //lista_venda[cont_venda].Cesta = new QueridoCarro_GravaVenda2.CestaCestaItem[200];
                                ItensList.Clear();

                                if (!dt_itens.HasErrors)
                                {
                                    foreach (DataRow dtRow_i in dt_itens.Rows)    //  **  percorro os itens  **
                                    {

                                        //Itens = new QueridoCarro_GravaVenda2.CestaCestaItem();
                                        Itens = new QueridoCarro_GravaVendaDllV1.CestaCestaItem();

                                        Itens.Ocitem = Convert.ToInt16(dtRow_i[dtRow_i.Table.Columns["ocitem"].Ordinal].ToString());
                                        Itens.Procodigo = dtRow_i[dtRow_i.Table.Columns["procodigo"].Ordinal].ToString();
                                        Itens.Marca = dtRow_i[dtRow_i.Table.Columns["Marca"].Ordinal].ToString();
                                        Itens.FamNome = dtRow_i[dtRow_i.Table.Columns["FamNome"].Ordinal].ToString();
                                        Itens.Sercodigo = Convert.ToInt32(dtRow_i[dtRow_i.Table.Columns["Sercodigo"].Ordinal].ToString());
                                        Itens.Descricao = dtRow_i[dtRow_i.Table.Columns["Descricao"].Ordinal].ToString();
                                        Itens.OciGarKm = Convert.ToInt64(dtRow_i[dtRow_i.Table.Columns["OciGarKm"].Ordinal].ToString());
                                        Itens.OciKmRevi = Convert.ToInt64(dtRow_i[dtRow_i.Table.Columns["OciKmRevi"].Ordinal].ToString());
                                        Itens.Ociquanti = Convert.ToDouble(dtRow_i[dtRow_i.Table.Columns["Ociquanti"].Ordinal].ToString());
                                        Itens.OciprcTab = Convert.ToDouble(dtRow_i[dtRow_i.Table.Columns["OciprcTab"].Ordinal].ToString());
                                        Itens.Ocidtapro = DateTime.Parse(dtRow_i[dtRow_i.Table.Columns["Ocidtapro"].Ordinal].ToString()).FixDataNull();
                                        Itens.Ocitergar = DateTime.Parse(dtRow_i[dtRow_i.Table.Columns["Ocitergar"].Ordinal].ToString()).FixDataNull();
                                        Itens.OciDtRevi = DateTime.Parse(dtRow_i[dtRow_i.Table.Columns["OciDtRevi"].Ordinal].ToString()).FixDataNull();
                                        Itens.Ocisubtot = Convert.ToDouble(dtRow_i[dtRow_i.Table.Columns["Ocisubtot"].Ordinal].ToString());

                                        ItensList.Add(Itens);
                                        cont_itens += 1;
                                    }                                    
                                }

                                //  **  feito por lista par alocar o necessario por venda   **
                                //lista_venda[cont_venda].Cesta = new QueridoCarro_GravaVenda2.CestaCestaItem[ItensList.Count];

                                lista_venda[cont_venda].Cesta = new QueridoCarro_GravaVendaDllV1.CestaCestaItem[ItensList.Count];
                                for (int i = 0; i < ItensList.Count; i++)
                                {
                                    lista_venda[cont_venda].Cesta[i] = new QueridoCarro_GravaVendaDllV1.CestaCestaItem();

                                    lista_venda[cont_venda].Cesta[i].Ocitem = ItensList[i].Ocitem;
                                    lista_venda[cont_venda].Cesta[i].Procodigo = ItensList[i].Procodigo;
                                    lista_venda[cont_venda].Cesta[i].Marca = ItensList[i].Marca;
                                    lista_venda[cont_venda].Cesta[i].FamNome = ItensList[i].FamNome;
                                    lista_venda[cont_venda].Cesta[i].Sercodigo = ItensList[i].Sercodigo;
                                    lista_venda[cont_venda].Cesta[i].Descricao = ItensList[i].Descricao;
                                    lista_venda[cont_venda].Cesta[i].OciGarKm = ItensList[i].OciGarKm;
                                    lista_venda[cont_venda].Cesta[i].OciKmRevi = ItensList[i].OciKmRevi;
                                    lista_venda[cont_venda].Cesta[i].Ociquanti = ItensList[i].Ociquanti;
                                    lista_venda[cont_venda].Cesta[i].OciprcTab = ItensList[i].OciprcTab;
                                    lista_venda[cont_venda].Cesta[i].Ocidtapro = ItensList[i].Ocidtapro;
                                    lista_venda[cont_venda].Cesta[i].Ocitergar = ItensList[i].Ocitergar;
                                    lista_venda[cont_venda].Cesta[i].OciDtRevi = ItensList[i].OciDtRevi;
                                    lista_venda[cont_venda].Cesta[i].Ocisubtot = ItensList[i].Ocisubtot;

                                }
                                cont_venda += 1;

                            }   //  **  10 minutos  **
                            else
                            {
                                //string mensagem = "Tempo Invalido Orcodigo : " + dtRow[dtRow.Table.Columns["orcodigo"].Ordinal].ToString() + DateTime.Now.ToString();
                                //Controle.log("log_qc.txt", mensagem);
                            }

                        }

                        ///MessageBox.Show("Lista Final Qtd : " + orcodigo_list.Count.ToString());
                        if (orcodigo_list.Count > 0)    //  **  se tem pelo menos 1 venda a enviar  **
                        {
                            //  **  ENVIA PARA O WEBSERVICE **
                            try
                            {
                                //c = lista_venda.Where(v => v != null).Count();
                                //c = lista_venda.ToList().Count;
                                //MessageBox.Show(c.ToString());
                                //MessageBox.Show(VendaList.Count().ToString());

                                if (Sobe_Cliente(lista_empcodigo_str))
                                {
                                    ws.Execute(ref lista_venda);

                                    //MessageBox.Show("FOI");
                                    ////  **  se enviou os itens com sucesso remove da lista as os enviadas   **
                                    //cont_deleta = 0;
                                    //while (cont_deleta < orcodigo_list.Count)
                                    //{
                                    //    tselect = "delete from QCENVIAD where QcOrcodigo = " + orcodigo_list[cont_deleta];
                                    //    DataTable dt = Controle.getda_data_vfp(tselect);
                                    //    cont_deleta += 1;
                                    //}

                                    //string mensagem = "Enviei : " + DateTime.Now.ToString();
                                    //Controle.log("log_qc.txt", mensagem);

                                    Apaga_Fila_Envio_Venda(orcodigo_list);

                                }
                                else
                                {
                                    //MessageBox.Show("Erro Sobre CLiente");
                                    //string mensagem = "Erro Sobe Cliente : " + DateTime.Now.ToString();
                                    //Controle.log("log_qc.txt", mensagem);                                
                                }
                            }
                            catch(Exception e)
                            {
                                //MessageBox.Show("Erro Ferpa:" + e.Message);

                                //string mensagem = "Erro : " + e.Message + " | " + DateTime.Now.ToString();
                                //Controle.log("log_qc.txt", mensagem);

                                //  **   adiciono na lista de tentativas    ?????   **
                                //fila_tentativas_orcodigo
                            }

                        }
                        else
                        {
                            //string mensagem = "Lista Vazia : " + DateTime.Now.ToString();
                            //Controle.log("log_qc.txt", mensagem);
                        }
                        //MessageBox.Show("FIM!!!!!!!!!!");
                    }   //  **  se tem os a envia   **

                }   // **   isnull empty cnpj   **

                Limpa_Fila_Dados_Invalidos("VENDA");
            }   //  **  envio_lierado   **
            else
            {
                //string mensagem = "Nada para enviar : " + DateTime.Now.ToString();
                //Controle.log("log_qc.txt", mensagem);
            }
            //return 1;
        }

        public void Quantidade_Itens_Fila()
        {
            //Quantidade_Fila_Envio Fila = new Quantidade_Fila_Envio();
            QueridoCarro_GravaTamFila.FilaEnvio Fila_ws = new QueridoCarro_GravaTamFila.FilaEnvio();
            QueridoCarro_GravaTamFila.GravaTamFila ws = new QueridoCarro_GravaTamFila.GravaTamFila();

            int Quantidade_Os = 0;
            int Quantidade_Entrada = 0;


            //  **  quantidade de os na fila   **
            string tselect = "select count(qcorcodigo) from QCENVIAD";
            DataTable dt = Controle.getda_data_vfp(tselect);

            if (!dt.HasErrors)
            {
                foreach (DataRow dtRow in dt.Rows)
                {
                    Quantidade_Os = Convert.ToInt32(dtRow[0].ToString().NumTryParse());
                }                
            }

            //  **  quantidade de entrada na fila   **
            tselect = "select count(qcnfecodig) from QCENTRAD";
            dt = Controle.getda_data_vfp(tselect);

            if (!dt.HasErrors)
            {
                foreach (DataRow dtRow in dt.Rows)
                {
                    Quantidade_Entrada = Convert.ToInt32(dtRow[0].ToString().NumTryParse());
                }
            }
            
            try
            {
                Fila_ws.OfCnpjCpf = Controle.pega_cnpj().Trim();
                Fila_ws.QuantidadeOs = Quantidade_Os;
                Fila_ws.QuantidadeEntrada = Quantidade_Entrada;
                ws.Execute(ref Fila_ws);
            }
            catch { }

        }

        public bool Envio_Liberado(string modulo)
        {
            string tselect;
            //MessageBox.Show("Envio_Liberado:" + modulo);

            switch (modulo)
            {
                case "VENDA":
                    tselect = "select top 1 QcOrcodigo from QCENVIAD order by QcOrcodigo";
                    break;
                case "COMPRA":
                    tselect = "select top 1 QcNfecodig from qcentrad order by QcNfecodig";
                    break;
                default:
                    return false;
            }

            DataTable dt = Controle.getda_data_vfp(tselect);

            //MessageBox.Show(dt.Rows.Count.ToString() + " " + Controle.IsConnectedToInternet() + " " + dt.HasErrors);

            //  **  verifica se tem os a enviar e se tem internet ativa    **
            if (dt.Rows.Count > 0 && Controle.IsConnectedToInternet() && !dt.HasErrors)
            {
                //MessageBox.Show("ok");
                return true;
            }

            else
            {
                //MessageBox.Show("DEU RUIM!");
                return false;
            }
                
        }

        //  **  apaga da fia de envio das os enviadas com sucesso    **
        public void Apaga_Fila_Envio_Venda(List<String> orcodigo_list)
        {
            string tselect;

            for (int i = 0; i < orcodigo_list.Count; i++)
            {
                tselect = "delete from QCENVIAD where QcOrcodigo = " + orcodigo_list[i];
                DataTable dt = Controle.getda_data_vfp(tselect);
            }
        }

        //  **  apaga da fia de envio as os compras com sucesso    **
        public void Apaga_Fila_Envio_Compra(List<String> nfecodigo_list)
        {
            string tselect;

            for (int i = 0; i < nfecodigo_list.Count; i++)
            {
                tselect = "delete from qcentrad where qcnfecodig = " + '"' + nfecodigo_list[i].Trim() + '"';
                DataTable dt = Controle.getda_data_vfp(tselect);
            }
        }

        public void Sobe_Compras()
        {
            //MessageBox.Show("apenas msg");
          
            string tselect = "";
            string NfeCodigo;
            int cont_compra = 0;
            QueridoCarro_GravaCompra.GravaCompra ws = new QueridoCarro_GravaCompra.GravaCompra();
            QueridoCarro_GravaCompra.CompraSDTCompraSDTItem Compra = new QueridoCarro_GravaCompra.CompraSDTCompraSDTItem();
            List<String> nfecodigo_list = new List<string>();

            QueridoCarro_GravaCompra.CompraSDTCompraSDTItem[] lista_compra = new QueridoCarro_GravaCompra.CompraSDTCompraSDTItem[quantidade_compra];
            List<QueridoCarro_GravaCompra.CestaCompraCestaFornItem> lista_itens_fornecedor = new List<QueridoCarro_GravaCompra.CestaCompraCestaFornItem>();




            if (Envio_Liberado("COMPRA"))   //  **  se tem os a enviar e com acesso a internet  **
            {
                string cnpj = Controle.pega_cnpj().Trim();
                nfecodigo_list.Clear();

                tselect = "select top " + quantidade_compra.ToString().Trim() + " qcnfecodig, Qcnfectrl, nfecodigo, nfemissa, nfevlrnot,  " +
                          "empcgccpf, emprzsocia, empnomfan, empemail, empcep, empendere, empnumero, empcomple, empbairro, empcidade, empestado " +
                          ", empddd, empfone, empdddcel, empcel " +
                          "from qcentrad A, nentrsto B, empresas C where A.qcnfecodig = B.nfecodigo and C.empcodigo = B.empcodigo order by qcnfecodig";

                DataTable dt = Controle.getda_data_vfp(tselect);

                if (dt.Rows.Count > 0 && !dt.HasErrors)
                {
                    foreach (DataRow dtRow in dt.Rows)
                    {
                        //  **  apenas envio os que não tiveram alteração a pelo menos 20 minutos   **
                        DateTime datainicial = DateTime.Parse(dtRow[dtRow.Table.Columns["Qcnfectrl"].Ordinal].ToString()).FixDataNull();
                        DateTime datafinal = DateTime.Now;

                        //if ((datafinal - datainicial).TotalMinutes > 20)
                        if ((datafinal - datainicial).TotalMinutes > tempo_lancamento_compra)
                        {
                            lista_compra[cont_compra] = new QueridoCarro_GravaCompra.CompraSDTCompraSDTItem();
                            lista_compra[cont_compra].OfCnpjCpf = cnpj;
                            lista_compra[cont_compra].NFeCodigo = dtRow[dtRow.Table.Columns["nfecodigo"].Ordinal].ToString();
                            lista_compra[cont_compra].NFeEmissa = DateTime.Parse(dtRow[dtRow.Table.Columns["nfemissa"].Ordinal].ToString()).FixDataNull();
                            lista_compra[cont_compra].NfeVlrNot = Convert.ToDouble(dtRow[dtRow.Table.Columns["nfevlrnot"].Ordinal].ToString());
                            lista_compra[cont_compra].ForCgcCpf = dtRow[dtRow.Table.Columns["empcgccpf"].Ordinal].ToString().Replace("-", "").Replace("/", "").Replace(".", "");
                            lista_compra[cont_compra].ForRzSocia = dtRow[dtRow.Table.Columns["emprzsocia"].Ordinal].ToString();
                            lista_compra[cont_compra].ForNomeFam = dtRow[dtRow.Table.Columns["empnomfan"].Ordinal].ToString();
                            lista_compra[cont_compra].ForEmail = dtRow[dtRow.Table.Columns["empemail"].Ordinal].ToString();
                            lista_compra[cont_compra].ForCep = dtRow[dtRow.Table.Columns["empcep"].Ordinal].ToString();
                            lista_compra[cont_compra].ForEndere = dtRow[dtRow.Table.Columns["empendere"].Ordinal].ToString();
                            lista_compra[cont_compra].ForNum = Convert.ToInt32(dtRow[dtRow.Table.Columns["empnumero"].Ordinal].ToString());
                            lista_compra[cont_compra].ForComple = dtRow[dtRow.Table.Columns["empcomple"].Ordinal].ToString();
                            lista_compra[cont_compra].Forbairro = dtRow[dtRow.Table.Columns["empbairro"].Ordinal].ToString();
                            lista_compra[cont_compra].ForCidade = dtRow[dtRow.Table.Columns["empcidade"].Ordinal].ToString();
                            lista_compra[cont_compra].ForEstado = dtRow[dtRow.Table.Columns["empestado"].Ordinal].ToString();
                            lista_compra[cont_compra].ForDDD = Convert.ToInt16(dtRow[dtRow.Table.Columns["empddd"].Ordinal].ToString());
                            lista_compra[cont_compra].ForFone = Convert.ToInt32(dtRow[dtRow.Table.Columns["empfone"].Ordinal].ToString());
                            lista_compra[cont_compra].ForDDDCel = Convert.ToInt16(dtRow[dtRow.Table.Columns["empdddcel"].Ordinal].ToString());
                            lista_compra[cont_compra].ForCel = Convert.ToInt32(dtRow[dtRow.Table.Columns["empcel"].Ordinal].ToString());

                            NfeCodigo = dtRow[dtRow.Table.Columns["nfecodigo"].Ordinal].ToString();
                            nfecodigo_list.Add(NfeCodigo);
                            lista_itens_fornecedor.Clear();

                            
                            lista_itens_fornecedor = Pega_Itens_Compra(NfeCodigo);
                            lista_compra[cont_compra].CestaCompra = new QueridoCarro_GravaCompra.CestaCompraCestaFornItem[lista_itens_fornecedor.Count];
                            for (int i = 0; i < lista_itens_fornecedor.Count; i++)
                            {
                                lista_compra[cont_compra].CestaCompra[i] = new QueridoCarro_GravaCompra.CestaCompraCestaFornItem();
                                lista_compra[cont_compra].CestaCompra[i].NeiTem = lista_itens_fornecedor[i].NeiTem;
                                lista_compra[cont_compra].CestaCompra[i].NfeProcodigo = lista_itens_fornecedor[i].NfeProcodigo;
                                lista_compra[cont_compra].CestaCompra[i].NfeProMarca = lista_itens_fornecedor[i].NfeProMarca;
                                lista_compra[cont_compra].CestaCompra[i].NfeProFamNome = lista_itens_fornecedor[i].NfeProFamNome;
                                lista_compra[cont_compra].CestaCompra[i].NfeSercodigo = lista_itens_fornecedor[i].NfeSercodigo;
                                lista_compra[cont_compra].CestaCompra[i].NeiDescri = lista_itens_fornecedor[i].NeiDescri;
                                lista_compra[cont_compra].CestaCompra[i].NeiQuanti = lista_itens_fornecedor[i].NeiQuanti;
                                lista_compra[cont_compra].CestaCompra[i].NeiBaseMult = lista_itens_fornecedor[i].NeiBaseMult;
                                lista_compra[cont_compra].CestaCompra[i].NeiPrcUnt = lista_itens_fornecedor[i].NeiPrcUnt;
                                lista_compra[cont_compra].CestaCompra[i].NeiTotal = lista_itens_fornecedor[i].NeiTotal;
                                lista_compra[cont_compra].CestaCompra[i].NeiBaseCalc = lista_itens_fornecedor[i].NeiBaseCalc;
                            }
                            
                            cont_compra += 1;

                        }   //  **  20 minutos  **
                        else
                        {
                            //string mensagem = "COMPRA TEMPO Nada para enviar : " + DateTime.Now.ToString();
                            //Controle.log("log_qc.txt", mensagem);
                        }

                    }

                    //  **  envia   **
                    if (cont_compra > 0)
                    {
                        try
                        {
                            ws.Execute(ref lista_compra);
                            Apaga_Fila_Envio_Compra(nfecodigo_list);

                            //MessageBox.Show("FOI");
                            //string mensagem = "COMPRA Enviei : " + DateTime.Now.ToString();
                            //Controle.log("log_qc.txt", mensagem);
                        }
                        catch
                        {
                            //string mensagem = "COMPRA ERRO a enviar : " + DateTime.Now.ToString();
                            //Controle.log("log_qc.txt", mensagem);
                        }
                        //MessageBox.Show("FOI");
                    }
                    else
                    {
                        //string mensagem = "COMPRA Nada para enviar : " + DateTime.Now.ToString();
                        //Controle.log("log_qc.txt", mensagem);
                    }
                }


                Limpa_Fila_Dados_Invalidos("COMPRA");
            }   //  **   envio liberado **
            //else
            //{
                //string mensagem = "COMPRA Envio Nao liberado : " + DateTime.Now.ToString();
                //Controle.log("log_qc.txt", mensagem);
            //}
            //else MessageBox.Show("NADA");


            
        }

        protected List<QueridoCarro_GravaCompra.CestaCompraCestaFornItem> Pega_Itens_Compra(string NfeCodigo)
        {
            List<QueridoCarro_GravaCompra.CestaCompraCestaFornItem> lista_itens_fornecedor = new List<QueridoCarro_GravaCompra.CestaCompraCestaFornItem>();
            QueridoCarro_GravaCompra.CestaCompraCestaFornItem itens_fornecedor = new QueridoCarro_GravaCompra.CestaCompraCestaFornItem();

            string tselect = "select nfecodigo, neitem, procodigo, sercodigo, neidescri, neiquanti, neibasemul, neiprcunt, neibasecal  " +
                ", ( select x.Marca from marcas X, produtos Y where Y.procodigo = A.procodigo and Y.marcodigo = X.marcodigo ) as Marca " +
                ", ( select x.Famnome from famcad X, produtos Y where Y.procodigo = A.procodigo and Y.famcodigo = X.famcodigo ) as FamNome " +
                ", ( Round(neiprcunt *  neiquanti, 2) + neivfrete + neivdespes ) as neitotal " +
                "from nentrst2 A where nfecodigo = " + '"' + NfeCodigo.Trim() + '"';

            lista_itens_fornecedor.Clear();
            DataTable dt = Controle.getda_data_vfp(tselect);
            if (dt.Rows.Count > 0 && !dt.HasErrors)
            {
                foreach (DataRow dtRow in dt.Rows)
                {
                    itens_fornecedor = new QueridoCarro_GravaCompra.CestaCompraCestaFornItem();
                    itens_fornecedor.NeiTem = Convert.ToInt16(dtRow[dtRow.Table.Columns["neitem"].Ordinal].ToString());
                    itens_fornecedor.NfeProcodigo = dtRow[dtRow.Table.Columns["procodigo"].Ordinal].ToString();
                    itens_fornecedor.NfeProMarca = dtRow[dtRow.Table.Columns["Marca"].Ordinal].ToString();
                    itens_fornecedor.NfeProFamNome = dtRow[dtRow.Table.Columns["FamNome"].Ordinal].ToString();
                    itens_fornecedor.NfeSercodigo = Convert.ToInt32(dtRow[dtRow.Table.Columns["sercodigo"].Ordinal].ToString());
                    itens_fornecedor.NeiDescri = dtRow[dtRow.Table.Columns["neidescri"].Ordinal].ToString();
                    itens_fornecedor.NeiQuanti = Convert.ToDouble(dtRow[dtRow.Table.Columns["neiquanti"].Ordinal].ToString());
                    itens_fornecedor.NeiBaseMult = Convert.ToDouble(dtRow[dtRow.Table.Columns["neibasemul"].Ordinal].ToString());
                    itens_fornecedor.NeiPrcUnt = Convert.ToDouble(dtRow[dtRow.Table.Columns["neiprcunt"].Ordinal].ToString());
                    itens_fornecedor.NeiTotal = Convert.ToDouble(dtRow[dtRow.Table.Columns["neitotal"].Ordinal].ToString().NumTryParse());
                    itens_fornecedor.NeiBaseCalc = Convert.ToInt16(dtRow[dtRow.Table.Columns["neibasecal"].Ordinal].ToString());

                    lista_itens_fornecedor.Add(itens_fornecedor);
                }
            }

            return lista_itens_fornecedor;
        }


        private void add_fila_envio(DateTime dt_inicio, DateTime dt_fim, int tipo, String ofcnpjcpf, short sequencia)
        {
            string tselect1 = null;
            String chave_tabela_str = "";

            switch (tipo)
            {
                case 1: //  **  Vendas  **
                    //  **  Pega as vendas dentro do periodo    **
                    tselect1 = "SELECT orcodigo FROM ORCVENS1 WHERE ORCDATA >= CtoD('" + dt_inicio.ToString("MM/dd/yyyy") +
                                     "') and ORCDATA <= CtoD('" + dt_fim.ToString("MM/dd/yyyy") + "')";
                    break;

                case 2: //  **  Compras  **
                    //  **  Pega as vendas dentro do periodo    **
                    tselect1 = "SELECT nfecodigo FROM NENTRSTO WHERE nfemissa >= CtoD('" + dt_inicio.ToString("MM/dd/yyyy") +
                                     "') and nfemissa <= CtoD('" + dt_fim.ToString("MM/dd/yyyy") + "')";

                    break;
            }

            if (!String.IsNullOrEmpty(tselect1))
            {
                DataTable dt = Controle.getda_data_vfp(tselect1);

                if (dt.Rows.Count > 0 && !dt.HasErrors)
                {
                    foreach (DataRow dtRow in dt.Rows)
                    {
                        switch (tipo)
                        {
                            case 1: //  **  Vendas  **
                                chave_tabela_str = dtRow[dtRow.Table.Columns["orcodigo"].Ordinal].ToString();
                                //  **  Insere na fila de envio **
                                tselect1 = "insert into QCENVIAD (qcorcodigo, Qcorcdata) values (" + chave_tabela_str + ", Ctot('" + "01/01/2001 01:01:01" + "') )";
                                break;

                            case 2: //  **  Compras  **
                                chave_tabela_str = dtRow[dtRow.Table.Columns["nfecodigo"].Ordinal].ToString();
                                //  **  Insere na fila de envio **
                                tselect1 = "insert into QCENTRAD (qcnfecodig, qcnfectrl) values ('" + chave_tabela_str.Trim() + "', Ctot('" + "01/01/2001 01:01:01" + "') )";
                                break;
                        }

                        if (!String.IsNullOrEmpty(tselect1))
                        {
                            //  **  Insere na fila de envio **
                            //tselect = "insert into QCENVIAD (qcorcodigo, Qcorcdata) values (" + orcodigo_str + ", Ctod('" + DateTime.Now.ToString("MM / dd / yyyy") + "') )";
                            dt = Controle.getda_data_vfp(tselect1);
                        }
                    }

                    //  **  Marca como Processado a sequencia de reenvio    **
                    if (!dt.HasErrors)
                    {
                        ProcessaReenviaOs ws = new ProcessaReenviaOs();
                        ws.Execute(ofcnpjcpf, sequencia);
                        
                    }
                    else
                    {
                        //MessageBox.Show("Erro:");
                    }
                }
                else
                {
                    //  **  Não tem nada a enviar no período da baixa pos foi processado com sucesso    **
                    if (dt.Rows.Count == 0 && !dt.HasErrors)
                    {
                        ProcessaReenviaOs ws = new ProcessaReenviaOs();
                        ws.Execute(ofcnpjcpf, sequencia);
                        //MessageBox.Show("ok2");
                    }
                }
            }
            
        }


        //        protected bool check_Fila(List<fila_tentativas_orcodigo> fila_orcodigo)
        //protected bool Fila_Com_tentativas_Validas()
        //{
        //    bool sucesso = false;

        //    if (fila_orcodigo.Count == 0)
        //    {
        //        for (int i = 0; i < fila_orcodigo.Count; i++)
        //        {
        //            if (fila_orcodigo[i].tentativas > 3)
        //                fila_orcodigo.RemoveAt(i);
        //            else
        //                sucesso = true;
        //        }
        //    }
        //    else sucesso = true;

        //    return sucesso;
        //}

        public void Arquivo_Configuracao_AppWkm()
        {
            string line;
            int modulo = 0;
            int pos = 0;
            int tam = 0;
            string palavra = "";
            string arquivo = "APPWKMCONF.DAT";

            if (System.IO.File.Exists(arquivo))
            {
                System.IO.StreamReader file = new System.IO.StreamReader(arquivo);

                while ((line = file.ReadLine()) != null)
                {
                    if (line.Trim() == "#ESTOQUE")
                    {
                        modulo = 1;
                        line = file.ReadLine();
                    }
                    else if (line.Trim() == "#ENVIARCLIENTES")
                    {
                        modulo = 2;
                        line = file.ReadLine();
                    }
                    else if (line.Trim() == "#MARCAS")
                    {
                        modulo = 3;
                        line = file.ReadLine();
                    }
                    else if (line.Trim() == "#RECEBERCLIENTES")
                    {
                        modulo = 4;
                        line = file.ReadLine();
                    }
                    else if (line.Trim() == "#RECEBERVENDAS")
                    {
                        modulo = 5;
                        line = file.ReadLine();
                    }
                    else if (line.Trim() == "#FAMILIAS")
                    {
                        modulo = 6;
                        line = file.ReadLine();
                    }
                    else if (line.Trim() == "#FIPE")
                    {
                        modulo = 7;
                        line = file.ReadLine();
                    }
                    else if (line.Trim() == "#VEICULOSCLIENTES")
                    {
                        modulo = 8;
                        line = file.ReadLine();
                    }
                    else if (line.Trim() == "#FUNCIONARIOS")
                    {
                        modulo = 9;
                        line = file.ReadLine();
                    }
                    else if (line.Trim() == "#VENDAVALORES")
                    {
                        modulo = 10;
                        line = file.ReadLine();
                    }

                    pos = line.IndexOf(':');
                    tam = line.Length - pos - 1;
                    palavra = line.Substring(0, pos);

                    //MessageBox.Show(modulo.ToString() + "||" + palavra);

                    switch (palavra)
                    {
                        case "INTERVALO ENVIO":
                            if (modulo == 1)
                                time_enviarprodutos = Convert.ToInt32(line.Substring(pos + 1, tam));
                            else if (modulo == 2)
                                time_enviarclientes = Convert.ToInt32(line.Substring(pos + 1, tam));
                            else if (modulo == 3)
                                time_enviarmarcas = Convert.ToInt32(line.Substring(pos + 1, tam));
                            else if (modulo == 4)
                                time_receberclientes = Convert.ToInt32(line.Substring(pos + 1, tam));
                            else if (modulo == 5)
                                time_recebervendas = Convert.ToInt32(line.Substring(pos + 1, tam));
                            else if (modulo == 6)
                                time_enviarfamilias = Convert.ToInt32(line.Substring(pos + 1, tam));
                            else if (modulo == 7)
                                time_enviartabelafipe = Convert.ToInt32(line.Substring(pos + 1, tam));
                            else if (modulo == 8)
                                time_enviarveiculosclientes = Convert.ToInt32(line.Substring(pos + 1, tam));
                            else if (modulo == 9)
                                time_enviarfuncionarios = Convert.ToInt32(line.Substring(pos + 1, tam));
                            else if (modulo == 10)
                                time_vendavalores = Convert.ToInt32(line.Substring(pos + 1, tam));
                            break;
                    }

                }

                file.Close();
                //MessageBox.Show(quantidade_venda.ToString() + " | " + intervalo_envio_venda.ToString() + " | " + tempo_lancamento_venda.ToString());
                //MessageBox.Show(quantidade_compra.ToString() + " | " + intervalo_envio_compra.ToString() + " | " + tempo_lancamento_compra.ToString());
            }
            else   //   ** se nao existe o arquivo crio com os campos default para usar como modelo caso quera alterar   **
            {
                try
                {
                    string dados_arquivo = "#ESTOQUE" + "\n" +
                                           "INTERVALO ENVIO:" + time_enviarprodutos.ToString().Trim() + "\n" +
                                           "#ENVIARCLIENTES" + "\n" +
                                           "INTERVALO ENVIO:" + time_enviarclientes.ToString().Trim() + "\n" +
                                           "#MARCAS" + "\n" +
                                           "INTERVALO ENVIO:" + time_enviarmarcas.ToString().Trim() + "\n" +
                                           "#RECEBERCLIENTES" + "\n" +
                                           "INTERVALO ENVIO:" + time_receberclientes.ToString().Trim() + "\n" +
                                           "#RECEBERVENDAS" + "\n" +
                                           "INTERVALO ENVIO:" + time_recebervendas.ToString().Trim() + "\n" +
                                           "#FAMILIAS" + "\n" +
                                           "INTERVALO ENVIO:" + time_enviarfamilias.ToString().Trim() + "\n" +
                                           "#FIPE" + "\n" +
                                           "INTERVALO ENVIO:" + time_enviartabelafipe.ToString().Trim() + "\n" +
                                           "#VEICULOSCLIENTES" + "\n" +
                                           "INTERVALO ENVIO:" + time_enviarveiculosclientes.ToString().Trim() + "\n" +
                                           "#FUNCIONARIOS" + "\n" +
                                           "INTERVALO ENVIO:" + time_enviarfuncionarios.ToString().Trim() + "\n" +
                                           "#VENDAVALORES" + "\n" +
                                           "INTERVALO ENVIO:" + time_vendavalores.ToString().Trim();

                    System.IO.TextWriter txt = System.IO.File.AppendText(arquivo);
                    txt.WriteLine(dados_arquivo);
                    txt.Close();
                }
                catch { }
            }

        }
    }

    public class Quantidade_Fila_Envio
    {
        public int Quantidade_Os;
        public int Quantidade_Entrada;

    }

    public partial class logReenvioArquivo
    {
        private static String nomeArquivo = "reenvOs.dat";

        public DateTime getDataProcesado()
        {
            DateTime dataout = default(DateTime);
            if (System.IO.File.Exists(nomeArquivo))
            {
                String dataprocessado = File.ReadAllText(nomeArquivo);


                if (DateTime.TryParse(dataprocessado, out dataout)) { }
                else
                    dataout = default(DateTime);

                return dataout;
            }

            return dataout;
        }

        public void setDataProcesado(DateTime dataprocessado)
        {
            System.IO.File.Create(nomeArquivo).Close();
            System.IO.TextWriter arquivo = System.IO.File.AppendText(nomeArquivo);
            arquivo.WriteLine(dataprocessado.ToString("dd/MM/yyyy"));
            arquivo.Close();
        }

    }

}

