﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace DllControleV1
{
    class ExtraFoxPro
    {

        //  **  cria os arquivos que o fox/gx necessitam para tratar datas, sem eles da erro no select  **
        public static void FoxDbfPrg()
        {
            string pathfile = "";

            pathfile = Directory.GetCurrentDirectory() + '\\' + "gxddesc.prg";
            if (!System.IO.File.Exists(pathfile))
            {
                try
                {
                    System.IO.File.Create(pathfile).Close();
                    System.IO.TextWriter arquivo = System.IO.File.AppendText(pathfile);
                    arquivo.WriteLine("PARAMETER gx_d_dt");
                    arquivo.WriteLine("");
                    arquivo.WriteLine("IF TYPE(" + "\"" + "gx_d_dt" + "\"" + ") = " + "\"" + "D" + "\"");
                    arquivo.WriteLine(" RETURN STR(CTOD('01/01/9999') - gx_d_dt, 8)+" + "\"" + ".000000" + "\"");
                    arquivo.WriteLine("ENDIF");
                    arquivo.WriteLine("");
                    arquivo.WriteLine("IF TYPE(" + "\"" + "gx_d_dt" + "\"" + ") = " + "\"" + "T" + "\"");
                    arquivo.WriteLine(" RETURN STR(({^9999-01-01 00:00:00a} - gx_d_dt)/60/60/24, 15, 6) ");
                    arquivo.WriteLine("ENDIF");
                    arquivo.WriteLine("");
                    arquivo.WriteLine("RETURN gx_d_dt");
                    arquivo.Close();
                }
                catch { }
            }

            pathfile = Directory.GetCurrentDirectory() + '\\' + "Gxdesc.prg";
            if (!System.IO.File.Exists(pathfile))
            {
                try
                {
                    System.IO.File.Create(pathfile).Close();
                    System.IO.TextWriter arquivo = System.IO.File.AppendText(pathfile);
                    arquivo.WriteLine("PARAMETER l");
                    arquivo.WriteLine("");
                    arquivo.WriteLine(" gxi = 1");
                    arquivo.WriteLine(" gx_rs = " + "\"" + "\"");
                    arquivo.WriteLine(" gx_long = LEN(l)");
                    arquivo.WriteLine("");
                    arquivo.WriteLine(" DO WHILE gxi <= gx_long");
                    arquivo.WriteLine("     gx_rs = gx_rs + chr(254 - ASC(SUBSTR(l,gxi,1)))");
                    arquivo.WriteLine("     gxi = gxi + 1");
                    arquivo.WriteLine(" ENDDO");
                    arquivo.WriteLine("");
                    arquivo.WriteLine("RETURN gx_rs");
                    arquivo.Close();
                }
                catch { }
            }


        }
    }
}
