﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Data.OleDb;
using System.Data;
using System.Data.Odbc;
using System.Net;

using Newtonsoft.Json;
using System.Threading;
using System.Linq;
using System.Collections.Specialized;
using System.Text.RegularExpressions;

namespace DllControleV1
{
    class AppWkm
    {
        /* FEITO */
        public class Clientes
        {
            public int Id { get; set; }
            public int OficinaId { get; set; }
            public string RazaoSocial { get; set; }
            public string NomeFantasia { get; set; }
            public string CnpjCpf { get; set; }
            public string InscricaoEstadual { get; set; }
            public string Endereco { get; set; }
            public int Numero { get; set; }
            public string Complemento { get; set; }
            public string Bairro { get; set; }
            public string Cidade { get; set; }
            public string Estado { get; set; }
            public string Cep { get; set; }
            public int Ddd { get; set; }
            public string Telefone { get; set; }
            public string Fax { get; set; }
            public string Email { get; set; }
            public int Codigo { get; set; }
            public int CelularDdd { get; set; }
            public string Celular { get; set; }
            public DateTime DataNascimento { get; set; }
            public string Sexo { get; set; }
            public int MunicipioIbge { get; set; }
        }

        /* FEITO */
        public class VendasApp
        {
            public int Id { get; set; }
            public int OficinaId { get; set; }
            public string FuncionarioId { get; set; }
            public string CarroFipeId { get; set; }
            public string CombustivelId { get; set; }
            public int ClienteId { get; set; }
            public int NumeroOrcamento { get; set; }
            public string Ano { get; set; }
            public decimal Quilometragem { get; set; }
            public string Observacao { get; set; }
            public string Defeitos { get; set; }
            public string Avarias { get; set; }
            public string Placa { get; set; }
            public int WorkMotorId { get; set; }
            public DateTime DataEmissao { get; set; }
            public string Cor { get; set; }
            
            public string Status { get; set; }
            public decimal TotalPecas { get; set; }
            public decimal TotalServicos { get; set; }
            public decimal Total { get; set; }
            public decimal DescProdutos { get; set; }
            public decimal DescServicos { get; set; }
            
        }
               
        /* FEITO */
        public class Marcas
        {
            public int Id { get; set; }
            public int OficinaId { get; set; }
            public string Codigo { get; set; }
            public string Tipo { get; set; }
            public string Descricao { get; set; }
            public DateTime DataModificacao { get; set; }
        }

        /* FEITO */
        public class Familias
        {
            public int Id;
            public int OficinaId;
            public string Codigo;
            public string Descricao;
            public DateTime DataModificacao;
        }

        /* FEITO */
        public class Veiculos
        {
            public int Id = 0;
            public string ClienteId { get; set; }
            public string CarroFipeId { get; set; }
            public string CombustivelId { get; set; }
            public string Placa { get; set;}
            public string Ano { get; set; }
            public string Cor { get; set; }
            public string Quilometragem { get; set; }
            public string Descricao { get; set; }
            public string CombustivelTipo { get; set; }
            public string Codigo { get; set;}
        }

        /* FEITO */
        public class Produtos
        {
            public int Id { get; set; }
            public int OficinaId { get; set; }
            public int MarcaId { get; set; }
            public int FamiliaId { get; set; }
            public string Codigo { get; set; }
            public string Descricao { get; set; }
            public string ReferenciaFabricante { get; set; }
            public string Modelo { get; set; }
            public string Tamanho { get; set; }
            public decimal Peso { get; set; }
            public string Local { get; set; }
            public decimal ValorVenda { get; set; }
            public string Unidade { get; set; }
            public DateTime DataModificacao { get; set; }
            public decimal Quantidade { get; set; }
            public string Aplicacao { get; set; }
            public string CodigoReferencia { get; set; }
            public string FamiliaDescricao { get; set; }
            public string MarcaDescricao { get; set; }
            public string CodigoBarras { get; set; }
        }

        /* FEITO */
        public class Funcionarios
        {
            public int Id { get; set; }
            public int OficinaId { get; set; }
            public string Nome { get; set; }
            public string Senha { get; set; }
            public DateTime DataModificacao { get; set; }
            public string Cargo { get; set; }
            public string Codigo { get; set; }
            public bool Logado { get; set; }
            public bool Ativo { get; set; }
            public DateTime DataDemissão { get; set; }
        }

        /* FEITO */
        public class TabelaFipe
        {
            public int Id { get; set; }
            public int OficinaId { get; set; }
            public string CodigoFipe { get; set; }
            public string Descricao { get; set; }
            public string AnoInicial { get; set; }
            public string AnoFinal { get; set; }
        }

        /* FEITO */
        public static bool EnviarFipe()
        {
            string cnpj = "";
            cnpj = Controle.pega_cnpj();
            //var url = "http://201.77.177.34/wkm/InsercaoCarroFipeLista2";
            var url = "http://192.168.0.115:62838/wkm/InsercaoCarroFipeLista2";

            if (cnpj != "")
            {
                try
                {
                    string arquivo = "AppWkmCarroFipe.json";
                    List<TabelaFipe> listaFipe = new List<TabelaFipe>();
                    List<TabelaFipe> lfsucesso = new List<TabelaFipe>();
                    List<TabelaFipe> lferro = new List<TabelaFipe>();
                    string selectFipe = "SELECT Objcodigo, Objeto, Objmodelo FROM Objetmat";
                    DataTable fipe = new DataTable();
                    fipe = Controle.getda_data_vfp(selectFipe);
                    int i;

                    if (fipe.Rows.Count > 0)
                    {
                        /* POPULANDO JSON  */
                        for (i = 0; i < fipe.Rows.Count; i++)
                        {
                            listaFipe.Add(new TabelaFipe
                            {
                                Id = 0,
                                OficinaId = 0,
                                CodigoFipe = fipe.Rows[i][0].ToString().Trim().Replace("\'", " ").Replace("&", "E").Replace("\\\\", "/").Replace("\"", " "),
                                Descricao = fipe.Rows[i][1].ToString().Trim().Replace("\'", " ").Replace("&", "E").Replace("\\\\", "/").Replace("\"", " "),
                                AnoInicial = "0",
                                AnoFinal = "0"
                            });
                        }

                        /* FIM POPULAÇÃO DO JSON */

                        if (System.IO.File.Exists(arquivo))
                        {
                            var fipearquivojson = File.ReadAllText(arquivo);
                            List<TabelaFipe> listafipearquivo = new List<TabelaFipe>();
                            listafipearquivo = JsonConvert.DeserializeObject<List<TabelaFipe>>(fipearquivojson);
                            List<TabelaFipe> listaAenviar = new List<TabelaFipe>();

                            foreach (TabelaFipe lf in listaFipe)
                            {
                                var lfarquivo = listafipearquivo.FirstOrDefault(x => x.CodigoFipe == lf.CodigoFipe);

                                if (lfarquivo == null)
                                {
                                    listaAenviar.Add(lf);
                                    continue;
                                }

                                if (lf.Descricao != lfarquivo.Descricao) { listaAenviar.Add(lf); continue; }
                                if (lf.AnoFinal != lfarquivo.AnoFinal) { listaAenviar.Add(lf); continue; }
                                if (lf.AnoInicial != lfarquivo.AnoInicial) { listaAenviar.Add(lf); continue; }
                            }

                            if (listaAenviar.Count == 0)
                            {
                                /********************* NÃO FAZ NADA *********************/
                                return true;
                                /********************* NÃO FAZ NADA *********************/
                            }
                            else
                            {
                                /********************* MÉTODO DE ENVIO DE DADOS *********************/

                                while (listaAenviar.Count() > 0)
                                {
                                    var listaDividida = listaAenviar.Take(200);
                                    var fipeEnviar = JsonConvert.SerializeObject(listaDividida);
                                    var jsonEnviar = ("{'cnpj' : '" + cnpj.Trim() + "', carro: '" + fipeEnviar + "'}");

                                    var result = EnviarDados(jsonEnviar, url);

                                    if (result.ToUpper() != "TRUE")
                                    {
                                        lferro.AddRange(listaDividida);
                                    }
                                    else
                                    {
                                        lfsucesso.AddRange(listaDividida);
                                    }

                                    listaAenviar.RemoveRange(0, listaDividida.Count());
                                }

                                if (lfsucesso.Count > 0 && lferro.Count == 0)
                                {
                                    using (StreamWriter writer = new StreamWriter("AppWkmCarroFipe.json"))
                                    {
                                        writer.WriteLine(JsonConvert.SerializeObject(listaFipe));
                                    }
                                    return true;
                                }
                                
                                if (lferro.Count > 0)
                                {
                                    using (StreamWriter writer = new StreamWriter("AppWkmCarroFipeErroEnvio.json"))
                                    {
                                        writer.WriteLine(JsonConvert.SerializeObject(lferro));
                                    }
                                } 

                                if (lfsucesso.Count > 0)
                                {
                                    using (StreamWriter writer = new StreamWriter("AppWkmCarroFipe.json"))
                                    {
                                        writer.WriteLine(JsonConvert.SerializeObject(lfsucesso));
                                    }
                                }

                                /******************* FIM MÉTODO DE ENVIO DE DADOS *******************/
                            }
                            return true;
                        }
                        else
                        {
                            /********************* MÉTODO DE ENVIO DE DADOS *********************/

                            while (listaFipe.Count() > 0)
                            {
                                var listaDividida = listaFipe.Take(200);
                                var fipeEnviar = JsonConvert.SerializeObject(listaDividida);
                                var jsonEnviar = ("{'cnpj' : '" + cnpj.Trim() + "', carro: '" + fipeEnviar + "'}");

                                var result = EnviarDados(jsonEnviar, url);

                                if (result.ToUpper() != "TRUE")
                                {
                                    lferro.AddRange(listaDividida);
                                }
                                else
                                {
                                    lfsucesso.AddRange(listaDividida);
                                }

                                listaFipe.RemoveRange(0, listaDividida.Count());
                            }

                            if (lferro.Count > 0)
                            {
                                using (StreamWriter writer = new StreamWriter("AppWkmCarroFipeErroEnvio.json"))
                                {
                                    writer.WriteLine(JsonConvert.SerializeObject(lferro));
                                }
                            } 

                            if (lfsucesso.Count > 0)
                            {
                                using (StreamWriter writer = new StreamWriter("AppWkmCarroFipe.json"))
                                {
                                    writer.WriteLine(JsonConvert.SerializeObject(lfsucesso));
                                }
                            }
                            
                        }
                    }
                    return true;
                }
                catch (Exception Erro)
                {
                    using (StreamWriter writer = new StreamWriter("AppWkmCarroFipeErroCatch.json"))
                    {
                        writer.WriteLine("Erro envio de dados: " + Erro);
                    }
                    return false;
                }
                
            }
            return true;
        }

        /* FEITO */
        public static bool EnviarClientes()
        {
            string cnpj = "";
            cnpj = Controle.pega_cnpj();
            //string url = "http://192.168.0.115/wkm/InsercaoClienteLista2";
            string url = "http://192.168.0.115:62838/wkm/InsercaoClienteLista2";
            //string url = "http://201.77.177.34/Wkm/InsercaoClienteLista2";

            if (cnpj != "")
            {
                try
                {
                    Clientes clientesJson = new Clientes();

                    List<Clientes> listaClientes = new List<Clientes>();
                    List<Clientes> lcsucesso = new List<Clientes>();
                    List<Clientes> lcerro = new List<Clientes>();

                    DataTable retorno = new DataTable();
                    string selectClientes = "SELECT emprzsocia, empnomfan, empcgccpf, empnscest, empendere, empnumero, empcomple, empbairro, empcidade, empestado, empcep, empddd, empfone, empfax, empemail, empcodigo, empdddcel, empcel, empdtnasc, empsexo, empresas.empcidcodi FROM empresas";
                    DataTable clientes = new DataTable();
                    clientes = Controle.getda_data_vfp(selectClientes);

                    int i;
                    string arquivo = "AppWkmClientes.json";

                    /* POPULANDO JSON PARA ENVIAR PARA WEB */
                    for (i = 0; i < clientes.Rows.Count; i++)
                    {
                        listaClientes.Add(new Clientes
                        {
                            Id = 0,
                            OficinaId = 0,
                            RazaoSocial = LimpezaGeral(clientes.Rows[i][0].ToString().Trim().Replace("Ç", "C").Replace("ç", "C")),
                            NomeFantasia = LimpezaGeral(clientes.Rows[i][1].ToString().Trim().Replace("Ç", "C").Replace("ç", "C")),
                            CnpjCpf = LimpezaGeral(clientes.Rows[i][2].ToString().Trim()),
                            InscricaoEstadual = LimpezaGeral(clientes.Rows[i][3].ToString().Trim()),
                            Endereco = LimpezaGeral(clientes.Rows[i][4].ToString().Trim().Replace("Ç", "C").Replace("ç", "C")),
                            Numero = Convert.ToInt32(clientes.Rows[i][5]),
                            Complemento = LimpezaGeral(clientes.Rows[i][6].ToString().Trim().Replace("Ç", "C").Replace("ç", "C")),
                            Bairro = LimpezaGeral(clientes.Rows[i][7].ToString().Trim().Replace("Ç", "C").Replace("ç", "C")),
                            Cidade = LimpezaGeral(clientes.Rows[i][8].ToString().Trim().Replace("Ç", "C").Replace("ç", "C")),
                            Estado = LimpezaGeral(clientes.Rows[i][9].ToString().Trim().Replace("Ç", "C").Replace("ç", "C")),
                            Cep = LimpezaGeral(clientes.Rows[i][10].ToString().Trim()),
                            Ddd = Convert.ToInt32(clientes.Rows[i][11]),
                            Telefone = LimpezaGeral(clientes.Rows[i][12].ToString().Trim()),
                            Fax = LimpezaGeral(clientes.Rows[i][13].ToString().Trim()),
                            Email = LimpezaGeral(clientes.Rows[i][14].ToString().Trim().Replace("Ç", "C").Replace("ç", "C")),
                            Codigo = Convert.ToInt32(clientes.Rows[i][15]),
                            CelularDdd = Convert.ToInt32(clientes.Rows[i][16]),
                            Celular = LimpezaGeral(clientes.Rows[i][17].ToString().Trim()),
                            DataNascimento = Convert.ToDateTime(clientes.Rows[i][18]),
                            Sexo = LimpezaGeral(clientes.Rows[i][19].ToString().Trim()),
                            MunicipioIbge = Convert.ToInt32(clientes.Rows[i][20])
                        });

                    }
                    /* FIM POPULAÇÃO DO JSON */

                    if (System.IO.File.Exists(arquivo))
                    {
                        string clientesarquivojson = File.ReadAllText(arquivo);
                        List<Clientes> listaclientesarquivo = new List<Clientes>();
                        listaclientesarquivo = JsonConvert.DeserializeObject<List<Clientes>>(clientesarquivojson);
                        List<Clientes> listaAenviar = new List<Clientes>();

                        foreach (Clientes lc in listaClientes)
                        {
                            var lcarquivo = listaclientesarquivo.FirstOrDefault(x => x.Codigo == lc.Codigo);

                            if (lcarquivo == null)
                            {
                                listaAenviar.Add(lc);
                                continue;
                            }

                            if (lc.Bairro != lcarquivo.Bairro) { listaAenviar.Add(lc); continue; }
                            if (lc.Celular != lcarquivo.Celular) { listaAenviar.Add(lc); continue; }
                            if (lc.CelularDdd != lcarquivo.CelularDdd) { listaAenviar.Add(lc); continue; }
                            if (lc.Cep != lcarquivo.Cep) { listaAenviar.Add(lc); continue; }
                            if (lc.Cidade != lcarquivo.Cidade) { listaAenviar.Add(lc); continue; }
                            if (lc.CnpjCpf != lcarquivo.CnpjCpf) { listaAenviar.Add(lc); continue; }
                            if (lc.Complemento != lcarquivo.Complemento) { listaAenviar.Add(lc); continue; }
                            if (lc.DataNascimento != lcarquivo.DataNascimento) { listaAenviar.Add(lc); continue; }
                            if (lc.Ddd != lcarquivo.Ddd) { listaAenviar.Add(lc); continue; }
                            if (lc.Email != lcarquivo.Email) { listaAenviar.Add(lc); continue; }
                            if (lc.Endereco != lcarquivo.Endereco) { listaAenviar.Add(lc); continue; }
                            if (lc.Estado != lcarquivo.Estado) { listaAenviar.Add(lc); continue; }
                            if (lc.Fax != lcarquivo.Fax) { listaAenviar.Add(lc); continue; }
                            if (lc.InscricaoEstadual != lcarquivo.InscricaoEstadual) { listaAenviar.Add(lc); continue; }
                            if (lc.NomeFantasia != lcarquivo.NomeFantasia) { listaAenviar.Add(lc); continue; }
                            if (lc.Numero != lcarquivo.Numero) { listaAenviar.Add(lc); continue; }
                            if (lc.RazaoSocial != lcarquivo.RazaoSocial) { listaAenviar.Add(lc); continue; }
                            if (lc.Sexo != lcarquivo.Sexo) { listaAenviar.Add(lc); continue; }
                            if (lc.Telefone != lcarquivo.Telefone) { listaAenviar.Add(lc); continue; }
                            if (lc.MunicipioIbge != lcarquivo.MunicipioIbge) { listaAenviar.Add(lc); continue; }
                        }

                        if (listaAenviar.Count == 0)
                        {
                            /********************* NÃO FAZ NADA *********************/

                            return true;
                            /********************* NÃO FAZ NADA *********************/
                        }
                        else
                        {
                            /********************* MÉTODO DE ENVIO DE DADOS *********************/

                            while (listaAenviar.Count() > 0)
                            {
                                var listaDividida = listaAenviar.Take(200);
                                var clientesEnviar = JsonConvert.SerializeObject(listaDividida);
                                var jsonEnviar = ("{'cnpj' : '" + cnpj.Trim() + "', cliente: '" + clientesEnviar + "'}");

                                var result = EnviarDados(jsonEnviar, url);

                                if (result.ToUpper() != "TRUE")
                                {
                                    lcerro.AddRange(listaDividida);
                                } else
                                {
                                    lcsucesso.AddRange(listaDividida);
                                }

                                listaAenviar.RemoveRange(0, listaDividida.Count()); 
                            }
                            /******************* FIM MÉTODO DE ENVIO DE DADOS *******************/

                            if(lcsucesso.Count > 0 && lcerro.Count == 0)
                            {
                                using (StreamWriter writer = new StreamWriter("AppWkmClientes.json"))
                                {
                                    writer.WriteLine(JsonConvert.SerializeObject(listaClientes));
                                }

                                return true;
                            }

                            if (lcerro.Count > 0)
                            {
                                using (StreamWriter writer = new StreamWriter("AppWkmClientesErro.json"))
                                {
                                    writer.WriteLine(JsonConvert.SerializeObject(lcerro));
                                }

                                return true;
                            } 
                        }
                    }

                    else
                    {
                        /********************* MÉTODO DE ENVIO DE DADOS *********************/
                        
                        while (listaClientes.Count() > 0)
                        {
                            var listaDividida = listaClientes.Take(200);
                            var clientesEnviar = JsonConvert.SerializeObject(listaDividida);
                            var jsonEnviar = ("{'cnpj' : '" + cnpj.Trim() + "', cliente: '" + clientesEnviar + "'}");

                            var result = EnviarDados(jsonEnviar, url);

                            if (result.ToUpper() != "TRUE")
                            {
                                lcerro.AddRange(listaDividida);
                            } else
                            {
                                lcsucesso.AddRange(listaDividida);
                            }
                                                        
                            listaClientes.RemoveRange(0, listaDividida.Count());
                        }
                        /******************* FIM MÉTODO DE ENVIO DE DADOS *******************/
                        

                        if (lcerro.Count == 0 && lcsucesso.Count > 0)
                        {
                            using (StreamWriter writer = new StreamWriter("appWkmClientes.json"))
                            {
                                writer.WriteLine(JsonConvert.SerializeObject(listaClientes));
                            }

                            return true;
                        }

                        if (lcerro.Count > 0)
                        {
                            using (StreamWriter writer = new StreamWriter("appWkmClientesErro.json"))
                            {
                                writer.WriteLine(JsonConvert.SerializeObject(lcerro));
                            }
                        }

                        if (lcsucesso.Count > 0)
                        {
                            using (StreamWriter writer = new StreamWriter("appWkmClientes.json"))
                            {
                                writer.WriteLine(JsonConvert.SerializeObject(lcerro));
                            }
                        }
                                                
                        return true;
                    }
                    return true;
                }
                catch (Exception erro)
                {
                    using (StreamWriter writer = new StreamWriter("AppWkmClientesErro.json"))
                    {
                        writer.WriteLine("Erro de Envio: " + DateTime.Today + erro );
                    }
                    return false;
                }
            }
            return true;
        }

        /* FEITO */
        public static void EnviarVeiculos()
        {
            var cnpj = "";
            cnpj = Controle.pega_cnpj();
            var url = "http://192.168.0.115:62838/wkm/InsercaoVeiculoClienteLista2";
            //var url = "http://201.77.177.34/Wkm/InsercaoVeiculoClienteLista2";

            if (cnpj != "")
            {
                try
                {
                    int i;
                    string arquivo = "AppWkmVeiculos.json";
                    Veiculos veiculosJson = new Veiculos();
                    List<Veiculos> listaVeiculos = new List<Veiculos>();
                    List<Veiculos> listaVeiculos2 = new List<Veiculos>();
                    List<Veiculos> lvsucesso = new List<Veiculos>();
                    List<Veiculos> lverro = new List<Veiculos>();
                    string selectVeiculos = "SELECT empveicu.empcodigo, empveicu.empvobjcod, empveicu.EmpVComb, empveicu.empvplaca, empveicu.empVano, empveicu.EmpvCor, empveicu.empvKm, empveicu.EmpTpCombu, empveicu.empvobjeto FROM empveicu, Objetmat, empresas WHERE empveicu.empvobjcod = Objetmat.Objcodigo AND empresas.empcodigo = empveicu.empcodigo"; 
                    DataTable veiculos = new DataTable();
                    veiculos = Controle.getda_data_vfp(selectVeiculos);
                    
                    if (veiculos.Rows.Count > 0)
                    {
                        /* POPULANDO JSON PARA ENVIAR PARA WEB */
                        for (i = 0; i < veiculos.Rows.Count; i++)
                        {
                            listaVeiculos.Add(new Veiculos
                            {
                                Id = 0,
                                ClienteId = veiculos.Rows[i][0].ToString().Trim().Replace("\'", " ").Replace("&", "E").Replace("\\\\", "/"),
                                CarroFipeId = "0",
                                CombustivelId = ValidaAnoModeloFipe(veiculos.Rows[i][2].ToString().Trim().Replace("\'", " ").Replace("&", "E").Replace("\\\\", "/")),
                                Placa = MascaraPlaca(veiculos.Rows[i][3].ToString().Trim().Replace("\'", " ").Replace("&", "E").Replace("\\\\", "/")),
                                Codigo = veiculos.Rows[i][1].ToString().Trim().Replace("\'", " ").Replace("&", "E").Replace("\\\\", "/"),
                                Ano = ValidaAnoModeloFipe(veiculos.Rows[i][4].ToString().Trim().Replace("\'", " ").Replace("&", "E").Replace("\\\\", "/")),
                                Cor = veiculos.Rows[i][5].ToString().Trim().Replace("\'", " ").Replace("&", "E").Replace("\\\\", "/"),
                                Quilometragem = veiculos.Rows[i][6].ToString().Trim().Replace("\'", " ").Replace("&", "E").Replace("\\\\", "/"),
                                Descricao = ValidaDescricao(veiculos.Rows[i][7].ToString().Trim().Replace("\'", " ").Replace("&", "E").Replace("\\\\", "/")),
                                CombustivelTipo = veiculos.Rows[i][1].ToString().Trim().Replace("\'", " ").Replace("&", "E").Replace("\\\\", "/")
                            });

                            listaVeiculos2.Add(new Veiculos
                            {
                                Id = 0,
                                ClienteId = veiculos.Rows[i][0].ToString().Trim().Replace("\'", " ").Replace("&", "E").Replace("\\\\", "/"),
                                CarroFipeId = "0",
                                CombustivelId = ValidaAnoModeloFipe(veiculos.Rows[i][2].ToString().Trim().Replace("\'", " ").Replace("&", "E").Replace("\\\\", "/")),
                                Placa = MascaraPlaca(veiculos.Rows[i][3].ToString().Trim().Replace("\'", " ").Replace("&", "E").Replace("\\\\", "/")),
                                Codigo = veiculos.Rows[i][1].ToString().Trim().Replace("\'", " ").Replace("&", "E").Replace("\\\\", "/"),
                                Ano = ValidaAnoModeloFipe(veiculos.Rows[i][4].ToString().Trim().Replace("\'", " ").Replace("&", "E").Replace("\\\\", "/")),
                                Cor = veiculos.Rows[i][5].ToString().Trim().Replace("\'", " ").Replace("&", "E").Replace("\\\\", "/"),
                                Quilometragem = veiculos.Rows[i][6].ToString().Trim().Replace("\'", " ").Replace("&", "E").Replace("\\\\", "/"),
                                Descricao = ValidaDescricao(veiculos.Rows[i][7].ToString().Trim().Replace("\'", " ").Replace("&", "E").Replace("\\\\", "/")),
                                CombustivelTipo = veiculos.Rows[i][1].ToString().Trim().Replace("\'", " ").Replace("&", "E").Replace("\\\\", "/")
                            });

                        }
                        /* FIM POPULAÇÃO DO JSON */
                        if (System.IO.File.Exists(arquivo))
                        {
                            var veiculosrquivojson = File.ReadAllText(arquivo);
                            List<Veiculos> listaveiculosarquivo = new List<Veiculos>();
                            listaveiculosarquivo = JsonConvert.DeserializeObject<List<Veiculos>>(veiculosrquivojson);
                            List<Veiculos> listaAenviar = new List<Veiculos>();

                            if(JsonConvert.SerializeObject(listaveiculosarquivo) == JsonConvert.SerializeObject(listaVeiculos))
                            {
                                
                                /* NÃO FAÇA NADA - DADOS IGUAIS*/
                            } else
                            {
                                int loop = 0;

                                while(listaVeiculos.Count > loop)
                                {
                                    foreach (Veiculos v in listaveiculosarquivo)
                                    {
                                        if (listaVeiculos[loop].Ano == v.Ano && listaVeiculos[loop].CarroFipeId == v.CarroFipeId
                                            && listaVeiculos[loop].ClienteId == v.ClienteId && listaVeiculos[loop].Codigo == v.Codigo
                                            && listaVeiculos[loop].CombustivelId == v.CombustivelId && listaVeiculos[loop].CombustivelTipo == v.CombustivelTipo
                                            && listaVeiculos[loop].Cor == v.Cor && listaVeiculos[loop].Descricao == v.Descricao
                                            && listaVeiculos[loop].Placa == v.Placa && listaVeiculos[loop].Quilometragem == v.Quilometragem)
                                        {
                                            listaVeiculos.RemoveRange(loop, 1);
                                            loop = loop - 1;
                                            break;
                                        }
                                    }
                                    loop++;
                                }
                                
                                if(listaVeiculos.Count > 0)
                                {
                                    string resultado;
                                    bool erro = false;
                                    while (listaVeiculos.Count() > 0)
                                    {
                                        var listaDividida = listaVeiculos.Take(200);
                                        var veiculoEnviar = JsonConvert.SerializeObject(listaDividida);
                                        var jsonEnviarVeiculos = ("{'cnpj' : '" + cnpj.Trim() + "', veiculocliente: '" + veiculoEnviar + "'}");

                                        resultado = EnviarDados(jsonEnviarVeiculos, url);

                                        if (resultado != "true") { erro = true; }

                                        if (resultado != "true")
                                        {
                                            lverro.AddRange(listaDividida);
                                        }
                                        else
                                        {
                                            lvsucesso.AddRange(listaDividida);
                                        }

                                        loop++;
                                        listaVeiculos.RemoveRange(0, listaDividida.Count());
                                    }
                                    /* lverro = Lista Veiculos ERRO
                                     * lvsucesso = Lista Veiculos SUCESSO */
                                                                         
                                    if (lvsucesso.Count > 0)
                                    {
                                        using (StreamWriter writer = new StreamWriter("AppWkmVeiculos.json"))
                                        {
                                            writer.WriteLine(JsonConvert.SerializeObject(lvsucesso));
                                        }
                                    }

                                    if (lverro.Count > 0)
                                    {
                                        using (StreamWriter writer = new StreamWriter("AppWkmVeiculosErro.json"))
                                        {
                                            writer.WriteLine("Dados com erro: ");
                                            writer.WriteLine(JsonConvert.SerializeObject(lverro));
                                        }
                                    }

                                    if (erro == false)
                                    {
                                        using (StreamWriter writer = new StreamWriter("AppWkmVeiculos.json"))
                                        {
                                            writer.WriteLine(JsonConvert.SerializeObject(listaVeiculos2));
                                        }

                                    }
                                } else
                                {
                                    /* NÃO FAZ NADA! */
                                }
                            }
                        } else
                        {
                            /********************* MÉTODO DE ENVIO DE DADOS *********************/
                            int loop = 0;
                            bool erro = false;
                            string resultado = "";

                            while (listaVeiculos.Count() > 0)
                            {
                                var listaDividida = listaVeiculos.Take(200);
                                var veiculoEnviar = JsonConvert.SerializeObject(listaDividida);
                                var jsonEnviarVeiculos = ("{'cnpj' : '" + cnpj.Trim() + "', veiculocliente: '" + veiculoEnviar + "'}");

                                resultado = EnviarDados(jsonEnviarVeiculos, url);

                                if (resultado != "true") { erro = true; }

                                if (resultado != "true")
                                {
                                    lverro.AddRange(listaDividida);                                    
                                } else
                                {
                                    lvsucesso.AddRange(listaDividida);
                                }

                                loop++;
                                listaVeiculos.RemoveRange(0, listaDividida.Count());
                            }
                            /* lverro = Lista Veiculos ERRO
                             * lvsucesso = Lista Veiculos SUCESSO */

                            if (lvsucesso.Count > 0)
                            {
                                using (StreamWriter writer = new StreamWriter("AppWkmVeiculos.json"))
                                {
                                    writer.WriteLine(JsonConvert.SerializeObject(lvsucesso));
                                }
                            }

                            if (lverro.Count > 0)
                            {
                                using (StreamWriter writer = new StreamWriter("AppWkmVeiculosErro.json"))
                                {
                                    writer.WriteLine("Dados com erro: ");
                                    writer.WriteLine(JsonConvert.SerializeObject(lverro));
                                }
                            } 
                            
                            /********************* MÉTODO DE ENVIO DE DADOS *********************/
                        }
                    }
                }
                catch (Exception Erro)
                {
                    using (StreamWriter writer = new StreamWriter("AppWkmVeiculosErroCatch.json"))
                    {
                        writer.WriteLine("Erro de envio: " + DateTime.Today);
                        writer.WriteLine(Erro);
                    }
                }
            }
        }

        /* FEITO */
        public static bool ReceberVeiculos()
        {
            string cnpj = "";
            cnpj = Controle.pega_cnpj();
            //var url = "http://201.77.177.34/wkm/RetornarClienteVeiculos";
            var url = "http://192.168.0.115:62838/wkm/RetornarClienteVeiculos";

            if (cnpj != "")
            {
                try
                {
                    List<Veiculos> listaVeiculos = new List<Veiculos>();
                    List<Veiculos> listaVeiculosRetornar = new List<Veiculos>();
                    List<Veiculos> listaVeiculosRetornarErro = new List<Veiculos>();
                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Method = "POST";

                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        var json = ("{'cnpj' :'" + cnpj.Trim() + "'}");
                        streamWriter.Write(json);
                        streamWriter.Flush();
                        streamWriter.Close();
                    }

                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        var result = streamReader.ReadToEnd();

                        listaVeiculos = JsonConvert.DeserializeObject<List<Veiculos>>(result);

                        if (listaVeiculos.Count > 0)
                        {
                            DataTable veiculos = new DataTable();

                            foreach (Veiculos vc in listaVeiculos)
                            {
                                vc.Placa = vc.Placa.Substring(0, 3) + "-" + vc.Placa.Substring(3);
                                string insertVeiculos = "insert into Empveicu (empcodigo, empvplaca, empvobjcod, empvano, empvmode, empvchassi, empvcor, empvcomb, empvkm, empvobjeto, empvobserv, empcodrena, empvkmor, empvdtor, emptpcombu) " +
                                    "VALUES ("+ vc.ClienteId +", '"+ vc.Placa + "',  '" + vc.CombustivelTipo + "', '" + vc.Ano + "', '" + vc.Ano + "', '', '" + vc.Cor + "', '" + vc.CombustivelId + "', " + vc.Quilometragem + ", '" + vc.Descricao + "', 'VEICULO BAIXADO PELO APP', '', 0, ctod('///'), 0)";

                                veiculos = Controle.getda_data_vfp(insertVeiculos);

                                if (!veiculos.HasErrors)
                                {
                                    listaVeiculosRetornar.Add(vc);
                                } else
                                {
                                    listaVeiculosRetornarErro.Add(vc);
                                }
                            }
                        }

                        if (listaVeiculosRetornar.Count > 0)
                        {
                            //url = "http://201.77.177.34/wkm/InsercaoVeiculoClienteVolta";
                            url = "http://192.168.0.115:62838/wkm/InsercaoVeiculoClienteVolta";
                            var jsonEnviar = ("{'cnpj' : '" + cnpj.Trim() + "', veiculoCliente: '" + JsonConvert.SerializeObject(listaVeiculosRetornar) + "'}");

                            string resultado = EnviarDados(jsonEnviar, url);

                            if (resultado.Trim() == "True")
                            {
                                using (StreamWriter writer = new StreamWriter("appWkmVeiculosClientesBaixa.json"))
                                {
                                    writer.WriteLine(JsonConvert.SerializeObject(listaVeiculosRetornar));
                                }
                            }
                            else
                            {
                                using (StreamWriter writer = new StreamWriter("appWkmVeiculosClientesErroBaixa.json"))
                                {
                                    writer.WriteLine("Erro Envio de Veiculos Clientes: " + DateTime.Today + resultado);
                                    writer.WriteLine(JsonConvert.SerializeObject(listaVeiculosRetornar));
                                }
                                return false;
                            }
                        }

                        if (listaVeiculosRetornarErro.Count > 0)
                        {
                            //url = "http://201.77.177.34/wkm/InsercaoVeiculoClienteVolta";
                            url = "http://192.168.0.115:62838/wkm/InsercaoVeiculoClienteVolta";
                            var jsonEnviar = ("{'cnpj' : '" + cnpj.Trim() + "', veiculoCliente: '" + JsonConvert.SerializeObject(listaVeiculosRetornarErro) + "'}");

                            string resultado = EnviarDados(jsonEnviar, url);

                            if (resultado.Trim() == "True")
                            {
                                using (StreamWriter writer = new StreamWriter("appWkmVeiculosClientesBaixaErro.json"))
                                {
                                    writer.WriteLine(JsonConvert.SerializeObject(listaVeiculosRetornarErro));
                                }
                            }
                            else
                            {
                                using (StreamWriter writer = new StreamWriter("appWkmVeiculosClientesBaixaErroRequisicao.json"))
                                {
                                    writer.WriteLine("Erro Envio de Veiculos Clientes requisição: " + DateTime.Today + resultado);
                                    writer.WriteLine(JsonConvert.SerializeObject(listaVeiculosRetornarErro));
                                }
                                return false;
                            }
                        }
                    }
                    return true;
                }
                catch (Exception error)
                {
                    using (StreamWriter writer = new StreamWriter("appWkmVeiculosClientesCatchErro.json"))
                    {
                        writer.WriteLine("Erro: " + error.ToString());
                    }
                    return false;
                }
            }
            return true;
        }

        /* FEITO */
        public static bool EnviarFamilias()
        {
            string cnpj = "";
            cnpj = Controle.pega_cnpj();
            string data = DateTime.Today.ToString("MMddyy");
            //var url = "http://201.77.177.34/Wkm/InsercaoFamiliaLista2";
            var url = "http://192.168.0.115:62838/Wkm/InsercaoFamiliaLista2";

            if (cnpj != "")
            {
                try
                {
                    List<Familias> listaFamilias = new List<Familias>();
                    DataTable retorno = new DataTable();
                    string selectFamilia = "SELECT famcodigo as 'codigo', famnome as 'descricao' FROM famcad";
                    DataTable familia = new DataTable();
                    familia = Controle.getda_data_vfp(selectFamilia);

                    int i;
                    string arquivo = "AppWkmFamilias.json";

                    /* POPULANDO JSON PARA ENVIAR PARA WEB */
                    for (i = 0; i < familia.Rows.Count; i++)
                    {
                        listaFamilias.Add(new Familias
                        {
                            Id = 0,
                            OficinaId = 0,
                            Codigo = familia.Rows[i][0].ToString().Trim(),
                            Descricao = familia.Rows[i][1].ToString().Trim(),
                            DataModificacao = DateTime.Today
                        });
                    }
                    /* FIM POPULAÇÃO DO JSON */

                    if (System.IO.File.Exists(arquivo))
                    {
                        var familiasarquivojson = File.ReadAllText(arquivo);
                        List<Familias> listafamiliasarquivo = new List<Familias>();
                        listafamiliasarquivo = JsonConvert.DeserializeObject<List<Familias>>(familiasarquivojson);
                        List<Familias> listaAenviar = new List<Familias>();

                        foreach (Familias lf in listaFamilias)
                        {
                            var lfarquivo = listafamiliasarquivo.FirstOrDefault(x => x.Codigo == lf.Codigo);

                            if (lfarquivo == null) { listaAenviar.Add(lf); continue; }

                            if (lf.Descricao != lfarquivo.Descricao) { listaAenviar.Add(lf); continue; }
                        }

                        if (listaAenviar.Count == 0)
                        {
                            /********************* NÃO FAZ NADA *********************/
                            return true;
                            /********************* NÃO FAZ NADA *********************/
                        }
                        else
                        {
                            /********************* MÉTODO DE ENVIO DE DADOS *********************/

                            var familiasEnviar = listaAenviar;
                            var jsonEnviar = ("{'cnpj' : '" + cnpj.Trim() + "', familia: '" + JsonConvert.SerializeObject(familiasEnviar) + "'}");
                            string result = "";

                            result = EnviarDados(jsonEnviar, url);

                            if(result.ToUpper() == "TRUE")
                            {
                                using (StreamWriter writer = new StreamWriter("AppWkmFamilias.json"))
                                {
                                    writer.WriteLine(JsonConvert.SerializeObject(listaFamilias));
                                }
                                return true;
                            } else
                            {
                                using (StreamWriter writer = new StreamWriter("AppWkmFamiliasErro.json"))
                                {
                                    writer.WriteLine(JsonConvert.SerializeObject(listaFamilias));
                                }
                                return false;
                            }
                            
                            /******************* FIM MÉTODO DE ENVIO DE DADOS *******************/
                        }
                    }
                    else
                    {
                        /********************* MÉTODO DE ENVIO DE DADOS *********************/
                        var familiasEnviar = listaFamilias;
                        var jsonEnviar = ("{'cnpj' : '" + cnpj.Trim() + "', familia: '" + JsonConvert.SerializeObject(familiasEnviar) + "'}");
                        string result = "";

                        result = EnviarDados(jsonEnviar, url);
                        
                        if(result.ToUpper() == "TRUE")
                        {
                            using (StreamWriter writer = new StreamWriter("AppWkmFamilias.json"))
                            {
                                writer.WriteLine(JsonConvert.SerializeObject(familiasEnviar));
                            }
                            return true;
                        } else
                        {
                            using (StreamWriter writer = new StreamWriter("AppWkmFamiliasErro.json"))
                            {
                                writer.WriteLine(JsonConvert.SerializeObject(familiasEnviar));
                            }
                            return false;
                        }
                        
                        /******************* FIM MÉTODO DE ENVIO DE DADOS *******************/
                    }
                }
                catch (Exception erro)
                {
                    using (StreamWriter writer = new StreamWriter("AppWkmFamiliaErro.json"))
                    {
                        writer.WriteLine("Erro de envio: " + erro + "Data: " + DateTime.Today);
                    }
                    return false;
                }
            }
            return true;
        }
        
        /* FEITO */
        public static bool EnviarMarcas()
        {
            string cnpj = "";
            cnpj = Controle.pega_cnpj();

            if (cnpj != "")
            {
                try
                {
                    var url = "http://192.168.0.115:62838/wkm/InsercaoMarcaLista2";
                    //var url = "http://201.77.177.34/Wkm/InsercaoMarcaLista2";
                    string arquivo = "AppWkmMarcas.json";
                    List<Marcas> listamarcas = new List<Marcas>();
                    List<Marcas> lmerro = new List<Marcas>();
                    List<Marcas> lmsucesso = new List<Marcas>();
                    string selectMarcas = "SELECT marcodigo, marca, martipo FROM marcas";
                    DataTable marcas = new DataTable();
                    marcas = Controle.getda_data_vfp(selectMarcas);
                    int i;

                    if (marcas.Rows.Count > 0)
                    {
                        /* POPULANDO JSON  */
                        for (i = 0; i < marcas.Rows.Count; i++)
                        {
                            listamarcas.Add(new Marcas
                            {
                                Id = 0,
                                OficinaId = 0,
                                Codigo = marcas.Rows[i][0].ToString().Trim().Replace("\'", " ").Replace("&", "E").Replace("\\", "/"),
                                Tipo = marcas.Rows[i][2].ToString().Trim().Replace(" ", "P"),
                                Descricao = marcas.Rows[i][1].ToString().Trim().Replace("\'", " ").Replace("&", "E").Replace("\\", "/"),
                                DataModificacao = DateTime.Today
                            });
                                                        
                        }
                        /* FIM POPULAÇÃO DO JSON */

                        if (System.IO.File.Exists(arquivo))
                        {
                            var marcasarquivojson = File.ReadAllText(arquivo);
                            List<Marcas> listamarcasarquivo = new List<Marcas>();
                            List<Marcas> listaAenviar = new List<Marcas>();
                            listamarcasarquivo = JsonConvert.DeserializeObject<List<Marcas>>(marcasarquivojson);

                            foreach (Marcas lm in listamarcas)
                            {
                                var lmarquivo = listamarcasarquivo.FirstOrDefault(x => x.Codigo.Trim() == lm.Codigo.Trim());

                                if (lmarquivo == null) { listaAenviar.Add(lm); continue; }                              
                                if (lm.Descricao.Trim() != lmarquivo.Descricao.Trim()) { listaAenviar.Add(lm); continue; }
                            }
                                                     
                            if (listaAenviar.Count == 0)
                            {
                                /********************* NÃO FAZ NADA *********************/
                                
                                return true;
                                /********************* NÃO FAZ NADA *********************/
                            } else
                            {
                                /********************* MÉTODO DE ENVIO DE DADOS *********************/
                                string result;

                                while (listaAenviar.Count > 0)
                                {
                                    var listaDividida = listaAenviar.Take(100);
                                    var jsonEnviar = ("{'cnpj' : '" + cnpj.Trim() + "', marca: '" + JsonConvert.SerializeObject(listaDividida) + "'}");
                                    result = EnviarDados(jsonEnviar, url);

                                    if (result.ToUpper() != "TRUE") { lmerro.AddRange(listaDividida); }
                                    else { lmsucesso.AddRange(listaDividida); }

                                    listaAenviar.RemoveRange(0, listaDividida.Count());
                                }

                                if (lmsucesso.Count > 0 && lmerro.Count == 0)
                                {
                                    using (StreamWriter writer = new StreamWriter("AppWkmMarcas.json"))
                                    {
                                        writer.WriteLine(JsonConvert.SerializeObject(listamarcas));
                                        return true;
                                    }
                                } 

                                if (lmerro.Count > 0)
                                {
                                    using (StreamWriter writer = new StreamWriter("AppWkmMarcasErroEnvio.json"))
                                    {
                                        writer.WriteLine(JsonConvert.SerializeObject(lmerro));
                                    }
                                }

                                if (lmsucesso.Count > 0)
                                {
                                    using (StreamWriter writer = new StreamWriter("AppWkmMarcas.json"))
                                    {
                                        writer.WriteLine(JsonConvert.SerializeObject(lmsucesso));
                                    }
                                }
                                /******************* FIM MÉTODO DE ENVIO DE DADOS *******************/
                            } return true;
                        }
                        else
                        {
                            /********************* MÉTODO DE ENVIO DE DADOS *********************/
                            string result = "";

                            while (listamarcas.Count > 0)
                            {
                                var listaDividida = listamarcas.Take(100);
                                var jsonEnviar = ("{'cnpj' : '" + cnpj.Trim() + "', marca: '" + JsonConvert.SerializeObject(listaDividida) + "'}");
                                result = EnviarDados(jsonEnviar, url);

                                if (result.ToUpper() != "TRUE") { lmerro.AddRange(listaDividida); }
                                else { lmsucesso.AddRange(listaDividida); }

                                listamarcas.RemoveRange(0, listaDividida.Count());
                            }

                            if (lmsucesso.Count > 0 && lmerro.Count == 0)
                            {
                                using (StreamWriter writer = new StreamWriter("AppWkmMarcas.json"))
                                {
                                    writer.WriteLine(JsonConvert.SerializeObject(lmsucesso));
                                    return true;
                                }
                            }

                            if (lmerro.Count > 0)
                            {
                                using (StreamWriter writer = new StreamWriter("AppWkmMarcasErroEnvio.json"))
                                {
                                    writer.WriteLine(JsonConvert.SerializeObject(lmerro));
                                }
                            }

                            if (lmsucesso.Count > 0)
                            {
                                using (StreamWriter writer = new StreamWriter("AppWkmMarcas.json"))
                                {
                                    writer.WriteLine(JsonConvert.SerializeObject(lmsucesso));
                                }
                            }
                            /******************* FIM MÉTODO DE ENVIO DE DADOS *******************/
                        }
                        return true;
                    }
                    return true;
                }
                catch (Exception error)
                {
                    using (StreamWriter writer = new StreamWriter("AppWkmMarcasErro.json"))
                    {
                        writer.WriteLine("Erro de envio Catch: " + error.ToString());
                    }

                    return false;
                }
            } return true;
        }

        /* FEITO */
        public static bool EnviarEstoque()
        {
            string cnpj = "";
            cnpj = Controle.pega_cnpj();

            if (cnpj != "")
            {
                try
                {
                    string arquivo = "AppWkmEstoque.json";
                    //string url = "http://192.168.0.115/wkm/InsercaoProdutoLista2";
                    string url = "http://192.168.0.115:62838/wkm/InsercaoProdutoLista2";
                    //string url = "http://201.77.177.34/wkm/InsercaoProdutoLista2";
                    Produtos produtosJson = new Produtos();
                    List<Produtos> listaProdutos = new List<Produtos>();
                    List<Produtos> lperro = new List<Produtos>();
                    List<Produtos> lpsucesso = new List<Produtos>();
                    string selectProdutos = "SELECT p.procodigo, p.marcodigo, p.famcodigo, p.prodescri, p.proncomer, p.promodelo, p.protamanh, p.propeso, p.prolocalz, p.proprcvd, p.prounidade, p.prodtatulz, p.proestoq, p.proobs, f.famcodigo, m.marcodigo, p.prodtatulz, p.procodbarr FROM produtos p, Marcas m, famcad f WHERE m.marcodigo = p.marcodigo AND f.famcodigo = p.famcodigo";
                    DataTable produtos = new DataTable();
                    produtos = Controle.getda_data_vfp(selectProdutos);
                    
                    int i;
                    
                    /* POPULANDO JSON PARA ENVIAR PARA WEB */
                    for (i = 0; i < produtos.Rows.Count; i++)
                    {
                        listaProdutos.Add(new Produtos
                        {
                            Id = 0,
                            OficinaId = 0,
                            MarcaId = 0,
                            FamiliaId = 0,
                            Codigo = StringVaziaInt(produtos.Rows[i][0].ToString().Trim()).Replace("\\", "/").Replace("\"", " ").Replace("'", " "),
                            Descricao = LimpezaString(StringVaziaInt(produtos.Rows[i][3].ToString().Trim().Replace("\\", "/").Replace("\"", " ").Replace("'", " "))),
                            ReferenciaFabricante = StringVaziaInt(produtos.Rows[i][4].ToString().Trim().Replace("\\", "/").Replace("\"", " ").Replace("'", " ")),
                            Modelo = StringVaziaInt(produtos.Rows[i][5].ToString().Trim().Replace("\\", "/").Replace("\"", " ").Replace("'", " ")),
                            Tamanho = StringVaziaInt(produtos.Rows[i][6].ToString().Trim().Replace("\\", "/").Replace("\"", " ").Replace("'", " ")),
                            Peso = Convert.ToDecimal(produtos.Rows[i][7]),
                            Local = StringVaziaInt(produtos.Rows[i][8].ToString().Trim().Replace("\\", "/").Replace("\"", " ").Replace("'", " ")),
                            ValorVenda = Convert.ToDecimal(produtos.Rows[i][9].ToString().Trim().Replace("\\", "/").Replace("\"", " ").Replace("'", " ")),
                            Unidade = StringVaziaInt(produtos.Rows[i][10].ToString().Trim().Replace("\\", "/").Replace("\"", " ").Replace("'", " ")),
                            DataModificacao = Convert.ToDateTime(StringVaziaInt(produtos.Rows[i][16].ToString().Trim().Replace("\\", "/").Replace("\"", " ").Replace("'", " "))),
                            Quantidade = Convert.ToDecimal(produtos.Rows[i][12].ToString().Trim().Replace("\\", "/").Replace("\"", " ").Replace("'", " ")),
                            Aplicacao = LimpezaString(StringVaziaInt(produtos.Rows[i][13].ToString().Trim().Replace("\\", "/").Replace("\"", " ").Replace("'", " "))),
                            CodigoReferencia = StringVaziaInt(produtos.Rows[i][4].ToString().Trim().Replace("\\", "/").Replace("\"", " ").Replace("'", " ")),
                            FamiliaDescricao = StringVaziaInt(produtos.Rows[i][14].ToString().Trim().Replace("\\", "/").Replace("\"", " ").Replace("'", " ")),
                            MarcaDescricao = StringVaziaInt(produtos.Rows[i][15].ToString().Trim().Replace("\\", "/").Replace("\"", " ").Replace("'", " ")),
                            CodigoBarras = LimpezaString(StringVaziaInt(produtos.Rows[i][17].ToString().Trim().Replace("\\", "/").Replace("\"", " ").Replace("'", " ")))
                        });
                        
                    }
                    /* FIM POPULAÇÃO DO JSON */

                    if (System.IO.File.Exists(arquivo))
                    {
                        var produtosarquivojson = File.ReadAllText(arquivo);
                        List<Produtos> listaprodutosarquivo = new List<Produtos>();
                        List<Produtos> listaAenviar = new List<Produtos>();
                        listaprodutosarquivo = JsonConvert.DeserializeObject<List<Produtos>>(produtosarquivojson);

                        foreach (Produtos lp in listaProdutos)
                        {
                            var lparquivo = listaprodutosarquivo.FirstOrDefault(x => x.Codigo == lp.Codigo);

                            if(lparquivo == null)
                            {
                                listaAenviar.Add(lp);
                                continue;
                            }

                            if (lp.Aplicacao.Trim() != lparquivo.Aplicacao.Trim()) { listaAenviar.Add(lp); continue; }
                            if (lp.CodigoReferencia.Trim() != lparquivo.CodigoReferencia.Trim()) { listaAenviar.Add(lp); continue; }
                            if (lp.DataModificacao != lparquivo.DataModificacao) { listaAenviar.Add(lp); continue; }
                            if (lp.Descricao.Trim() != lparquivo.Descricao.Trim()) { listaAenviar.Add(lp); continue; }
                            if (lp.FamiliaDescricao.Trim() != lparquivo.FamiliaDescricao.Trim()) { listaAenviar.Add(lp); continue; }
                            if (lp.Local.Trim() != lparquivo.Local.Trim()) { listaAenviar.Add(lp); continue; }
                            if (lp.MarcaDescricao.Trim() != lparquivo.MarcaDescricao.Trim()) { listaAenviar.Add(lp); continue; }
                            if (lp.Modelo.Trim() != lparquivo.Modelo.Trim()) { listaAenviar.Add(lp); continue; }
                            if (lp.Peso != lparquivo.Peso) { listaAenviar.Add(lp); continue; }
                            if (lp.Quantidade != lparquivo.Quantidade) { listaAenviar.Add(lp); continue; }
                            if (lp.ReferenciaFabricante.Trim() != lparquivo.ReferenciaFabricante.Trim()) { listaAenviar.Add(lp); continue; }
                            if (lp.Tamanho.Trim() != lparquivo.Tamanho.Trim()) { listaAenviar.Add(lp); continue; }
                            if (lp.Unidade.Trim() != lparquivo.Unidade.Trim()) { listaAenviar.Add(lp); continue; }
                            if (lp.ValorVenda != lparquivo.ValorVenda) { listaAenviar.Add(lp); continue; }
                            if (lp.CodigoBarras != lparquivo.CodigoBarras) { listaAenviar.Add(lp); continue; }

                        }

                        if (listaAenviar.Count == 0)
                        {
                            /********************* NÃO FAZ NADA *********************/
                            
                            return true;
                            /********************* NÃO FAZ NADA *********************/
                        } else
                        {
                            /********************* MÉTODO DE ENVIO DE DADOS *********************/
                           
                            while (listaAenviar.Count() > 0)
                            {
                                var listaDividida = listaAenviar.Take(200);
                                var estoqueEnviar = JsonConvert.SerializeObject(listaDividida);
                                var jsonEnviar = ("{'cnpj' : '" + cnpj.Trim() + "', produto: '" + estoqueEnviar + "'}");

                                var result = EnviarDados(jsonEnviar, url);
                                                                
                                if (result.ToUpper() != "TRUE") { lperro.AddRange(listaDividida); }
                                else { lpsucesso.AddRange(listaDividida); }

                                listaAenviar.RemoveRange(0, listaDividida.Count());
                            }
                            /******************* FIM MÉTODO DE ENVIO DE DADOS *******************/

                            if (lperro.Count == 0 && lpsucesso.Count > 0)
                            {
                                using (StreamWriter writer = new StreamWriter("AppWkmEstoque.json"))
                                {
                                    writer.WriteLine(JsonConvert.SerializeObject(listaProdutos));
                                }
                                return true;
                            }

                            if (lperro.Count > 0)
                            {
                                using (StreamWriter writer = new StreamWriter("AppWkmEstoqueErro.json"))
                                {
                                    writer.WriteLine(JsonConvert.SerializeObject(lperro));
                                }
                            }

                            if (lpsucesso.Count > 0)
                            {
                                using (StreamWriter writer = new StreamWriter("AppWkmEstoque.json"))
                                {
                                    writer.WriteLine(JsonConvert.SerializeObject(lpsucesso));
                                }
                            }
                            return true;
                            /******************* FIM MÉTODO DE ENVIO DE DADOS *******************/
                        } 
                    }
                    else
                    {
                        while (listaProdutos.Count() > 0)
                        {
                            var listaDividida = listaProdutos.Take(100);
                            var estoqueEnviar = JsonConvert.SerializeObject(listaDividida);
                            var jsonEnviar = ("{'cnpj' : '" + cnpj.Trim() + "', produto: '" + estoqueEnviar + "'}");

                            var result = EnviarDados(jsonEnviar, url);

                            if (result.ToUpper() != "TRUE") { lperro.AddRange(listaDividida); }
                            else { lpsucesso.AddRange(listaDividida); }
                                                       
                            listaProdutos.RemoveRange(0, listaDividida.Count());
                            Thread.Sleep((5 / 60) * 60 * 1000);
                        }
                        /******************* FIM MÉTODO DE ENVIO DE DADOS *******************/

                        if (lperro.Count == 0 && lpsucesso.Count > 0)
                        {
                            using (StreamWriter writer = new StreamWriter("AppWkmEstoque.json"))
                            {
                                writer.WriteLine(JsonConvert.SerializeObject(listaProdutos));
                            }
                            return true;
                        }

                        if (lperro.Count > 0)
                        {
                            using (StreamWriter writer = new StreamWriter("AppWkmEstoqueErro.json"))
                            {
                                writer.WriteLine(JsonConvert.SerializeObject(lperro));
                            }
                        }

                        if (lpsucesso.Count > 0)
                        {
                            using (StreamWriter writer = new StreamWriter("AppWkmEstoque.json"))
                            {
                                writer.WriteLine(JsonConvert.SerializeObject(lpsucesso));
                            }
                        }
                        return true;
                    }
                }
                catch (Exception error)
                {
                    using (StreamWriter writer = new StreamWriter("AppWkmEstoqueErroCatch.json"))
                    {
                        writer.WriteLine("Erro de envio: " + error.ToString());
                        return false;
                    }
                }
            } return true;
        }
        
        /* FEITO */
        public static void EnviarFuncionarios()
        {
            string cnpj = "";
            cnpj = Controle.pega_cnpj();

            if (cnpj != "")
            {
                //var url = "http://201.77.177.34/Wkm/InsercaoFuncionarioLista2";
                var url = "http://192.168.0.115:62838/Wkm/InsercaoFuncionarioLista2";
                try
                {
                    string arquivo = "AppWkmFunc.json";
                    Funcionarios FuncionariosJson = new Funcionarios();
                    List<Funcionarios> listaFuncionarios = new List<Funcionarios>();
                    List<Funcionarios> lferro = new List<Funcionarios>();
                    List<Funcionarios> lfsucesso = new List<Funcionarios>();
                    string selectFuncions = "SELECT funome, funomecpl, funsenha, funcao, fundatdemi FROM funcions";
                    DataTable funcionarios = new DataTable();
                    funcionarios = Controle.getda_data_vfp(selectFuncions);
                    int i;

                    /* POPULANDO JSON PARA ENVIAR PARA WEB */
                    for (i = 0; i < funcionarios.Rows.Count; i++)
                    {
                        listaFuncionarios.Add(new Funcionarios
                        {
                            Id = 0,
                            OficinaId = 0,
                            Nome = funcionarios.Rows[i][1].ToString().TrimEnd(),
                            DataModificacao = DateTime.Today,
                            Cargo = funcionarios.Rows[i][3].ToString().TrimEnd(),
                            Senha = funcionarios.Rows[i][2].ToString().TrimEnd(),
                            Codigo = funcionarios.Rows[i][0].ToString().TrimEnd(),
                            Ativo = true,
                            Logado = false,
                            DataDemissão = Convert.ToDateTime(funcionarios.Rows[i][4].ToString()),
                        });
                    }
                    /* FIM POPULAÇÃO DO JSON */

                    if (System.IO.File.Exists(arquivo))
                    {
                        var funcionariosarquivojson = File.ReadAllText(arquivo);
                        List<Funcionarios> listafuncionariosarquivo = new List<Funcionarios>();
                        listafuncionariosarquivo = JsonConvert.DeserializeObject<List<Funcionarios>>(funcionariosarquivojson);
                        List<Funcionarios> listaAenviar = new List<Funcionarios>();

                        foreach (Funcionarios lf in listaFuncionarios)
                        {
                            var lmarquivo = listafuncionariosarquivo.FirstOrDefault(x => x.Codigo == lf.Codigo);

                            if (lmarquivo == null)
                            {
                                listaAenviar.Add(lf);
                                continue;
                            }

                            if (lf.Cargo != lmarquivo.Cargo) { listaAenviar.Add(lf); continue; }
                            if (lf.Nome != lmarquivo.Nome) { listaAenviar.Add(lf); continue; }
                            if (lf.Senha != lmarquivo.Codigo) { listaAenviar.Add(lf); continue; }
                            if (lf.DataDemissão != lmarquivo.DataDemissão) { listaAenviar.Add(lf); continue; }
                        }

                        if (listaAenviar.Count == 0)
                        {
                            /********************* NÃO FAZ NADA *********************/
                            
                            /********************* NÃO FAZ NADA *********************/
                        }
                        else
                        {
                            /********************* MÉTODO DE ENVIO DE DADOS *********************/
                            var FuncEnviar = listaAenviar;
                            var jsonEnviar = ("{'cnpj' : '" + cnpj.Trim() + "', funcionario: '" + JsonConvert.SerializeObject(FuncEnviar) + "'}");

                            string result = EnviarDados(jsonEnviar, url);

                            if (result.ToUpper() != "TRUE") { lferro.AddRange(listaFuncionarios); }
                            else { lfsucesso.AddRange(listaFuncionarios); }

                            if(lfsucesso.Count > 0)
                            {
                                using (StreamWriter writer = new StreamWriter("AppWkmFunc.json"))
                                {
                                    writer.WriteLine(JsonConvert.SerializeObject(lfsucesso));
                                }
                            } 

                            if (lferro.Count > 0)
                            {
                                using (StreamWriter writer = new StreamWriter("AppWkmFuncErro.json"))
                                {
                                    writer.WriteLine(JsonConvert.SerializeObject(lferro));
                                }
                            }

                            /******************* FIM MÉTODO DE ENVIO DE DADOS *******************/
                        }
                    }
                    else
                    {
                        /********************* MÉTODO DE ENVIO DE DADOS *********************/
                        var FuncEnviar = JsonConvert.SerializeObject(listaFuncionarios);
                        var jsonEnviar = ("{'cnpj' : '" + cnpj.Trim() + "', funcionario: '" + FuncEnviar + "'}");

                        string result = EnviarDados(jsonEnviar, url);

                        if (result.ToUpper() != "TRUE") { lferro.AddRange(listaFuncionarios); }
                        else { lfsucesso.AddRange(listaFuncionarios); }

                        if (lfsucesso.Count > 0)
                        {
                            using (StreamWriter writer = new StreamWriter("AppWkmFunc.json"))
                            {
                                writer.WriteLine(JsonConvert.SerializeObject(lfsucesso));
                            }
                        }

                        if (lferro.Count > 0)
                        {
                            using (StreamWriter writer = new StreamWriter("AppWkmFuncErro.json"))
                            {
                                writer.WriteLine("Erro envio Funcionarios: " + result + JsonConvert.SerializeObject(lferro));
                            }
                        }
                        /********************* MÉTODO DE ENVIO DE DADOS *********************/
                    }
                }
                catch (Exception error)
                {
                    using (StreamWriter writer = new StreamWriter("AppWkmFuncErro.json"))
                    {
                        writer.WriteLine("Erro no Envio: " + error.ToString());
                    }
                }
            
            }
        }
                
        /* FEITO */
        public static void RecebeOrdensServico()
        {
            string cnpj = "";
            cnpj = Controle.pega_cnpj();
            //var url = "http://201.77.177.34:62838/wkm/RetornaOrdensServico";
            var url = "http://192.168.0.115:62838/wkm/RetornaOrdensServico";
            List<VendasApp> listaVendasRetornar = new List<VendasApp>();
            List<VendasApp> listaVendasRetornarErro = new List<VendasApp>();

            if (cnpj != "")
            {
                try
                {
                    int orcodigo = -1;
                    List<VendasApp> listaVendas = new List<VendasApp>();
                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Method = "POST";
                    string envio = "";

                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        var json = ("{'cnpj' :'" + cnpj.Trim() + "'}");
                        streamWriter.Write(json);
                        streamWriter.Flush();
                        streamWriter.Close();
                        envio = json.ToString();
                    }
                    
                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        var result = streamReader.ReadToEnd();

                        listaVendas = JsonConvert.DeserializeObject<List<VendasApp>>(result);

                        if (listaVendas.Count > 0)
                        {
                            DataTable retorno = new DataTable();
                            DataTable vendas = new DataTable();
                            DataTable numerador = new DataTable();
                            DataTable numeradorErro = new DataTable();
                            string cliente;
                            bool sucessoVendas = true;
                            bool sucessonumerador = true;
                            int orcodigoanterior = 0;
                            int ultimaVendaAPP = 0;
                            string updateNumerador = "";
                            string selectCliente = "";
                            int qtdVendas = listaVendas.Count;

                            foreach (VendasApp vl in listaVendas)
                            {
                                orcodigo = Controle.pega_venda() + 1;
                                orcodigoanterior = orcodigo - 1;
                                ultimaVendaAPP = vl.Id;

                                if (orcodigo >= 0)
                                {
                                    string Objeto = "NÃO ENCONTRADO";

                                    if (vl.CarroFipeId != "0" && vl.CarroFipeId != "")
                                    {
                                        string selectObjeto = "SELECT objeto FROM objetmat WHERE Objcodigo  = '" + vl.CarroFipeId + "'"; 
                                        Objeto = Controle.pega_objeto(selectObjeto);
                                    } else
                                    {
                                        Objeto = "NÃO ENCONTRADO";
                                    }

                                    if (vl.ClienteId <= 0) { vl.ClienteId = 1; cliente = "CONSUMIDOR APP";  }
                                    else
                                    {
                                        selectCliente = "SELECT emprzsocia FROM empresas WHERE empcodigo = " + vl.ClienteId + "";
                                        cliente = Controle.pega_cliente(selectCliente);
                                    }
                                        
                                    updateNumerador = "UPDATE numerado SET Ndrlstdoc = " + orcodigo + " WHERE UPPER(ndrcod) = 'VEN'";
                                    numerador = Controle.getda_data_vfp(updateNumerador);

                                    if (numerador.HasErrors) { sucessonumerador = false; }

                                    if (sucessonumerador == true)
                                    {
                                        vl.Placa = vl.Placa.Substring(0, 3) + "-" + vl.Placa.Substring(3);

                                        string insertVendas = "insert into ORCVENS1 (Orcodigo, Dadkm, Dadcombust, Tracodigo, Orcdata, Orcdescto, Orchrinic, Orcultnum, Orcrzsocia, Orctipo, Orcdescpro, Orcfrete, Orcoutrasd, Penultnumi, Orctotalvd, Orctotalor, Priscod, Orcobs1, Orcdefeit1, Orcavari1, Dadkmmesro, Dadtara, Dadpbt, Dadlotacao, Orccmv, Orcrateiod, Orccmvtota, Orcdescopr, Orcdescose, Orctotrete, Orctotdesc, Chcodigo, Empcodigo, Dadpesoliq, Dadpesobr, Dadqtdvol, Orccomiss, Orcvenda, Orccomiser, Funome, Orcprodipi, Dadtpcombu, Motcod, Orcrepcodi, Orctotpret, Orcnumdav, Orcprevend, Orcseguro, Orcoperaca, Orcstatnfc, Orcnatoper, Dadcombqtd, Orctpemiss, Orcdatapon, Orcultapon, Orctecnico, Orcdtchega, Orcdtsaida, Dadprevret, Orcdtvige, Daddatfech, Daddatentr, Dadvainspe, Orcleitdt, Interdata, Orcleitdte, Orccupodat, Ospdtini, Ospdtfim, Orchegada, Orcsaida, Orcmedd1, Orcmedd2, Dadchapa, Dadchassis, Dadcorveic, Orctpfret, Orcdadexp, Orcomprd, Orcpedcli, Orcpedint, Dadcttnume, Dadoutros, Orchrfim, Orcobs,  Daddespadi, Orcnumcupo, Orcdatcupo, Dadanoveic, Orcequiult, Dadveiculo, Orcobjcodi, Orcobjeto, Repcodigo, Tposnum, Objcodigo, Dadanofab, Dadcodrena, Dadesptipo, Dadcertifi, Dadgaranti, Dadobserva, Dadobjeto, Oscodfim, Orcmarcave, Orcmarcodi, Orcdefeito, Orcavarias, Cpgcodigo, Dadmodveic, Dadcilindr, Daddocref, Dadcontrre, Orcredetef, Orcnsutef, Orcdatatef, Orchoratef, Orcfinalte, Orculttit, Dadrespons, Orcleithr, Dadguincho, Dadhrprevi, Orcrepnome, Dadentrvei, Orcimpress, Interkm, Afecod, Orcnccf, Orcgnf, Orcgrg, Orccdc, Orcdenomr6, Pckosstatu, Orcleitvol, Orcleitcon, Orcndav, Orcdtdav, Orcvlrdav, Orccoo, Orccancoo, Daddatprev, Orcnumccar, Orcdavseq, Orcpafhash, Orcempcgcc, Orcalterac, Orccupohor, Orcpaf2has, orcpaf3has, Orcpaf4has, Orcpaf5has, Orccoorelg, Orcrattitu, Orctxfina, Orc2nsutef, Orc2datate, Orc2horate, Orc2redete, Orc2finalt, Orc2gnf, Orc2cdc, Orc2numcca, Orcbcst, Orcicmsst, Orcbseicm, Orctotalic, Orcalqiss, Orccredalq, Orccredval, Orcretcssl, Orccfgrete, Orccfgpis, Orccfgcofi, Orcretpis, Orcretcofi, Orcirrf, Orcinss, Orcbseser, Orcvalorse, Orcissreti, Orccfgirrf, Orccfginss, Orcxml, Orcchnfce, Orcatncodi, Orccstat, Orcmotivo, Orcnrec, Orcdigval, Orcdhrecbt, Orcnprotau, Orcverapli, Orcnfcecod, Orcseqeven, Orcjust, Orcnprotca, Orcdthtcon, Orcjustcon, Orcnfceemi, Ospdtini, Osphrini, Ospdtfim, Osphrfin, Orcnfceser, Orcdhrecut, Orcnotman, Orcversao, Orctpdoc, Orcsatcodr, Orcassqrco, Orcj1hashp) " +
                                    "VALUES (" + orcodigo + ", " + vl.Quilometragem + ", '" + vl.CombustivelId + "', 0, CTOD('" + vl.DataEmissao.ToString("MM/dd/yyyy") + "'),  0, '" + vl.DataEmissao.ToString("HH:mm") + "', 0, '" + cliente + "', 'V', 0, 0, 0, 0, 0, 0, 0, '" + vl.Observacao + "', '" + vl.Defeitos + "', '" + vl.Avarias + "', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, " + vl.ClienteId + ", 0, 0, 0, 0, 0, 0, '" + vl.FuncionarioId + "', 0, 0, 0, 0, 0, '" + orcodigo + "', 1, 0, '5', 0, 4, 0, 0, CTOD('\\'), 0, '', CTOD('\\'), CTOD('\\'), CTOD('\\'), CTOD('\\'), CTOD('\\'), CTOD('\\'), CTOD('\\'), CTOD('\\'), CTOD('\\'), CTOD('\\'), CTOD('\\'), CTOD('\\'), CTOD('\\'), '', '', 0, 0, '"+ vl.Placa +"', '', '"+ vl.Cor +"', 'E', '', '', '', '', 0, '', '', '" + vl.Observacao + "', 0, '', '', '"+ vl.Ano +"', 0, '"+ Objeto +"', '"+ vl.CarroFipeId + "', '" + Objeto + "', 0, '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', 0, 0, 0, '', '', '', '', '', 0, 0, '', '', '', '', '', '', CTOD('\\'), '', '', '', '', 0, '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', 0, '', 0, '', CTOD('\\'), 0, '', 0, 0, '', 0, CTOD('\\'), '', CTOD('\\'), CTOD('\\'), '', CTOD('\\'), '', '', '', 0, 0, 0, '', '', '')";

                                        vendas = Controle.getda_data_vfp(insertVendas);

                                        if (!vendas.HasErrors)
                                        {
                                            vl.WorkMotorId = orcodigo;
                                            listaVendasRetornar.Add(vl);

                                            orcodigo = Controle.pega_venda();

                                            /*
                                            if(orcodigo-1 == orcodigoanterior)
                                            {
                                                string updateNumeradorErro = "UPDATE numerado SET Ndrlstdoc = " + orcodigo + " WHERE UPPER(ndrcod) = 'VEN'";
                                                numeradorErro = Controle.getda_data_vfp(updateNumeradorErro);
                                            }
                                            */
                                        } else
                                        {
                                            vl.WorkMotorId = 0;
                                            listaVendasRetornarErro.Add(vl);

                                            string updateNumeradorErro = "UPDATE numerado SET Ndrlstdoc = " + orcodigoanterior + " WHERE UPPER(ndrcod) = 'VEN'";
                                            numeradorErro = Controle.getda_data_vfp(updateNumeradorErro);
                                        }
                                    } 
                                }

                                orcodigo = -1;
                            }

                            if(listaVendasRetornar.Count > 0)
                            {
                                url = "http://201.77.177.34/wkm/RetornaOrdensServicoVolta";
                                //url = "http://192.168.0.115:62838/wkm/RetornaOrdensServicoVolta";

                                var jsonEnviar = ("{'cnpj' : '" + cnpj.Trim() + "', ordemServico: '" + JsonConvert.SerializeObject(listaVendasRetornar) + "'}");

                                string resultado = EnviarDados(jsonEnviar, url);

                                if (resultado == "True")
                                {
                                    using (StreamWriter writer = new StreamWriter("appWkmVendasEnvioRetorno.json"))
                                    {
                                        writer.WriteLine("Vendas Baixadas e Enviadas: " + JsonConvert.SerializeObject(listaVendasRetornar));
                                    }
                                }
                                else
                                {
                                    using (StreamWriter writer = new StreamWriter("appWkmVendasEnvioErro.json"))
                                    {
                                        writer.WriteLine("Erro Envio de vendas: " + DateTime.Today);
                                        writer.WriteLine(resultado);
                                        writer.WriteLine(JsonConvert.SerializeObject(listaVendasRetornar));
                                    }
                                }
                            }

                            if (listaVendasRetornarErro.Count > 0)
                            {
                                //url = "http://201.77.177.34/wkm/RetornaOrdensServicoVolta";
                                url = "http://192.168.0.115:62838/wkm/RetornaOrdensServicoVolta";

                                var jsonEnviar = ("{'cnpj' : '" + cnpj.Trim() + "', ordemServico: '" + JsonConvert.SerializeObject(listaVendasRetornarErro) + "'}");

                                string resultado = EnviarDados(jsonEnviar, url);

                                if (resultado == "True")
                                {
                                    using (StreamWriter writer = new StreamWriter("appWkmVendasComErroApp.json"))
                                    {
                                        writer.WriteLine("Vendas Baixadas e com problemas: " + JsonConvert.SerializeObject(listaVendasRetornarErro));
                                    }
                                }
                                else
                                {
                                    using (StreamWriter writer = new StreamWriter("appWkmVendasComErroAppFalha.json"))
                                    {
                                        writer.WriteLine("Vendas Baixadas e com problemas, Falha na requisição: " + DateTime.Today);
                                        writer.WriteLine(resultado);
                                        writer.WriteLine(JsonConvert.SerializeObject(listaVendasRetornarErro));
                                    }
                                }
                            }
                        }
                    }

                } catch (Exception error)
                {
                    using (StreamWriter writer = new StreamWriter("AppWkmVendasErro.json"))
                    {
                        writer.WriteLine("Erro: " + error.ToString());
                        writer.WriteLine("Erro Retorno Erro: " + JsonConvert.SerializeObject(listaVendasRetornarErro));
                        writer.WriteLine("Erro Vendas Com erro: " + JsonConvert.SerializeObject(listaVendasRetornar));
                    }
                }
            }
        }

        /* FEITO */
        public static bool ReceberClientes()
        {
            string cnpj = "";
            cnpj = Controle.pega_cnpj();
            //var url = "http://201.77.177.34/wkm/RetornarClientes2";
            var url = "http://192.168.0.115:62838/wkm/RetornarClientes2";
            List<Clientes> listaClientes = new List<Clientes>();
            List<Clientes> clientesRetorno = new List<Clientes>();
            List<Clientes> clientesRetornoErro = new List<Clientes>();
            //var url = "http://192.168.0.115:62838/wkm/RetornarClientes2";

            if (cnpj != "")
            {
                try
                {
                    int ultCliente = -1;

                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Method = "POST";

                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        var json = ("{'cnpj' :'" + cnpj.Trim() + "'}");
                        streamWriter.Write(json);
                        streamWriter.Flush();
                        streamWriter.Close();
                    }

                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        var result = streamReader.ReadToEnd();

                        listaClientes = JsonConvert.DeserializeObject<List<Clientes>>(result);

                        if (listaClientes.Count > 0)
                        {
                            using (StreamWriter writer = new StreamWriter("appWkmClientesAppBaixados.json"))
                            {
                                writer.WriteLine("Clientes Baixados: " + JsonConvert.DeserializeObject(result));
                            }

                            DataTable retorno = new DataTable();
                            DataTable clientes = new DataTable();
                            DataTable numerador = new DataTable();
                            DataTable numeradorErro = new DataTable();
                            bool sucessoClientes = false;
                            bool sucessonumerador = false;
                            bool erroClientes = false;
                            int cliAnterior = 0;
                            string updateNumerador = "";

                            foreach (Clientes cl in listaClientes)
                            {
                                ultCliente = Controle.pega_cliente_numerador() + 1;
                                cliAnterior = ultCliente - 1;

                                if (ultCliente >= 0)
                                {
                                    if(Convert.ToString(cl.Fax) == "") { cl.Fax = "0"; }
                                    if (Convert.ToString(cl.RazaoSocial) == "") { cl.RazaoSocial = "NÃO INFORMADA"; }
                                    if (Convert.ToString(cl.NomeFantasia) == "") { cl.NomeFantasia = "NÃO INFORMADA"; }
                                    if (Convert.ToString(cl.CnpjCpf) == "") { cl.CnpjCpf = "00000000000"; }
                                    if (Convert.ToString(cl.InscricaoEstadual) == "") { cl.InscricaoEstadual = "ISENTO"; }
                                    if (Convert.ToString(cl.Endereco) == "") { cl.Endereco = "SEM ENDEREÇO INFORMADO"; }
                                    if (Convert.ToString(cl.Numero) == "") { cl.Numero = 0; }
                                    if (Convert.ToString(cl.Complemento) == "") { cl.Complemento = "SEM COMPLEMENTO"; }
                                    if (Convert.ToString(cl.Bairro) == "") { cl.Bairro = "NÃO INFORMADO"; }
                                    if (Convert.ToString(cl.Cidade) == "") { cl.Cidade = "NÃO INFORMADA"; }
                                    if (Convert.ToString(cl.Estado) == "") { cl.Estado = "NÃO INFORMADO"; }
                                    if (Convert.ToString(cl.Cep) == "") { cl.Cep = "00000000"; }
                                    cl.Cep = cl.Cep.Substring(0, 5) + "-" + cl.Cep.Substring(5);
                                    if (Convert.ToString(cl.Ddd) == "") { cl.Ddd = 0; }
                                    if (Convert.ToString(cl.Telefone) == "") { cl.Telefone = "0"; }
                                    if (Convert.ToString(cl.Email) == "") { cl.Email = "SEM E-MAIL"; }
                                    if (Convert.ToString(cl.Codigo) == "") { cl.Codigo = 0; }
                                    if (Convert.ToString(cl.CelularDdd) == "") { cl.CelularDdd = 0; }
                                    if (Convert.ToString(cl.DataNascimento) == "") { cl.DataNascimento = DateTime.Today; }
                                    if (Convert.ToString(cl.Sexo) == "") { cl.Sexo = "M"; }

                                    updateNumerador = "UPDATE numerado SET Ndrlstdoc = " + ultCliente + " WHERE UPPER(ndrcod) = 'CLI'";
                                    numerador = Controle.getda_data_vfp(updateNumerador);

                                    if (!numerador.HasErrors) { sucessonumerador = true; }

                                    if (sucessonumerador == true)
                                    {
                                        string insertClientes = "INSERT INTO EMPRESAS (empcodigo, emprzsocia, empnomfan, empcomprd, empcgccpf, empnscest, emptipo, empendere, empnumero, empcomple, empbairro, empcidade, empestado, empcep, empddd, empfone, empfax, empramais, empsuframa, empemail, empendcob, empnumcob, empcompcob, empbaircob, empcidcob, empestcob, empcepcob, empendent, empnument, empcompent, empbairent, empcident, empestent, emprefere, emplimcrd, regcodigo, atvcodigo, repcodigo, empdtcad, empatuali, empdtnasc, empbloqud, mgcodigo, empdscpad, empcancel, empnumref, emptime, emptratam, empsobren, empsexo, empcepent, empdialimi, empcel, empver, mgservico, empestcivi, emptpresid, empobserv, tpbcodigo, empcpgcodi, empcpgparc, empmensage, empdddcel, empdddfcom, empfonecom, empoutrfon, empdiames, empretimpo, empretiss, empimposto, empretirrf, empretinss, empemissor, emprendafa, empfrete, empultref, emptppesso, empcrdulti, empiss, empsegurad, empdscprod, empdscserv, emplimusad, empcidcodi, empcidcodc, empcidcode, empretcofi, empinsmun, empidrad, empultdt, emprg, empconsum, ultcttcodi, ultcttstat, ultcttdtfi, ulttpccodi, empindie, empqcemail, empfiliaca, empconjuge, emptrabcep, emptrabend, emptrabnum, emptrabcom, emptrabcid, emptrabnom, emptrabuf, emptrabbai, EMPTEXTNF, EMPTEXTPD, EMPIMAGEM, empcidcodi) " +
                                            "VALUES (" + ultCliente + ", '" + cl.RazaoSocial + "', '" + cl.NomeFantasia + "', '' , '" + cl.CnpjCpf + "', '" + cl.InscricaoEstadual + "' , 0, '" + cl.Endereco + "' , " + cl.Numero + ", '" + cl.Complemento + "', '" + cl.Bairro + "', '" + cl.Cidade + "', '" + cl.Estado + "', '" + cl.Cep + "', " + cl.Ddd + ", " + cl.Telefone + ", " + cl.Fax + ", '', '', '" + cl.Email + "', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', 0, 0, 1, 0, date(), date(), CTOD('" + cl.DataNascimento.ToString("MM/dd/yyyy") + "'), CTOD('\\\'), '', 0, 'F', 0, '', '', '', '" + cl.Sexo + "', '', 0, " + cl.Celular + ", 'N', '', 6, 1, '', '', '', '', 0, " + cl.CelularDdd + ", 0, 0, '', '0 0', 0, 0, 0, 0, 0, '', 0, 'D', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', CTOD('\\\'), '', 0, 0, 0, CTOD('\\\'), 0, 9, 0, '', '', '', '', 0, '', 0, '', '', '', '', '', '', " + cl.MunicipioIbge +")";

                                        clientes = Controle.getda_data_vfp(insertClientes);

                                        if (!clientes.HasErrors)
                                        {
                                            sucessoClientes = true;
                                                
                                            ultCliente = Controle.pega_cliente_numerador();

                                            if (ultCliente - 1 == cliAnterior)
                                            {
                                                string updateNumeradorErro = "UPDATE numerado SET Ndrlstdoc = " + ultCliente + " WHERE UPPER(CLI) = 'CLI'";
                                                numeradorErro = Controle.getda_data_vfp(updateNumeradorErro);
                                            }
                                            cl.Codigo = ultCliente;
                                            clientesRetorno.Add(cl);
                                        } else
                                        {
                                            erroClientes = true;
                                            cl.Codigo = 0;
                                            clientesRetornoErro.Add(cl);

                                            if (Controle.pega_cliente_numerador() == ultCliente)
                                            {
                                                string updateNumeradorErro = "UPDATE numerado SET Ndrlstdoc = " + (ultCliente - 1) + " WHERE UPPER(CLI) = 'CLI'";
                                                numeradorErro = Controle.getda_data_vfp(updateNumeradorErro);
                                            }  
                                        }
                                    }
                                }
                            }

                            if (sucessoClientes == true)
                            {
                                using (StreamWriter writer = new StreamWriter("appWkmClientesApp.json"))
                                {
                                    writer.WriteLine(JsonConvert.SerializeObject(clientesRetorno));
                                }
                            }

                            if(erroClientes == true)
                            {
                                using (StreamWriter writer = new StreamWriter("AppWkmClienteAppErro.json"))
                                {
                                    writer.WriteLine("Erro de baixa de clientes: " + DateTime.Today);
                                }
                            }
                        } else
                        {
                            return true;
                        }
                    }

                    if (clientesRetorno.Count() > 0)
                    {
                        //url = "http://201.77.177.34/wkm/InsercaoClienteVolta";
                        url = "http://192.168.0.115:62838/wkm/InsercaoClienteVolta";

                        var jsonEnviar = ("{'cnpj' : '" + cnpj.Trim() + "', cliente: '" + JsonConvert.SerializeObject(clientesRetorno) + "'}");

                        string result = EnviarDados(jsonEnviar, url);

                        if (result == "True")
                        {
                            using (StreamWriter writer = new StreamWriter("appWkmClientesAppRetorno.json"))
                            {
                                writer.WriteLine(JsonConvert.SerializeObject(clientesRetorno));
                            } return true;
                        }
                        else
                        {
                            using (StreamWriter writer = new StreamWriter("appWkmClientesAppRetornoErro.json"))
                            {
                                writer.WriteLine("Erro Envio de Clientes: " + DateTime.Today);
                                writer.WriteLine(JsonConvert.SerializeObject(clientesRetorno));
                            } return false;
                        }
                    }

                    if (clientesRetornoErro.Count() > 0)
                    {
                        //url = "http://201.77.177.34/wkm/InsercaoClienteVolta";
                        url = "http://192.168.0.115:62838/wkm/InsercaoClienteVolta";

                        var jsonEnviar = ("{'cnpj' : '" + cnpj.Trim() + "', cliente: '" + JsonConvert.SerializeObject(clientesRetornoErro) + "'}");

                        string result = EnviarDados(jsonEnviar, url);

                        if (result == "True")
                        {
                            using (StreamWriter writer = new StreamWriter("appWkmClientesRetornoErroInsercao.json"))
                            {
                                writer.WriteLine(JsonConvert.SerializeObject(clientesRetornoErro));
                            }
                            return true;
                        }
                        else
                        {
                            using (StreamWriter writer = new StreamWriter("appWkmClientesRetornoErroInsercaoRetorno.json"))
                            {
                                writer.WriteLine("Erro Envio de Clientes com erro de inserção: " + DateTime.Today);
                                writer.WriteLine(JsonConvert.SerializeObject(clientesRetornoErro));
                            }
                            return false;
                        }
                    }
                    else
                    {
                        return true;
                    }
                } 

                catch (Exception error)
                {
                    using (StreamWriter writer = new StreamWriter("appWkmClientesAppErro.json"))
                    {
                        writer.WriteLine("Erro ao baixar clientes do aplicativo AppWkm: " + error.ToString());
                    }
                    return false;
                }
            }
            return true;
        }

        /* FEITO */
        public static bool OficinaAtiva()
        {
            string cnpj = "";
            string result = "";
            cnpj = Controle.pega_cnpj();

            if (cnpj != "")
            {
                try
                {
                    
                    //var url = "http://201.77.177.34/Wkm/OficinaAtiva";
                    var url = "http://192.168.0.115:62838/Wkm/OficinaAtiva";
                    var jsonEnviar = ("{'cnpj' : '" + cnpj.Trim() + "'}");
                    result = EnviarDados(jsonEnviar, url);
                    
                    if(result.ToUpper() == "TRUE")
                    {
                        return true;
                    } else
                    {
                        return false;
                    }                    
                }
                catch
                {
                    return false;
                }
            }
            return false;
        }

        /* FEITO */
        public static bool EnviarOrdensServico()
        {
            string cnpj = "";
            cnpj = Controle.pega_cnpj();

            if (cnpj != "")
            {
                try
                {
                    var url = "http://192.168.0.115/wkm/AtualizaOrdensServico";
                    //var url = "http://201.77.177.34/Wkm/AtualizaOrdensServico";
                    string arquivo = "AppWkmVendasValores.json";
                    List<VendasApp> listaVendasEnviar = new List<VendasApp>();
                    List<VendasApp> listaVendasRetornarErro = new List<VendasApp>();
                    string selectVendas = "SELECT O.funome, O.OrcObjcodi, O.DadCombust, O.Empcodigo, O.DadAnoVeic, O.DadKm, O.Orcobs, O.OrcDefeito, O.OrcAvarias, O.DadChapa, O.Orcodigo, O.Orcdata, O.DadCorVeic, O.OrcVenda, t.TpOSNome, (O.Orctotalvd - O.Orcvalorse), O.Orcvalorse, (O.Orctotalvd), O.OrcDescoPr, O.OrcDescoSe FROM orcvens1 O LEFT JOIN tipoos t ON o.tposnum = t.tposnum";
                    DataTable Vendas = new DataTable();
                    Vendas = Controle.getda_data_vfp(selectVendas);
                    int i;
                    string statusFinal = "";

                    if (Vendas.Rows.Count > 0)
                    {
                        /* POPULANDO JSON  */
                        for (i = 0; i < Vendas.Rows.Count; i++)
                        {
                            
                            if (Convert.ToInt32(Vendas.Rows[i][13]) == 0)
                            {
                                statusFinal = "ORÇAMENTO: ";
                            } else if(Convert.ToInt32(Vendas.Rows[i][13]) == 1)
                            {
                                statusFinal = "OS/VENDA: : ";
                            } else if(Convert.ToInt32(Vendas.Rows[i][13]) == 2)
                            {
                                statusFinal = "OS/FECHADA: ";
                            } else if(Convert.ToInt32(Vendas.Rows[i][13]) == 3)
                            {
                                statusFinal = "CANCELADA: ";
                            } else
                            {
                                statusFinal = "ENCERRADA: ";
                            }
                            
                            listaVendasEnviar.Add(new VendasApp
                            {
                                Id = 0,
                                OficinaId = 0,
                                FuncionarioId = Vendas.Rows[i][0].ToString().Trim(),
                                CarroFipeId = Vendas.Rows[i][1].ToString().Trim(),
                                CombustivelId = Vendas.Rows[i][2].ToString().Trim(),
                                ClienteId = Convert.ToInt32(Vendas.Rows[i][3]),
                                NumeroOrcamento = 0,
                                Ano = ValidaAnoModeloFipe(Vendas.Rows[i][4].ToString()),
                                Quilometragem = Convert.ToDecimal(Vendas.Rows[i][5]),
                                Observacao = Vendas.Rows[i][6].ToString().Trim().Replace("\'", " ").Replace("&", "E").Replace("\\", "/"),
                                Defeitos = Vendas.Rows[i][7].ToString().Trim().Replace("\'", " ").Replace("&", "E").Replace("\\", "/"),
                                Avarias = Vendas.Rows[i][8].ToString().Trim().Replace("\'", " ").Replace("&", "E").Replace("\\", "/"),
                                Placa = Vendas.Rows[i][9].ToString().Trim().Replace("\'", " ").Replace("&", "E").Replace("\\", "/"),
                                WorkMotorId = Convert.ToInt32(Vendas.Rows[i][10]),
                                DataEmissao = Convert.ToDateTime(Vendas.Rows[i][11]), 
                                Cor = Vendas.Rows[i][12].ToString().Trim().Replace("\'", " ").Replace("&", "E").Replace("\\", "/"),
                                Status = statusFinal + Vendas.Rows[i][14].ToString().Trim().Replace("\'", " ").Replace("&", "E").Replace("\\", "/"),
                                TotalPecas = Convert.ToDecimal(Vendas.Rows[i][15]),
                                TotalServicos = Convert.ToDecimal(Vendas.Rows[i][16]),
                                Total = Convert.ToDecimal(Vendas.Rows[i][17]),
                                DescProdutos = Convert.ToDecimal(Vendas.Rows[i][18]),
                                DescServicos = Convert.ToDecimal(Vendas.Rows[i][19])
                            });
                            
                        }
                        /* FIM POPULAÇÃO DO JSON */

                        if (System.IO.File.Exists(arquivo))
                        {
                            var vendasappvaloresarquivojson = File.ReadAllText(arquivo);
                            List<VendasApp> listavendasarquivo = new List<VendasApp>();
                            List<VendasApp> listaAenviar = new List<VendasApp>();
                            listavendasarquivo = JsonConvert.DeserializeObject<List<VendasApp>>(vendasappvaloresarquivojson);

                            foreach (VendasApp lv in listaVendasEnviar)
                            {
                                var lparquivo = listavendasarquivo.FirstOrDefault(x => x.WorkMotorId == lv.WorkMotorId);

                                if (lparquivo == null) { listaAenviar.Add(lv); continue; }
                                if (lv.Ano != lparquivo.Ano) { listaAenviar.Add(lv); continue; }
                                if (lv.Avarias != lparquivo.Avarias) { listaAenviar.Add(lv); continue; }
                                if (lv.CarroFipeId != lparquivo.CarroFipeId) { listaAenviar.Add(lv); continue; }
                                if (lv.ClienteId != lparquivo.ClienteId) { listaAenviar.Add(lv); continue; }
                                if (lv.CombustivelId != lparquivo.CombustivelId) { listaAenviar.Add(lv); continue; }
                                if (lv.Cor != lparquivo.Cor) { listaAenviar.Add(lv); continue; }
                                if (lv.DataEmissao != lparquivo.DataEmissao) { listaAenviar.Add(lv); continue; }
                                if (lv.Defeitos != lparquivo.Defeitos) { listaAenviar.Add(lv); continue; }
                                if (lv.DescProdutos != lparquivo.DescProdutos) { listaAenviar.Add(lv); continue; }
                                if (lv.DescServicos != lparquivo.DescServicos) { listaAenviar.Add(lv); continue; }
                                if (lv.FuncionarioId != lparquivo.FuncionarioId) { listaAenviar.Add(lv); continue; }
                                if (lv.Id != lparquivo.Id) { listaAenviar.Add(lv); continue; }
                                if (lv.NumeroOrcamento != lparquivo.NumeroOrcamento) { listaAenviar.Add(lv); continue; }
                                if (lv.Observacao != lparquivo.Observacao) { listaAenviar.Add(lv); continue; }
                                if (lv.OficinaId != lparquivo.OficinaId) { listaAenviar.Add(lv); continue; }
                                if (lv.Placa != lparquivo.Placa) { listaAenviar.Add(lv); continue; }
                                if (lv.Quilometragem != lparquivo.Quilometragem) { listaAenviar.Add(lv); continue; }
                                if (lv.Status != lparquivo.Status) { listaAenviar.Add(lv); continue; }
                                if (lv.Total != lparquivo.Total) { listaAenviar.Add(lv); continue; }
                                if (lv.TotalPecas != lparquivo.TotalPecas) { listaAenviar.Add(lv); continue; }
                                if (lv.TotalServicos != lparquivo.TotalServicos) { listaAenviar.Add(lv); continue; }
                                if (lv.WorkMotorId != lparquivo.WorkMotorId) { listaAenviar.Add(lv); continue; }

                            }

                            if (listaAenviar.Count == 0)
                            {
                                /********************* NÃO FAZ NADA *********************/
                                
                                return true;
                                /********************* NÃO FAZ NADA *********************/
                            }
                            else
                            {
                                /********************* MÉTODO DE ENVIO DE DADOS *********************/
                                string result;
                                List<VendasApp> lverro = new List<VendasApp>();
                                List<VendasApp> lvsucesso = new List<VendasApp>();

                                while (listaAenviar.Count > 0)
                                {
                                    Thread.Sleep(10000);
                                    var listaDividida = listaAenviar.Take(100);
                                    var jsonEnviar = ("{'cnpj' : '" + cnpj.Trim() + "', ordemServico: '" + JsonConvert.SerializeObject(listaDividida) + "'}");
                                    result = EnviarDados(jsonEnviar, url);

                                    if (result.ToUpper() != "TRUE") { lverro.AddRange(listaDividida); }
                                    else { lvsucesso.AddRange(listaDividida); }

                                    listaAenviar.RemoveRange(0, listaDividida.Count());
                                }

                                if (lvsucesso.Count > 0 && lverro.Count == 0)
                                {
                                    using (StreamWriter writer = new StreamWriter("AppWkmVendasValores.json"))
                                    {
                                        writer.WriteLine(JsonConvert.SerializeObject(listaVendasEnviar));
                                        return true;
                                    }
                                }

                                if (lverro.Count > 0)
                                {
                                    using (StreamWriter writer = new StreamWriter("AppWkmVendasValoresErroEnvio.json"))
                                    {
                                        writer.WriteLine(JsonConvert.SerializeObject(lverro));
                                    }
                                }

                                if (lvsucesso.Count > 0)
                                {
                                    using (StreamWriter writer = new StreamWriter("AppWkmVendasValores.json"))
                                    {
                                        writer.WriteLine(JsonConvert.SerializeObject(lvsucesso));
                                    }
                                }
                                /******************* FIM MÉTODO DE ENVIO DE DADOS *******************/
                            }
                            return true;
                        }
                        else
                        {
                            /********************* MÉTODO DE ENVIO DE DADOS *********************/
                            string result = "";
                            List<VendasApp> lverro = new List<VendasApp>();
                            List<VendasApp> lvsucesso = new List<VendasApp>();

                            while (listaVendasEnviar.Count > 0)
                            {
                                var listaDividida = listaVendasEnviar.Take(100);
                                var jsonEnviar = ("{'cnpj' : '" + cnpj.Trim() + "', ordemServico: '" + JsonConvert.SerializeObject(listaDividida) + "'}");
                                result = EnviarDados(jsonEnviar, url);

                                if (result.ToUpper() != "TRUE") { lverro.AddRange(listaDividida); }
                                else { lvsucesso.AddRange(listaDividida); }

                                listaVendasEnviar.RemoveRange(0, listaDividida.Count());
                            }

                            if (lvsucesso.Count > 0 && lverro.Count == 0)
                            {
                                using (StreamWriter writer = new StreamWriter("AppWkmVendasValores.json"))
                                {
                                    writer.WriteLine(JsonConvert.SerializeObject(lvsucesso));
                                    return true;
                                }
                            }

                            if (lverro.Count > 0)
                            {
                                using (StreamWriter writer = new StreamWriter("AppWkmVendasValoresErroEnvio.json"))
                                {
                                    writer.WriteLine(JsonConvert.SerializeObject(lverro));
                                }
                            }

                            if (lvsucesso.Count > 0)
                            {
                                using (StreamWriter writer = new StreamWriter("AppWkmVendasValores.json"))
                                {
                                    writer.WriteLine(JsonConvert.SerializeObject(lvsucesso));
                                }
                            }
                            /******************* FIM MÉTODO DE ENVIO DE DADOS *******************/
                        }
                        return true;
                    }
                    return true;
                }
                catch (Exception error)
                {
                    using (StreamWriter writer = new StreamWriter("AppWkmVendasValoresErro.json"))
                    {
                        writer.WriteLine("Erro de envio:: " + error.ToString());
                    }

                    return false;
                }
            }
            return true;
        }

        /* FEITO */
        public static string EnviarDados(string jsonEnviar, string url)
        {
            string retorno = "";
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(jsonEnviar);
                streamWriter.Flush();
                streamWriter.Close();
            }

            try
            {
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    var jsonRetorno = result;
                    retorno = jsonRetorno;
                    return retorno;
                }
            }
            catch (Exception erro)
            {
                return erro.ToString();
            }
        }

        public static string LimpezaGeral(string input)
        {
            return string.IsNullOrEmpty(input) ? input : Regex.Replace(input, "[^a-zA-Z0-9@\\._\\-\\s]", string.Empty);
        }

        public static string LimpezaString(string input)
        {
            return string.IsNullOrEmpty(input) ? input : Regex.Replace(input, "[^a-zA-Zà-üÀ-Ü0-9\\s]", string.Empty);
        }

        public static string ValidaAnoModeloFipe (string Ano)
        {
            if(Ano == "")
            {
                return "0";
            }

            return Ano;
        }

        public static string StringVaziaInt(string texto)
        {

            if (texto == "")
            {
                return "0";
            }

            return texto;
        }

        public static string removeCarater(string texto)
        {
            string input = texto;
            string pattern = @"(?i)[^0-9a-záéíóúàèìòùâêîôûãõç\s]";
            string replacement = "";
            Regex rgx = new Regex(pattern);
            string result = rgx.Replace(input, replacement);

            return result;
        }

        public static string MascaraPlaca(string placa)
        {
            placa = placa.ToString().Replace("-", "");

            if(placa.ToString().Trim().Length < 7)
            {
                placa = "";
            }

            return placa;
        }

        public static string ValidaDescricao(string descricao)
        {
            if (descricao.ToString().Trim() == "0" || descricao.ToString().Trim() == " " || descricao.ToString().Trim() == "")
            {
                descricao = "SEM DESCRICAO";
            }

            return descricao;
        }

        
    }
}
