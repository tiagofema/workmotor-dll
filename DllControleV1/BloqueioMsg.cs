﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Threading;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Security.Permissions;
using System.IO;
using System.Windows.Forms;
using System.Net;
using System.Xml;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Data.SqlClient;

using System.Configuration;
using Microsoft.Win32;
using System.Data;

using System.Net.NetworkInformation;
using System.Xml.Serialization;



namespace DllControleV1
{
        

    public class StatusOficina
    {
        sbyte status;
        string dataatualiza;

        public void StatusSet(sbyte status, string dataatualiza)
        {
            this.status = status;
            this.dataatualiza = dataatualiza;
        }

        public sbyte StatusGetStatus()
        {
            return this.status;
        }

        public string StatusGetDataAtualiza()
        {
            return this.dataatualiza;
        }
    }

    

    [ComVisible(true)]
    public class Controle
    {

        private int time_thread = 0;
        private const string url_licenca = "http://201.77.177.42/QueridoCarro/avalidawkmv2.aspx";

        public int teste_thread()
        {

            //Thread thread = new Thread(new ThreadStart(loop_teste));
            Thread thread = new Thread(new ThreadStart(workt));
           
            thread.SetApartmentState(ApartmentState.STA);   // ** precisa colocar para nao dar erro na chamada do form no thread    **

            thread.Start();

            
            return 1;


        }

        public static void log(string nome_arquivo, string msglog)
        {
            //string pathfile = Directory.GetCurrentDirectory() +'\\' + nome_arquivo;
            // ** não mexer no caminho pq no gauge ja usa o caminho padrao  **
            string pathfile = nome_arquivo;

            if (!System.IO.File.Exists(pathfile))
                System.IO.File.Create(pathfile).Close();

            try
            {
                System.IO.TextWriter arquivo = System.IO.File.AppendText(pathfile);
                arquivo.WriteLine(msglog);
                arquivo.Close();
            }
            catch { }

        }

        protected void loop_teste()
        {
            string tselect = "select cfglicenca from cfgsist where cfgcodigo = 1";
            DataTable dt;
            string nome_arquivo = "C:/xacesso.txt";

            if (!System.IO.File.Exists(nome_arquivo))
                System.IO.File.Create(nome_arquivo).Close();

            while (true)
            {
                Thread.Sleep(2000);
                try
                {
                    dt = getda_data_vfp(tselect);
                    System.IO.TextWriter arquivo = System.IO.File.AppendText(nome_arquivo);

                    if (dt.Rows.Count > 0)
                    {
                        arquivo.WriteLine("Dei Select." + DateTime.Now.ToString());
                    }
                    else arquivo.WriteLine("*Não achei." + DateTime.Now.ToString());

                    arquivo.Close();
                }
                catch 
                {
                    System.IO.TextWriter arquivo = System.IO.File.AppendText(nome_arquivo);
                    arquivo.WriteLine("ERRO :" + DateTime.Now.ToString());
                    arquivo.Close();
                }

            }
        }


        public int lock_system(int time_thread_parm)
        {
            time_thread = time_thread_parm;
            Thread thread = new Thread(new ThreadStart(locks));
            //Thread thread2 = new Thread(new ThreadStart(BloqueioMsgPeriodicidade.MensagemBloqueia));

            ExtraFoxPro.FoxDbfPrg();
            
            thread.SetApartmentState(ApartmentState.STA);   // ** precisa colocar para nao dar erro na chamada do form no thread    **
            thread.Start();

            BloqueioMsgPeriodicidade.MensagemBloqueia(time_thread);
            //thread2.SetApartmentState(ApartmentState.STA);   // ** precisa colocar para nao dar erro na chamada do form no thread    **
            //thread2.Start();

            
            return 1;
        }

        protected void locks()
        {
            int ret_post = 0;

            //MessageBox.Show("inicio : " + time_thread.ToString());
            while (true)
            {
                Thread.Sleep(time_thread);

                //MessageBox.Show("AAA1");

                //if (!String.IsNullOrEmpty(xml_post) && !String.IsNullOrEmpty(licenca_atual()))
                if ( !String.IsNullOrEmpty(licenca_atual()))
                {
                    //MessageBox.Show("1");

                    if (IsConnectedToInternet())
                    {
                        //MessageBox.Show("2");
                        //ret_post = oficina_status();
                        StatusOficina retorno = new StatusOficina();
                        retorno = oficina_status();
                        ret_post = retorno.StatusGetStatus();

                        //MessageBox.Show("ret_post : " + ret_post.ToString());

                        if (ret_post == 1)  //  ** status 5 = bloqueado **
                            apaga_licença();
                        else
                        {
                            //  **  Mensagem de Travamento  **
                            if(ret_post == 2)
                            {
                                //  **  Do servidor
                                string dataatualiza = retorno.StatusGetDataAtualiza() ;

                                //  **  Carrego os dados ou crio o arquivo mb.dat se não existir   **
                                MsgBloqueio msgbloqueio = new MsgBloqueio();
                                msgbloqueio.Load();

                                //MessageBox.Show(msgbloqueio.MensagemGet());
                                DateTime datamsgservidor;

                                if (DateTime.TryParse(dataatualiza, out datamsgservidor)){}
                                else
                                    datamsgservidor = DateTime.MinValue;

                                //MessageBox.Show("local:" + msgbloqueio.DataGet().ToString() + " < " + datamsgservidor.ToString() + "< Servidor");

                                //  ** Se a data do arquivo menor que do site deve atualizar    **
                                if (msgbloqueio.DataGet() < datamsgservidor)
                                {
                                    //MessageBox.Show("Deve trocar a msg");

                                    string mensagemnova = msgbloqueio.Download();

                                    //MessageBox.Show("mensagemnova : " + mensagemnova);

                                    msgbloqueio.Set(datamsgservidor, mensagemnova);
                                    msgbloqueio.Save();
                                }//else
                                    //MessageBox.Show("Msg Atualizada");


                                //  **  Se as configurações de bloqueio não foram mudadas não altero a licença  **
                                //  **  caso sejam diferentes atualizo a licenca **
                                //  **  Devo baixar a licença para comparar !!!**


                            }
                            // ** Se algo mudou deve atualizar a licenca pois o cliente pode ter a trava de bloqueio removido ou ligada **

                            string licencaonline = pegalicenca_online();
                            string licencalocal = licenca_atual();

                            //MessageBox.Show(licencaonline + " | " + licencalocal);

                            if (licencaonline.Length > 0 && licencalocal.Length > 0)
                            {
                                Licenca obj_licencaonline, obj_licencalocal = new Licenca();

                                obj_licencaonline = DecodeLicenca.Decode(licencaonline);
                                obj_licencalocal = DecodeLicenca.Decode(licencalocal);

                                //string comparacao = obj_licencaonline.LiqBlock.ToString() + " != " + obj_licencalocal.LiqBlock.ToString() + "\n" +
                                //                    obj_licencaonline.TempoMsgB.ToString() + " !=  " + obj_licencalocal.TempoMsgB.ToString() + "\n" +
                                //                    obj_licencaonline.PeriodicidadeMsgBCode.ToString() + " !=  " + obj_licencalocal.PeriodicidadeMsgBCode.ToString();
                                //MessageBox.Show(comparacao);

                                //  **  Se qquer dado da msg de bloqueio for diferente  **
                                if (obj_licencaonline.LiqBlock != obj_licencalocal.LiqBlock ||
                                   obj_licencaonline.TempoMsgB != obj_licencalocal.TempoMsgB ||
                                   obj_licencaonline.PeriodicidadeMsgBCode != obj_licencalocal.PeriodicidadeMsgBCode)
                                {
                                    // ** Deve atualizar a licença !!!!!!
                                    //MessageBox.Show("Algo foi alterado! Deve atualizar a licenca");
                                    Controle.atualiza_licença(licencaonline);
                                }

                            }//else
                               // MessageBox.Show("Deu RUIM!!!");
                            
                        }
                        
                        //Application.Run(new Form1(msgbloqueio, Trava, segundos_lock));
                    }
                }
            }
        }

        public void pega_Status_Codigo()
        {
         /*
            StatusOficina retorno = new StatusOficina();
            retorno = oficina_status();
            ret_post = retorno.StatusGetStatus();

            //MessageBox.Show("ret_post : " + ret_post.ToString());

            if (ret_post == 1)  //  ** status 5 = bloqueado **
                apaga_licença();
            else
            {
                
            }
           */
        }

        //protected string licenca_atual()
        public static string licenca_atual()
        {
            string cfglicenca = "";
            string tselect = "select cfglicenca from cfgsist where cfgcodigo = 1"; 

            //log("loggeral.txt", "ENTREI LICENCA");
            try
            {
                DataTable dt = getda_data_vfp(tselect);
                if (!dt.HasErrors)
                {
                    //  **  pega o resultado do select  **
                    foreach (DataRow row in dt.Rows)
                    {
                        cfglicenca = row[0].ToString();
                    }                  
                }
            }
            catch{}

            //log("loggeral.txt", "RETORNO LICENCA");
            //log("loggeral.txt", cfglicenca.Trim());


            return cfglicenca.Trim();
        }
        /*
        protected string gera_xml_lock()
        {
            //string xml_post ="";
            XmlDocument xmldoc;
            XmlNode xmlnode;
            xmldoc = new XmlDocument();

            string tselect = "select cfgtitcgc, cfgrzsocia, checkcode, cfgnomefar from cfgsist where cfgcodigo = 1"; //  **  APAGA A KICENCA **
            //string tpath = Directory.GetCurrentDirectory();

            //OleDbConnection conn = new OleDbConnection(@"Provider=vfpoledb;Data Source=" + tpath + ";Collating Sequence=machine;");
            //OleDbCommand command = new OleDbCommand(tselect, conn);

            try
            {
                //conn.Open();
                //DataTable dt = new DataTable();
                //dt.Load(command.ExecuteReader());
                //conn.Close();
                DataTable dt = getda_data_vfp(tselect);

                //  **  pega o resultado do select  **
                foreach (DataRow row in dt.Rows)
                {
                    xmlnode = xmldoc.CreateXmlDeclaration("1.0", "UTF-8", null);
                    xmldoc.AppendChild(xmlnode);

                    xmlnode = xmldoc.CreateElement("parameters");
                    xmldoc.AppendChild(xmlnode);

                    XmlNode userNode = xmldoc.CreateElement("CGC");
                    userNode.InnerText = row[0].ToString();
                    xmlnode.AppendChild(userNode);

                    userNode = xmldoc.CreateElement("RZSOCIA");
                    userNode.InnerText = row[1].ToString();
                    xmlnode.AppendChild(userNode);

                    userNode = xmldoc.CreateElement("CHECKCODE");
                    userNode.InnerText = row[2].ToString();
                    xmlnode.AppendChild(userNode);

                    userNode = xmldoc.CreateElement("NFANTASIA");
                    userNode.InnerText = row[3].ToString();
                    xmlnode.AppendChild(userNode);

                    // MessageBox.Show(xmldoc.OuterXml);

                    //foreach (DataColumn column in dt.Columns)
                    //{
                    //    MessageBox.Show(row[column].ToString());
                    //}
                }

            }
            //catch (Exception e)
            catch
            {
                //MessageBox.Show("Erro Gerando XML : " + e.Message);
            }

            return xmldoc.OuterXml.Trim();
        }
        */
        public static bool IsConnectedToInternet()
        {
            bool connection;
            try
            {
                System.Net.IPHostEntry objIPHE = System.Net.Dns.GetHostEntry("www.google.com");
                connection = true;
            }
            catch
            {
                connection = false;
            }
            return connection;
        }

        public string versao_dll()
        {
            return "0.5";
        }

        public int msg_controle(string cnpj)
        {
            //MessageBox.Show("Entrei controle msg");

            cnpj = cnpj.Replace(".","").Replace("/","").Replace("-","");

            Thread thread = new Thread(() => thr_browser(cnpj));

            //  **  feito via thread para dar segurar a tela dos clientes   **
            thread.SetApartmentState(ApartmentState.STA);   // ** precisa colocar para nao dar erro na chamada do form no thread    **
            thread.Start();

            return 1;
        }

        public void thr_browser(string cnpj)
        {
            string Mensagem = "";
            sbyte Trava = 0;
            sbyte TipoMsg = 1;  // ** alerta    inicial
            sbyte segundos_lock = 0;

            //MessageBox.Show(cnpj);

            if (IsConnectedToInternet())
            {
                try
                {
                    MsgControle.CtrMsgGet ws = new MsgControle.CtrMsgGet();
                    ws.Execute(ref cnpj, ref TipoMsg, ref Mensagem, ref Trava, ref segundos_lock);

                    //MessageBox.Show("MEnsagem: " + Mensagem);

                    //log("log_ret_msg.txt", Mensagem);

                    if (!String.IsNullOrEmpty(Mensagem))
                    {
                        //Application.Run(new Form1(Mensagem, Trava, segundos_lock));

                        // ** Abro a janela sobre o aplicativo pai  **

                        Process currentProcess = Process.GetCurrentProcess();
                        IntPtr myFormParentHandle = currentProcess.MainWindowHandle;

                        Form form = new Form1(Mensagem, Trava, segundos_lock);
                        form.ShowDialog(new BloqueioMsgPeriodicidade.WindowWrapper(myFormParentHandle));
                        form.Dispose();

                    }


                }
                catch
                {
                    //MessageBox.Show("Deu Ruim!");
                }
            }
        }

        public static StatusOficina oficina_status()
        {
            string cnpj = pega_cnpj().Trim();  //  **  pega cnpj para pegar a msg  **
            sbyte statusoficina = 0;
            string dataatualiza = "";

            StatusOficina retorno = new StatusOficina();
            try
            {
                OfStatusMsg.LiOfStatusMsgWS ws = new OfStatusMsg.LiOfStatusMsgWS();
                ws.Execute(ref cnpj, ref statusoficina, ref dataatualiza);

                retorno.StatusSet(statusoficina, dataatualiza);
            }
            catch(Exception ex)
            {   //  **  Erro    **
                retorno.StatusSet(9, DateTime.Today.ToString());
            }

            return retorno;

            //OfStatusMsg
/*      //  **  OLD **
            int retorno = 0;
            OfBloqueada.LiOfStatusWS ws = new OfBloqueada.LiOfStatusWS();
            string cnpj = pega_cnpj().Trim();  //  **  pega cnpj para pegar a msg  **
            try
            {
                retorno = ws.Execute(cnpj);
            }
            catch { }

            return retorno;
*/
        }
/*
        public int post_lic(string xmlparm) //  **  post dos dados para pegar o status do clienet   **
        {
            // Create a request using a URL that can receive a post. 
            WebRequest request = WebRequest.Create("http://201.77.177.42/QueridoCarro/avalidawkm.aspx");
            // Set the Method property of the request to POST.
            request.Method = "POST";
            // Create POST data and convert it to a byte array.
            //string postData = "This is a test that posts this string to a Web server.";

            //string postData = System.IO.File.ReadAllText(@"C:\xml.xml");
            string postData = xmlparm;
            //MessageBox.Show(postData);

            byte[] byteArray = Encoding.UTF8.GetBytes(postData);

            try
            {
                // Set the ContentType property of the WebRequest.
                request.ContentType = "application/x-www-form-urlencoded";
                // Set the ContentLength property of the WebRequest.
                request.ContentLength = byteArray.Length;
                // Get the request stream.
                Stream dataStream = request.GetRequestStream();
                // Write the data to the request stream.
                dataStream.Write(byteArray, 0, byteArray.Length);
                // Close the Stream object.
                dataStream.Close();
                // Get the response.
                WebResponse response = request.GetResponse();
                // Display the status.
                Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                // Get the stream containing content returned by the server.
                dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.
                string responseFromServer = reader.ReadToEnd();
                // Display the content.
                //Console.WriteLine(responseFromServer);
                //MessageBox.Show(responseFromServer);
                // Clean up the streams.
                reader.Close();
                dataStream.Close();
                response.Close();

                XmlDocument RetXml = new XmlDocument();
                RetXml.LoadXml(responseFromServer);

                string StatusCode = RetXml.SelectSingleNode("parameters").ChildNodes[0].InnerText;
                string StatusDescri = RetXml.SelectSingleNode("parameters").ChildNodes[1].InnerText;

                return int.Parse(StatusCode);

            }
            //catch (Exception e)
            catch
            {
                //MessageBox.Show(e.Message);
                return 0;
            }
            
            //return 1;
        }
*/

        public static string ativo_verifica_chamado()
        {
            string tselect = "select Cfgativoqc from cfgsist where cfgcodigo = 1";

            string ativo = "S";

            try
            {

                DataTable dt = getda_data_vfp(tselect);
                if (!dt.HasErrors)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            ativo = row[0].ToString();
                        }

                    }
                    else ativo = "S";
                }
                return ativo;

            }
            catch
            {
                return "S";
            }
        }

        public static int pega_sleep_chamado()
        {
            string tselect = "select Cfgtimeqc from cfgsist where cfgcodigo = 1";

            int time = 60;

            try
            {

                DataTable dt = getda_data_vfp(tselect);
                if (!dt.HasErrors)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            time = Convert.ToInt32(row[0]);
                        }

                    }
                    else time = 60;
                }
                return time;

            }
            catch
            {
                return 60;
            }
        }

        public static string pega_url_acessoqc()
        {
            string tselect = "select Cfgurlaceq from cfgsist where cfgcodigo = 1";
            string url = "";

            try
            {

                DataTable dt = getda_data_vfp(tselect);
                if (!dt.HasErrors)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            url = row[0].ToString();
                        }

                    }
                    else url = "";
                }
                return url;

            }
            catch
            {
                return "";
            }
        }

        public static string pega_cnpj()
        {
            string tselect = "select cfgtitcgc from cfgsist where cfgcodigo = 1"; //  **  APAGA A KICENCA **
            //string tpath = Directory.GetCurrentDirectory();
            string cnpj = "";

            //OleDbConnection conn = new OleDbConnection(@"Provider=vfpoledb;Data Source=" + tpath + ";Collating Sequence=machine;");
            //OleDbCommand command = new OleDbCommand(tselect, conn);

            try
            {

                DataTable dt = getda_data_vfp(tselect);
                if (!dt.HasErrors)
                {
                    if (dt.Rows.Count > 0) 
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            cnpj = row[0].ToString();
                            cnpj = cnpj.Replace("-", "").Replace("/", "").Replace(".", "");
                        }

                    }
                    else cnpj = "";
                }
                return cnpj;
                
            }
            catch 
            {
                return "";
            }

        }

        public static int pega_venda()
        {
            string tselect = "select Ndrlstdoc FROM numerado WHERE UPPER(ndrcod) = 'VEN'"; //  ** PEGA ULTIMA VENDA **
            //string tpath = Directory.GetCurrentDirectory();
            int venda = -2;

            //OleDbConnection conn = new OleDbConnection(@"Provider=vfpoledb;Data Source=" + tpath + ";Collating Sequence=machine;");
            //OleDbCommand command = new OleDbCommand(tselect, conn);

            try
            {

                DataTable dt = getda_data_vfp(tselect);
                if (!dt.HasErrors)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            venda = Convert.ToInt32(row[0]);
                        }

                    }
                    else return venda;
                }
                return venda;

            }
            catch
            {
                return venda;
            }

        }

        public static string pega_cliente(string select)
        {
            string tselect = select;
            //string tpath = Directory.GetCurrentDirectory();
            string razaosocial = "CONSUMIDOR";

            //OleDbConnection conn = new OleDbConnection(@"Provider=vfpoledb;Data Source=" + tpath + ";Collating Sequence=machine;");
            //OleDbCommand command = new OleDbCommand(tselect, conn);

            try
            {

                DataTable dt = getda_data_vfp(tselect);
                if (!dt.HasErrors)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            razaosocial = row[0].ToString();
                        }

                    }
                    else return razaosocial;
                }
                return razaosocial;

            }
            catch
            {
                return razaosocial;
            }

        }

        public static int pega_cliente_numerador()
        {
            string tselect = "select Ndrlstdoc FROM numerado WHERE UPPER(ndrcod) = 'CLI'"; //  ** PEGA ULTIMO CLIENTE **
            //string tpath = Directory.GetCurrentDirectory();
            int codigo = -1;

            //OleDbConnection conn = new OleDbConnection(@"Provider=vfpoledb;Data Source=" + tpath + ";Collating Sequence=machine;");
            //OleDbCommand command = new OleDbCommand(tselect, conn);

            try
            {

                DataTable dt = getda_data_vfp(tselect);
                if (!dt.HasErrors)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            codigo = Convert.ToInt32(row[0]);
                        }

                    }
                    else return codigo;
                }
                return codigo;

            }
            catch
            {
                return -1;
            }

        }

        public static string pega_objeto(string select)
        {
            string tselect = select;
            //string tpath = Directory.GetCurrentDirectory();
            string objeto = "NÃO ENCONTRADO";

            //OleDbConnection conn = new OleDbConnection(@"Provider=vfpoledb;Data Source=" + tpath + ";Collating Sequence=machine;");
            //OleDbCommand command = new OleDbCommand(tselect, conn);

            try
            {

                DataTable dt = getda_data_vfp(tselect);
                if (!dt.HasErrors)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            objeto = row[0].ToString();
                        }

                    }
                    else return objeto;
                }
                return objeto;

            }
            catch
            {
                return objeto;
            }

        }

        private void apaga_licença()
        {
            Process currentProcess = Process.GetCurrentProcess();
            IntPtr myFormParentHandle = currentProcess.MainWindowHandle;

            //string bloq_msg = "";
            sbyte segundos_lock = 0;
            string cnpj = "";

            string Mensagem = "";
            sbyte Trava = 0;
            sbyte TipoMsg = 2;  // ** Bloqueio do Sistema  **

            string tselect = "update cfgsist set cfglicenca = ''  where cfgcodigo = 1"; //  **  APAGA A LICENCA **
            //string tpath = Directory.GetCurrentDirectory();

            try
            {
                DataTable dt = getda_data_vfp(tselect);

                if (!dt.HasErrors)
                {
                    cnpj = pega_cnpj().Trim();  //  **  pega cnpj para pegar a msg  **

                    if (!String.IsNullOrEmpty(cnpj))
                    {
                        MsgControle.CtrMsgGet ws = new MsgControle.CtrMsgGet();
                        ws.Execute(ref cnpj, ref TipoMsg, ref Mensagem, ref Trava, ref segundos_lock);

                        //if (!String.IsNullOrEmpty(Mensagem))
                        //Application.Run(new Form1(Mensagem, Trava, segundos_lock));

                        Form form = new Form1(Mensagem, Trava, segundos_lock);
                        form.ShowDialog(new BloqueioMsgPeriodicidade.WindowWrapper(myFormParentHandle));
                        form.Dispose();

                        currentProcess.Kill();
                    }
                }



            }
            catch { }
            //catch (Exception e)
            
            //{
                //MessageBox.Show("Erro: " + e.ToString());
            //}
        }

        public static DataTable getda_data_vfp(string query)
        {
            DataTable retorno = new DataTable();
            string tpath = Directory.GetCurrentDirectory();

            if (System.IO.File.Exists("multilojas.dat") &&
                (query == "SELECT marcodigo, marca, martipo FROM marcas" || 
                query == "SELECT famcodigo as 'codigo', famnome as 'descricao' FROM famcad" || 
                query == "SELECT emprzsocia, empnomfan, empcgccpf, empnscest, empendere, empnumero, empcomple, empbairro, empcidade, empestado, empcep, empddd, empfone, empfax, empemail, empcodigo, empdddcel, empcel, empdtnasc, empsexo, empresas.empcidcodi FROM empresas" ||
                query == "SELECT Objcodigo, Objeto, Objmodelo FROM Objetmat" || 
                query == "SELECT p.procodigo, p.marcodigo, p.famcodigo, p.prodescri, p.proncomer, p.promodelo, p.protamanh, p.propeso, p.prolocalz, p.proprcvd, p.prounidade, p.prodtatulz, p.proestoq, p.proobs, f.famcodigo, m.marcodigo, p.prodtatulz, p.procodbarr FROM produtos p, Marcas m, famcad f WHERE m.marcodigo = p.marcodigo AND f.famcodigo = p.famcodigo" ||
                query == "SELECT empveicu.empcodigo, empveicu.empvobjcod, empveicu.EmpVComb, empveicu.empvplaca, empveicu.empVano, empveicu.EmpvCor, empveicu.empvKm, empveicu.EmpTpCombu, empveicu.empvobjeto FROM empveicu, Objetmat, empresas WHERE empveicu.empvobjcod = Objetmat.Objcodigo AND empresas.empcodigo = empveicu.empcodigo" || 
                query.Contains("INSERT INTO EMPRESAS") || query.Contains("insert into Empveicu"))) {
                tpath = "c:\\externas_wkm\\";
                if(query == "SELECT p.procodigo, p.marcodigo, p.famcodigo, p.prodescri, p.proncomer, p.promodelo, p.protamanh, p.propeso, p.prolocalz, p.proprcvd, p.prounidade, p.prodtatulz, p.proestoq, p.proobs, f.famcodigo, m.marcodigo, p.prodtatulz, p.procodbarr FROM produtos p, Marcas m, famcad f WHERE m.marcodigo = p.marcodigo AND f.famcodigo = p.famcodigo")
                {
                    query = "SELECT p.procodigo, p.marcodigo, p.famcodigo, p.prodescri, p.proncomer, p.promodelo, p.protamanh, p.propeso, p.prolocalz, p.proprcvd, p.prounidade, p.prodtatulz, p.proestoq, p.proobs, f.famcodigo, m.marcodigo, p.prodtatulz, p.procodbarr FROM " + Directory.GetCurrentDirectory()  + "\\produtos p, "+tpath+ "Marcas m, " + tpath + "famcad f WHERE m.marcodigo = p.marcodigo AND f.famcodigo = p.famcodigo";
                    
                }
            }

            OleDbConnection conn = new OleDbConnection(@"Provider=vfpoledb;Data Source=" + tpath + ";Collating Sequence=machine;");
            OleDbCommand command = new OleDbCommand(query, conn);

            //TryLabel:
            try
            {
                conn.Open();
                Application.DoEvents();
                //OleDbDataReader Reader = command.ExecuteReader();
                //retorno.Load(Reader);
                retorno.Load(command.ExecuteReader());

                //Reader.Close();
                conn.Close();
            }
            catch (Exception ex)
            {
                retorno.Columns.Add(new DataColumn("Col1", typeof(int)));
                retorno.Rows.Add(retorno.NewRow());
                retorno.Rows[0].RowError = "Exception FOXPRO:" + ex.Message;
                conn.Close();
            }

            return retorno;
        }

        public static DataTable setda_data_vfp(string query)
        {
            DataTable retorno = new DataTable();
            string tpath = Directory.GetCurrentDirectory();
            string resultado = "";

            OleDbConnection conn = new OleDbConnection(@"Provider=vfpoledb;Data Source=" + tpath + ";Collating Sequence=general;");
            OleDbCommand command = new OleDbCommand (query, conn);

            //TryLabel:
            try
            {
                conn.Open();
                command.Connection = conn;
                command.CommandText = query;
                //Application.DoEvents();
                //OleDbDataReader Reader = command.ExecuteReader();
                //retorno.Load(Reader);
                command.ExecuteNonQuery();
                command.Transaction.Commit();
                //Reader.Close();
                conn.Close();
                resultado = "Sucesso";
            }
            catch (Exception ex)
            {
                retorno.Columns.Add(new DataColumn("Col1", typeof(int)));
                retorno.Rows.Add(retorno.NewRow());
                retorno.Rows[0].RowError = "Exception FOXPRO:" + ex.Message;
                conn.Close();
                resultado = retorno.Rows[0].RowError;
            }

            return retorno;
        }

        public static DataTable getda_data_vfp_sem_try(string query)
        {
            DataTable retorno = new DataTable();
            string tpath = Directory.GetCurrentDirectory();

            OleDbConnection conn = new OleDbConnection(@"Provider=vfpoledb;Data Source=" + tpath + ";Collating Sequence=machine;");
            OleDbCommand command = new OleDbCommand(query, conn);

            conn.Open();
            retorno.Load(command.ExecuteReader());
            conn.Close();

            return retorno;
        }


        public Boolean get_pid()
        {
            Process currentProcess = Process.GetCurrentProcess();
            //MessageBox.Show(currentProcess.Id.ToString());

            //currentProcess.Kill();
            Thread thread = new Thread(new ThreadStart(workt));
            thread.Start();

            //MessageBox.Show("passei");
            return true;
        }

        public void workt()
        {
            while(true)
            {
                Thread.Sleep(2000);
                MessageBox.Show(DateTime.Now.ToString("HH:mm:ss tt"));
            }
        }

        public static void atualiza_licença(string licenca)
        {
            string tselect = "update cfgsist set cfglicenca = '" + licenca + "' where cfgcodigo = 1";
            try
            {
                DataTable dt = getda_data_vfp(tselect);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        public string pegalicenca_online()
        {
            string tselect = "select cfgtitcgc, cfgrzsocia, cfgnomefar, checkcode from cfgsist where cfgcodigo = 1";
            string cfgtitcgc = "", cfgrzsocia = "", cfgnomefar = "", checkcode = "", licenca = "";

            try
            {
                DataTable dt = getda_data_vfp(tselect);
                if (!dt.HasErrors)
                {
                    //  **  pega o resultado do select  **
                    foreach (DataRow row in dt.Rows)
                    {
                        cfgtitcgc = row[0].ToString();
                        cfgrzsocia = row[1].ToString();
                        cfgnomefar = row[2].ToString();
                        checkcode = row[3].ToString();
                    }

                    XmlDocument xmlDoc = new XmlDocument();
                    XmlNode rootNode = xmlDoc.CreateElement("parameters");
                    xmlDoc.AppendChild(rootNode);

                    XmlNode userNode = xmlDoc.CreateElement("CGC");
                    userNode.InnerText = cfgtitcgc.Trim().Replace("-", "").Replace("/", "").Replace(".", "");
                    rootNode.AppendChild(userNode);

                    userNode = xmlDoc.CreateElement("RZSOCIA");
                    userNode.InnerText = cfgrzsocia.Trim();
                    rootNode.AppendChild(userNode);

                    userNode = xmlDoc.CreateElement("CHECKCODE");
                    userNode.InnerText = checkcode.Trim();
                    rootNode.AppendChild(userNode);

                    userNode = xmlDoc.CreateElement("NFANTASIA");
                    userNode.InnerText = cfgnomefar.Trim();
                    rootNode.AppendChild(userNode);

                    //MessageBox.Show(xmlDoc.OuterXml);

                    // Create a request using a URL that can receive a post. 
                    WebRequest request = WebRequest.Create(url_licenca);
                    // Set the Method property of the request to POST.
                    request.Method = "POST";
                    // Create POST data and convert it to a byte array.
                    //string postData = "This is a test that posts this string to a Web server.";
                    string postData = xmlDoc.OuterXml;
                    byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    // Set the ContentType property of the WebRequest.
                    request.ContentType = "application/x-www-form-urlencoded";
                    // Set the ContentLength property of the WebRequest.
                    request.ContentLength = byteArray.Length;
                    // Get the request stream.
                    Stream dataStream = request.GetRequestStream();
                    // Write the data to the request stream.
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    // Close the Stream object.
                    dataStream.Close();
                    // Get the response.
                    WebResponse response = request.GetResponse();
                    // Display the status.
                    //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                    // Get the stream containing content returned by the server.
                    dataStream = response.GetResponseStream();
                    // Open the stream using a StreamReader for easy access.
                    StreamReader reader = new StreamReader(dataStream);
                    // Read the content.
                    string responseFromServer = reader.ReadToEnd();
                    // Display the content.
                    //Console.WriteLine(responseFromServer);
                    //MessageBox.Show(responseFromServer);

                    xmlDoc.LoadXml(responseFromServer);
                    XmlNodeList nodeList = xmlDoc.GetElementsByTagName("CHAVE");
                    foreach (XmlNode node in nodeList)
                    {
                        licenca = node.InnerText;
                    }


                    //XmlNode example = node.SelectSingleNode("Example");
                    //licenca = xmlDoc.SelectSingleNode("CHAVE").InnerText;

                    // Clean up the streams.
                    reader.Close();
                    dataStream.Close();
                    response.Close();
                }


            }
            catch { }

            return licenca;


        }
    }
}
