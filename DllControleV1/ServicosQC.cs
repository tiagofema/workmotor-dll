﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Media;

namespace DllControleV1
{
    class ServicosQC
    { 
        public class Quantidade {

            public int quantidade = 0;

        }

        public static void Main()
        {
            string tpath = Directory.GetCurrentDirectory();
            string cnpj = "";
            bool chamadonovo = true;
            string urlChamado = "http://201.77.177.42/CallMec_WS/CallMec_WS.asmx/qtdChamadosEmAbertoPorOficina";
            cnpj = PegaCnpj();
            string urlchamado = "";
            string senha = cnpj;
            TelaChamadoQC telaServico = new TelaChamadoQC();

            if(cnpj != "")
            {
                if(IsConnectedToInternet() == true)
                {
                    /* MÉTODO PARA VERIFICAR SE TEM CHAMADO */
                    chamadonovo = VerificaChamado(cnpj, urlChamado);

                    if(chamadonovo == true)
                    {
                        SystemSounds.Beep.Play();
                        ChamaTela(urlchamado, telaServico);
                    }
                    /* FIM MÉTODO */
                }
            }
        }

        public static string PegaCnpj()
        {
            string cnpj = "";
            cnpj = Controle.pega_cnpj();
            cnpj = cnpj.Replace("/", "").Replace(",", "").Replace("-", "").Replace(" ", "");

            return cnpj;
        }

        public static void ChamaTela (string urlchamadologin, TelaChamadoQC telaServico)
        {
            
            
            telaServico.FormBorderStyle = FormBorderStyle.FixedSingle;
            telaServico.MaximizeBox = false;
            telaServico.MinimizeBox = false;
            telaServico.TopMost = true;

            telaServico.ShowDialog();

        }

        public static bool IsConnectedToInternet()
        {
            bool connection;
            try
            {
                System.Net.IPHostEntry objIPHE = System.Net.Dns.GetHostEntry("www.google.com");
                connection = true;
            }
            catch
            {
                connection = false;
            }
            return connection;
        }

        public static bool VerificaChamado(string cnpj, string urlverificachamado)
        {
            bool chamadonovo = true;
            string cnpjformatado = cnpj.Replace("/", "").Replace(",", "").Replace("-", "").Replace(" ", "");
            string urlchamado = urlverificachamado;

            try
            {
                if(cnpjformatado != " ")
                {
                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(urlchamado);
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Method = "POST";
                    var json = ("{'cnpj':'" + cnpjformatado.TrimEnd() + "'}");

                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        streamWriter.Write(json);
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                    try
                    {
                        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                        {
                            var result = streamReader.ReadToEnd();
                            var jsonRetorno = JsonConvert.DeserializeObject<Quantidade>(result);

                            if(jsonRetorno.quantidade > 0)
                            {
                                chamadonovo = true;
                            } else
                            {
                                chamadonovo = false;
                            }                          
                        }
                    }
                    catch (Exception erro)
                    {
                        using (StreamWriter writer = new StreamWriter("ErroChamado.txt"))
                        {
                            writer.WriteLine(erro + urlchamado);
                            chamadonovo = false;
                        }
                    }
                } else
                {
                    chamadonovo = false;
                }
            } catch (Exception Ex)
            {
                using (StreamWriter writer = new StreamWriter("ErroChamado.txt"))
                {
                    writer.WriteLine(Ex.Message + urlchamado);
                    chamadonovo = false;
                }
            }
            return chamadonovo;
        }
    }   
}
