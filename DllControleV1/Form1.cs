﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.ComponentModel;
using System.Diagnostics;


namespace DllControleV1
{
    public partial class Form1 : Form
    {
        short segundos_trava;

        public Form1(string Mensagem, sbyte Trava, short segundos_lock)
        {
            InitializeComponent();
            label1.Visible = false;
            this.Text = "WorkMotor";
            this.segundos_trava = segundos_lock;

            this.BringToFront();
            //this.progressBar1.Maximum = int.Parse(this.label1.Text);

            this.progressBar1.Maximum = segundos_lock;

            //  **  se existe alguma marca de travar a mensagem **
            if (Trava == 1)
            {
                if (segundos_lock == 0)
                    segundos_lock = 5;  //  **  se nao passar o tempo  o padrão sera 5s **
                
                //  **  fixa o campo acima de tudo  **
                //this.TopMost = true;
                
                btnFechar.Enabled = false;
                this.btnFechar.Visible = false;

                btnFechar.Text = "Aguarde  " + segundos_lock.ToString();
                label1.Text = segundos_lock.ToString(); 
                timer1.Start();
            }
            else 
            {
                //  ** mostra o close do form  **
                this.btnFechar.Visible = true;
                this.ControlBox = true;
                btnFechar.Enabled = true;

                progressBar1.Visible = false;

                btnFechar.Text = "Fechar";
                label1.Text = "";

                
            }

            webBrowser1.DocumentText = Mensagem;

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void timer1_Tick_1(object sender, EventArgs e)
        {
            //label1.Text = (int.Parse(label1.Text) - 1).ToString(); //lowering the value - explained above
            //btnFechar.Text = "Aguarde  " + label1.Text;

            //int i = int.Parse(label1.Text);


            this.progressBar1.Increment(1);

            //progressBar1.Refresh();
            //progressBar1.CreateGraphics().DrawString(i.ToString() + " segundos", new Font("Arial",
            //                                          (float)10.25, FontStyle.Regular),
            //                                          Brushes.Black, new PointF(progressBar1.Width / 2 - 10, progressBar1.Height / 2 - 7));

            //if (int.Parse(label1.Text) == 0)  //if the countdown reaches '0', we stop it
            if (this.segundos_trava == 0)
            {
                timer1.Stop();
                this.ControlBox = true;
                btnFechar.Enabled = true;
                label1.Text = "";
                btnFechar.Text = "Fechar " + label1.Text;

                this.btnFechar.Visible = true;
                this.progressBar1.Visible = false;
                //this.TopMost = false;
            }

            this.segundos_trava -= 1;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            this.CancelButton = btnFechar;
            /*
            this.btnFechar.Visible = false;
            if (int.Parse(this.label1.Text) > 0)
            {
                this.progressBar1.Maximum = int.Parse(this.label1.Text);
            }
            */

            

            

            //Process currentProcess = Process.GetCurrentProcess();
            //MessageBox.Show(currentProcess.ProcessName);
            //string nomeExe = currentProcess.ProcessName;
            //MessageBox.Show(nomeExe);


        }
    }
}
