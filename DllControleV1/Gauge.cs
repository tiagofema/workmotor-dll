﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Data;
using System.Threading;
using System.Runtime.InteropServices;
//using System.Collections.Generic;

using System.IO;
using System.Windows.Forms;

namespace DllControleV1
{
    [ComVisible(true)]
    public class Gauge
    {
        float Porcent_Retorno = 0;
        float TiketMedio = 0;
        float Porcent_Desconto = 0 ;

        public void gauge(DateTime datat0, int meses)
        {
            ExtraFoxPro.FoxDbfPrg();
            System.Threading.Thread.Sleep(2000);

            //MessageBox.Show("inicio de " + datat0.ToString() + " Meses " + meses.ToString());
            Thread thread = new Thread(() => th_gauge(datat0, meses));
            thread.Start();
        }

        public void th_gauge(DateTime datat0, int meses)
        {
            string nome_arquivo = @"gauge\gauge.dat";
            string pathfile = Directory.GetCurrentDirectory() + '\\' + nome_arquivo;

            //string pathfile2 = Directory.GetCurrentDirectory() + '\\' + "pqp.txt";

            //Controle.log(pathfile, "Caraleou!!!");
            //MessageBox.Show(pathfile);
            
            th_retorno_cliente(datat0, meses);
            th_ticket_medio_descontos(meses);

            //MessageBox.Show(Porcent_Retorno.ToString() + " | " + TiketMedio.ToString() + " | " + Porcent_Desconto.ToString());

            //  **  se todos forem > 0 consegui gerar os dados pois no pior do caso fica 0  **
            //if (Porcent_Retorno > 0 && Porcent_Desconto > 0 && TiketMedio > 0)
            //{
                if (File.Exists(pathfile))
                    File.Delete(pathfile);

                Controle.log(pathfile, Porcent_Retorno.ToString());
                Controle.log(pathfile, TiketMedio.ToString());
                Controle.log(pathfile, Porcent_Desconto.ToString());
            //}
        }

        public void th_retorno_cliente(DateTime datat0, int meses)
        {
            DateTime datainicial = datat0.AddMonths(-meses);
            DateTime datafinal = datat0.AddMonths(+meses);
            int empcodigo = 0;
            string tselect = "select empcodigo from ORCVENS1 where orcdata >= " + "date(" + datainicial.Year.ToString() + "," + datainicial.Month.ToString() + "," + datainicial.Day.ToString() + ")" + " and  orcdata <= " + "date(" + datat0.Year.ToString() + "," + datat0.Month.ToString() + "," + datat0.Day.ToString() + ")" ;
            DataTable dt = Controle.getda_data_vfp(tselect);

            List<int> lista_empresas = new List<int>() ;
            List<int> lista_empresas_retorno = new List<int>();

            if (!dt.HasErrors)
            {
                foreach (DataRow dtRow in dt.Rows)
                {
                    foreach (DataColumn dc in dt.Columns)
                    {
                        empcodigo = Convert.ToInt32(dtRow[dc].ToString());

                        if (!lista_empresas.Contains(empcodigo))
                            lista_empresas.Add(empcodigo); //  **  codigos de empesas dentro do periodo    ***
                    }
                }            
            }

            //  **  verifico se as empresas tiveraram retorno **
            tselect = "select empcodigo from ORCVENS1 where orcdata > " + "date(" + datat0.Year.ToString() + "," + datat0.Month.ToString() + "," + datat0.Day.ToString() + ")" + " and  orcdata <= " + "date(" + datafinal.Year.ToString() + "," + datafinal.Month.ToString() + "," + datafinal.Day.ToString() + ")";
            dt = Controle.getda_data_vfp(tselect);

            if (!dt.HasErrors)
            {
                foreach (DataRow dtRow in dt.Rows)
                {
                    foreach (DataColumn dc in dt.Columns)
                    {
                        empcodigo = Convert.ToInt32(dtRow[dc].ToString());

                        if (lista_empresas.Contains(empcodigo) && !lista_empresas_retorno.Contains(empcodigo))
                            lista_empresas_retorno.Add(empcodigo); //  **  codigos de empesas dentro do periodo    ***
                    }
                }               
            }


            //MessageBox.Show(lista_empresas_retorno.Count.ToString() + "/" + lista_empresas.Count.ToString());

            if (lista_empresas_retorno.Count > 0)
                Porcent_Retorno = (lista_empresas_retorno.Count * 100) / lista_empresas.Count;
            else
                //Porcent_Retorno = 1;
                Porcent_Retorno = 0;

            //MessageBox.Show(Porcent_Retorno.ToString());

        }

        public void th_ticket_medio_descontos(int meses)
        {
            DateTime datainicial = DateTime.Today.AddMonths(-meses);
            //string tselect = "select orcodigo, empcodigo, orcdescto, OrcDescoPr, OrcDescoSe,  (select sum(ociprctab * ( ociquanti - OciDevQuan )) as orctotItens from ORCVENS2 B where orcodigo = A.orcodigo and day(ocidtapro) > 0)  from ORCVENS1 A where A.orcvenda <> 3 and A.orcvenda <> 0 and A.orcdata >= " + "date(" + datainicial.Year.ToString() + "," + datainicial.Month.ToString() + "," + datainicial.Day.ToString() + ")";

            //  ok
            string tselect = "select orcodigo, empcodigo, orcdescto, OrcDescoPr, OrcDescoSe " +
                             ", (select sum(ociprctab * ( ociquanti - OciDevQuan )) as orctotItens from ORCVENS2 B where orcodigo = A.orcodigo and day(ocidtapro) > 0) " +
                             ", (select sum(( ociquanti - OciDevQuan )) as totqtdprodutos from ORCVENS2 B where orcodigo = A.orcodigo and day(ocidtapro) > 0 and len(procodigo) > 0 ) " +
                             "from ORCVENS1 A where ( A.orcvenda = 1 or A.orcvenda = 2 ) and A.orcdata >= " + "date(" + datainicial.Year.ToString() + "," + datainicial.Month.ToString() + "," + datainicial.Day.ToString() + ")";


            //MessageBox.Show(tselect);

            DataTable dt = Controle.getda_data_vfp(tselect);

            List<int> lista_empresas = new List<int>();
            int empcodigo = 0;
            double TotalDesconto = 0;
            double OrctotItens = 0;
            double TotalItens = 0;
            double OrcDescto = 0;
            //string log_controle = "";
            //int tamanho = 0;

            if (!dt.HasErrors)
            {
                foreach (DataRow dtRow in dt.Rows)
                {
                    empcodigo = Convert.ToInt32(dtRow[dtRow.Table.Columns["empcodigo"].Ordinal].ToString());
                    OrctotItens = Convert.ToDouble(dtRow[dtRow.Table.Columns["orctotItens"].Ordinal].ToString().NumTryParse());

                    //TiketMedio = (float)Math.Round(((TotalItens - TotalDesconto) / lista_empresas.Count), 2);

                    if (Single.Parse(dtRow[dtRow.Table.Columns["OrcDescoPr"].Ordinal].ToString()) == 0 &&
                        Single.Parse(dtRow[dtRow.Table.Columns["OrcDescoSe"].Ordinal].ToString()) == 0)
                    {
                        OrcDescto = Single.Parse(dtRow[dtRow.Table.Columns["orcdescto"].Ordinal].ToString());
                    }
                    else
                    {
                        //  **  se a quantidade de produtos aprovados e nao devolvidos > 0  **
                        if (Single.Parse(dtRow[dtRow.Table.Columns["totqtdprodutos"].Ordinal].ToString().NumTryParse()) > 0)
                        {
                            OrcDescto = Single.Parse(dtRow[dtRow.Table.Columns["OrcDescoPr"].Ordinal].ToString()) +
                                        Single.Parse(dtRow[dtRow.Table.Columns["OrcDescoSe"].Ordinal].ToString());
                        }
                        else
                        {
                            OrcDescto = Single.Parse(dtRow[dtRow.Table.Columns["OrcDescoSe"].Ordinal].ToString());
                        }

                    }

                    TotalDesconto += OrcDescto;
                    TotalItens += OrctotItens;

                    if (!lista_empresas.Contains(empcodigo))
                        lista_empresas.Add(empcodigo); //  **  codigos de empesas dentro do periodo    ***


                    //log_controle = dtRow[dtRow.Table.Columns["orcodigo"].Ordinal].ToString() + ";" + OrctotItens.ToString() + ";" + OrcDescto.ToString();
                    //Controle.log("C:\\controle.txt", log_controle);
                }                
            }
            
            //MessageBox.Show(TotalItens.ToString() + " - " + TotalDesconto.ToString() + " / " + lista_empresas.Count.ToString());

            if (lista_empresas.Count > 0)
            {
                TiketMedio = (float) Math.Round(((TotalItens - TotalDesconto)/lista_empresas.Count), 2);
                if(TiketMedio < 0 )
                    TiketMedio = 0;
            }
            else
                TiketMedio = 1;

            if (TotalItens > 0)
                Porcent_Desconto = (float)Math.Truncate(((TotalDesconto * 100) / TotalItens));
            else
                //Porcent_Desconto = 1;
                Porcent_Desconto = 0;

            //MessageBox.Show(TotalItens.ToString() + " | " + TotalDesconto.ToString() + " | " + Porcent_Desconto.ToString());
            //MessageBox.Show(TiketMedio.ToString() + " | " + Porcent_Desconto.ToString());
        }



    }
}
