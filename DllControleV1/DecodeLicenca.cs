﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace DllControleV1
{
    public class Licenca
    {
        public int DiasValidade;
        public DateTime DataInicio;
        public int LiqBlock;
        public int TempoMsgB;
        public int PeriodicidadeMsgBCode;

        public void Show()
        {
            MessageBox.Show("Dias : " + this.DiasValidade.ToString() + "\n" +
                            "Data Inicio : " +  this.DataInicio.Date.ToString()  + "\n" +
                            "LiqBloack : " + this.LiqBlock.ToString()  + "\n" +
                            "TempoMsg : " + this.TempoMsgB.ToString()  + "\n" +
                            "Periodicidade : " + this.PeriodicidadeMsgBCode.ToString() 
                );
        }
    }

    [ComVisible(true)]
    public class DecodeLicenca
    {
        public static Licenca Decode(string licenca)
        {
            //string licenca = licencaParam;
//            MessageBox.Show("Entrou : " + licenca );
            Licenca licencadecode = new Licenca();
            try
            {
                if (licenca.Trim().Length > 0)
                {
                    //int Magicnumb = int.Parse(licenca.Substring((licenca.Length - 1), 1));
                    //char ccode = licenca.Substring((licenca.Length - 2), 1)[0];
                    int Magicnumb = int.Parse(licenca.Substring(27, 1));
                    char ccode = licenca.Substring(26, 1)[0];
                    string DiasCode = "", DiaCode = "", MesCode = "", AnoCode = "", LiqBlockCode = "", TempoMsgBCode = "", PeriodicidadeMsgBCode = "";

                    switch (Magicnumb)
                    {
                        case 1:
                            DiasCode = licenca.Substring(1, 3);
                            DiaCode = licenca.Substring(4, 2);
                            MesCode = licenca.Substring(7, 2);
                            AnoCode = licenca.Substring(11, 4);
                            LiqBlockCode = licenca.Substring(16, 1);
                            TempoMsgBCode = licenca.Substring(17, 2);
                            PeriodicidadeMsgBCode = licenca.Substring(19, 5);
                            break;
                        case 2:
                            DiasCode = licenca.Substring(2, 3);
                            DiaCode = licenca.Substring(5, 2);
                            MesCode = licenca.Substring(8, 2);
                            AnoCode = licenca.Substring(12, 4);
                            LiqBlockCode = licenca.Substring(17, 1);
                            TempoMsgBCode = licenca.Substring(18, 2);
                            PeriodicidadeMsgBCode = licenca.Substring(20, 5);
                            break;
                        case 3:
                            DiasCode = licenca.Substring(3, 3);
                            DiaCode = licenca.Substring(6, 2);
                            MesCode = licenca.Substring(9, 2);
                            AnoCode = licenca.Substring(13, 4);
                            LiqBlockCode = licenca.Substring(18, 1);
                            TempoMsgBCode = licenca.Substring(19, 2);
                            PeriodicidadeMsgBCode = licenca.Substring(21, 5);
                            break;
                    }

                    //**    DECODIFICA A VALIDADE   **
                    int DiasValidade = int.Parse(TrocaNum(DiasCode, Magicnumb));
                    //**    DECODIFICA A DATA **
                    string DataInicio = TrocaNum(DiaCode, Magicnumb) + '/' +
                                        TrocaNum(MesCode, Magicnumb) + '/' +
                                        TrocaNum(AnoCode, Magicnumb);
                    //**    DECODIFICA LiqBloq **
                    int licbloq = int.Parse(TrocaNum(LiqBlockCode, Magicnumb));
                    //**    DECODIFICA tempo visibilidade da msg **
                    int tempomsg = int.Parse(TrocaNum(TempoMsgBCode, Magicnumb));
                    //**    DECODIFICA periodicidade da msg **
                    int periodicidade = int.Parse(TrocaNum(PeriodicidadeMsgBCode, Magicnumb));

                    
                    licencadecode.DiasValidade = DiasValidade;
                    licencadecode.DataInicio = DateTime.Parse(DataInicio);
                    licencadecode.LiqBlock = licbloq;
                    licencadecode.TempoMsgB = tempomsg;
                    licencadecode.PeriodicidadeMsgBCode = periodicidade;

                    //licencadecode.Show();
                }
                     
            }
            catch { }

            return licencadecode;
        }
        static string TrocaNum(string codigicado, int Magicnumb)
        {
            string decodificado = "";
            for(int i = 0; i < codigicado.Length; i++)
                decodificado += LicTrocaLetra(Magicnumb, codigicado[i]);

            return decodificado;
        }

        public static int LicTrocaLetra(int Magicnumb, char charentrada)
        {
            int troca = 0;
            switch (Magicnumb)
            {
                case 1:
                    switch (charentrada)
                    {
                        case 'A': troca = 1; break;
                        case 'X': troca = 2; break;
                        case 'C': troca = 3; break;
                        case 'L': troca = 4; break;
                        case 'E': troca = 5; break;
                        case 'R': troca = 6; break;
                        case 'G': troca = 7; break;
                        case 'H': troca = 8; break;
                        case 'V': troca = 9; break;
                        default: troca = 0; break;
                    }
                    break;
                case 2:
                    switch (charentrada)
                    {
                        case 'Y': troca = 1; break;
                        case 'K': troca = 2; break;
                        case 'D': troca = 3; break;
                        case 'M': troca = 4; break;
                        case 'T': troca = 5; break;
                        case 'O': troca = 6; break;
                        case 'W': troca = 7; break;
                        case 'Q': troca = 8; break;
                        case 'R': troca = 9; break;
                        default: troca = 0; break;
                    }
                    break;

                case 3:
                    switch (charentrada)
                    {
                        case 'I': troca = 1; break;
                        case 'T': troca = 2; break;
                        case 'U': troca = 3; break;
                        case 'V': troca = 4; break;
                        case 'B': troca = 5; break;
                        case 'Y': troca = 6; break;
                        case 'Z': troca = 7; break;
                        case 'O': troca = 8; break;
                        case 'L': troca = 9; break;
                        default: troca = 0; break;
                    }
                    break;
            }
            return troca;
        }
    }
}
